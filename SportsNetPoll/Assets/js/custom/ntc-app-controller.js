/*
Created by Netuce | ©2017
info@netuce.com
www.netuce.com
*/
$(document).ready(function(){
    changePage(); 
    pullSubmit();
});

function pullSubmit(){
    $('#ntc-pull').submit(function(e){
        e.preventDefault;
        var pullObject = $('form').serializeArray();
        
        if(pullObject.length == 7){
            console.log(pullObject);
            redirectPage('#ntci-thank-you');
        }else{
            alert('İşleminizi tamamlayabilmeniz için tüm kategorilerilerde seçim yapmalısınız.');
        }
        
        return false;
    });
};