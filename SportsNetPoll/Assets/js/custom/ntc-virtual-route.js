/*
Created by Netuce | ©2017
info@netuce.com
www.netuce.com
*/
function changePage(){
    $('[ntc-f-virtual-link]').click(function(e){
        e.preventDefault();

        var target = $(this).attr('href');
        if(window.location.pathname && target.charAt(0) == '#'){
            location.href = location.origin + location.pathname + target;
            return;
        }else{
            history.pushState(null, null, (target));   
        }
        
        redirectPage();
    });
    
    $(window).on('popstate', function() {
      redirectPage();
    });
}

function redirectPage(target){
    var pageId;
    
    if(!target){
        if(window.location.hash){
            target = window.location.hash;   
        }else if(window.location.pathname && window.location.pathname.length > 1){
            target = window.location.pathname;   
        }
    }
    
    if(target){
        if(target.charAt(0) == '#' || target.charAt(0) == '/'){
            pageId = target.substr(1);
            pageId = pageId.split("/");
            pageId = pageId[pageId.length - 1];
        }else{
            pageId = target;
        }
        
        target = decodeURIComponent(target);
        
        if(target != ''){
            $('.active').removeClass('active');
            $(target).addClass('active'); 
        }   
    }else{
        console.log('target is not defined yet.');
    }
}