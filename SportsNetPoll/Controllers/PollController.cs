﻿using System.Web.Mvc;
using NtC.Netuce.Business.Abstract;
using NtC.Netuce.Entities.SportsNet;

namespace SportsNetPoll.Controllers
{
    public class PollController : Controller
    {
        private ISportsPoll repo;
        // GET: Poll
        public JsonResult setpoll(string token, JuryLine model)
        {
            var query = repo.getvote(token, model);
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        public ActionResult set()
        {
            return View();
        }
    }
}