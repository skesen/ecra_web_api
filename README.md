Created by Netuce | ©2016  
info@netuce.com  
www.netuce.com  
Version 1.5.1  

# **Ecra Web Api** #   
-----------------------

# Api Adresleri#

**Live :**
http://ec.basarservers3.com/


**Develop :**
http://dev.basarservers3.com/

#**Api nasıl kullanılır?** #

#Authentication #


Api adresimizin uç noktasını **/token** path’ine yönlendirdiğimiz adrese gidip, geçerli bir **access_token** almalıyız.


Bunun için **POST** tipinde **/token** url’ine Headers’a ve Body’e bir kaç parametre set ederek gitmemiz gerekmektedir.


 **Headers’a eklenecek parametreler:** 

Keys     | Values
:---------| :-----  
Header: Accept       |         Value: application/json
Header: Content-Type  |   Value: application/x-www-form-urlencoded


** Body’e eklenecek parametreler:** 

Keys     | Values
:---------| :-----  
Key:username     | Value:Netuce    
Key:password  |  Value:123456 
Key:grant_type    |   Value:password 
            



Bu adımları tamamladıktan sonra sistem üzerinde access_token belirlenen expire süresi tarayıcı üzerinde set ediliyor. 


![Alt text](http://www.basarservers3.com/apitoken.png "Token Oluşturma İşlemi")

-----------

Apimizin uç noktasını bu sefer **/api/Project** 'e yönlendirip **get** işlemi yapıyoruz. Fakat bu sefer **access_token**’ı headers’a ekliyoruz.


 **Headers’a eklenecek parametreler:** 

Keys     | Values
:---------| :----- 
Value: Content-Type  |     Key:application/json
Value: Authorization |       Key:Bearer 96ayVx1k8ZFQOgEnQWJZLqmz7Y7uxL5bQ6HPN06VZQr_-....


# (GET)Sorgu işlemlerinde kullanılacak parametreler #

| Parametre Adı | Açıklama                       | Örnek   |
| ------------- | ------------------------------ |-------- |
| table         | sorgulamanın yapılacağı tablo  |/api/task|
| field         | tablodan dönülmesi istenilen kolonlar |/api/task?field=name,createddate|
| where         | tabloda yapılması gereken sorgu objesi|/api/task?where={"repono~='%R0%'%26%26datecreated>'2017-03-01'%7C%7Ccreator='u12ser'"}|
| range         | parçalı data çekme için aralık |/api/task?range=0,15|
| sort          | tablodan çekilecek verilerin sıralaması |/api/task?sort={"name":"asc","datecreated":"date=>desc"}|
| join          | birden fazla tabloyu birleştirme |/api/file?join={"file":"fileid","arbitration":"fileid"} |
| leftjoin      | birden fazla tabloyu birleştirme(Join sol tarafı baz alınarak) |/api/file?leftjoin={"file":"fileid","arbitration":"fileid"} |
| rightjoin     | birden fazla tabloyu birleştirme(Join sağ tarafı baz alınarak) |/api/file?rightjoin={"file":"fileid","arbitration":"fileid"} |

# (POST) Database işlemlerinde kullanılacak parametreler #

| Parametre Adı | Açıklama                       | Örnek   |
| ------------- | ------------------------------ |-------- |
| method        | database'de hangi işlemin yapılacağı|/api/task?method=insert|





>**method** parametresi için kulanılabilecek durumlar: *create || insert, update, trash*

>**where** parametresi için kullanılabilecek sorgusal durumlar: *and = %26%26 , like = ~=, or = ||, = = == , (% = %25 like için)*

# View oluşturma işlemi #

 /createview/{viewname} uç noktası kullanılarak databasede view oluşturulabilir. 

{viewname} = oluşturulacak view'in ismi

 '**-**' öneki sql sorgusunda **as** deyimi yerine kullanılmaktadır.
>** as** işlemi çekilecek tablodaki kolon ismini değiştirmektedir.

***Örnek:***
```
#!python

/createview/getviewrepo?field={"arbitration":"editor-AEditor,applicationdate","repo":"editor-REditor,repono"}&join={"arbitration":"arbitrationid","repo":"arbitrationid"}
```


# Dosya ekleme işlemi # 


**POST** api/addfile uç noktası kullanılarak fiziksel sunucuya dosya ekleyebilirsiniz.

header | multipart/form-data

# Email işlemleri #

**Listeleme ve seçilen kaynağın özeti**

| Olaylar | Açıklama | Örnek |
|---------|----------|-------|
|Labels   |Bağlı hesabın etkiteleri   |mail/labels?where={"userid":"ex:1234564",*"labelid":"ex:15bf.."*}|
|Threads  |Bağlı hesaptaki mail konularını |mail/threads?where={"userid":"ex:1234564",*"threadid":"ex:15bf.."*}|
|Messages |Bağlı hesaptaki mesajları listeler |mail/messages?where={"userid":"ex:1234564",*"messageid":"ex:15bf.."*}|

> where sorgusundaki labelid,threadid,messageid alanlarına göre seçili kaynağın özetini vermektedir. Alanlar(labelid,threadid,messageid) yazılmadığında bağlı hesaba ait tüm özet listelenmektedir.

**Güncelleme **

| Olaylar | Açıklama | Örnek |
|---------|----------|-------|
|action.removeLabelIds=['INBOX'] |Email arşive taşınır|/mail/threads?filter=INBOX|
|action.removeLabelIds=['UNREAD'] |Email okundu olarak değiştirilir|/mail/threads?filter=UNREAD|
|action.addLabelIds=['TRASH'] |Email çöp kutusuna taşınır|/mail/threads?filter=TRASH|
|action.addLabelIds=['STARRED'] |Yıldız olarak işaretlenir|/mail/threads?filter=STARRED|