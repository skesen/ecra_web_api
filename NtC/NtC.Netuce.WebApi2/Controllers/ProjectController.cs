﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using NtC.Netuce.Business.Abstract;
using NtC.Netuce.Entities.Concrete;

namespace NtC.Netuce.WebApi2.Controllers
{
    [EnableCors("*", "*", "*")]
    public class ProjectsController : ApiController
    {
        private IProjectService _projectService;

        public ProjectsController(IProjectService _projectService)
        {
            this._projectService = _projectService;
        }

        [Route("projects")]
        public IEnumerable<object> Get()
        {
            var result = _projectService.Get();
            return result;
        }

        //http://netuceapi/projects/search/angular
        [Route("projects/search/{projectName}")]
        public object Get(int projectName)
        {
            var result = _projectService.Get(projectName);
            return result;
        }

        [Authorize(Roles = "Admin")]
        public HttpResponseMessage Post([FromBody] Project project)
        {
            _projectService.Post(project);
            return new HttpResponseMessage(HttpStatusCode.OK);
        }
    }
}
