﻿// ***********************************************************************
// Assembly         : NtC.Netuce.DataAccess
// Author           : Serkan KESEN
// Created          : 11-28-2016
//
// Last Modified By : Serkan KESEN
// Last Modified On : 01-30-2017
// ***********************************************************************
// <copyright file="NtCContext.cs" company="Netuce">
//     Copyright ©  2016
// </copyright>
// <summary>Context Configuration File</summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Diagnostics;
using System.Linq;
using CodeFirstStoreFunctions;
using Microsoft.AspNet.Identity.EntityFramework;
using MySql.Data.Entity;
using MySql.Data.MySqlClient;
using NtC.Netuce.Core.Entities;
using NtC.Netuce.Core.Utilities.ExtensionMethods;

#region SportsNet
//using NtC.Netuce.Entities.SportsNet;

#endregion
#region ABIS
//using NtC.Netuce.Entities.Concrete;
//using User = NtC.Netuce.Entities.ABIS.User;
//using NtC.Netuce.Entities.ABIS;
#endregion
#region YTT
using NtC.Netuce.Entities.YTT;
using Client = NtC.Netuce.Entities.Concrete.Client;
using RefreshToken = NtC.Netuce.Entities.Concrete.RefreshToken;
using NtC.Netuce.Entities.YTT.TrashYTT;
#endregion
#region SecmenApp
//using NtC.Netuce.Entities.SecmenApp;
//using Client = NtC.Netuce.Entities.Concrete.Client;
//using RefreshToken = NtC.Netuce.Entities.Concrete.RefreshToken;
#endregion
#region Ecra
//using NtC.Netuce.Entities.Concrete;
//using NtC.Netuce.Entities.TrashConcrete;
#endregion


namespace NtC.Netuce.DataAccess.Concrete.EntityFramework
{
    /// <summary>
    /// Class NtCContext.
    /// </summary>
    /// <seealso cref="Microsoft.AspNet.Identity.EntityFramework.IdentityDbContext{ApplicationUser}" />
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class NtCContext : IdentityDbContext<ApplicationUser>
    {
        #region ContextInfos

        private static string context = Properties.System.Default.YTT_Dev;
        public static string contextpath = "YTT_Dev";

        /// <summary>
        /// Initializes a new instance of the <see cref="NtCContext"/> class.
        /// </summary>
        public NtCContext() : base(context)
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
            Configuration.ValidateOnSaveEnabled = false;
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<NtCContext, MyDbInitializer>());
            //Database.SetInitializer(new CreateDatabaseIfNotExists<NtCContext>());
            //Database.SetInitializer<NtCContext>(new MyDbInitializer());
            Database.Log = s => Debug.WriteLine(s);
        }

        /// <summary>
        /// Creates this instance.
        /// </summary>
        /// <returns>NtCContext.</returns>
        public static NtCContext Create()
        {
            return new NtCContext();
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Add(new FunctionsConvention<NtCContext>("ntc"));
            modelBuilder.Entity<IdentityRole>().Property(c => c.Name).HasMaxLength(128).IsRequired();
            modelBuilder.Entity<ApplicationUser>().ToTable("AspNetUser").Property(c => c.UserName).HasMaxLength(128).IsRequired();
        }
        /// <summary>
        /// Maps table names, and sets up relationships between the various user entities
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>

        #endregion
        #region Ecra 1.02

        //#region DbSets Ecra 1.02
        //public DbSet<TableReplace> TableReplace { get; set; }
        ///// <summary>
        ///// Gets or sets the tag.
        ///// </summary>
        ///// <value>The tag.</value>
        //public DbSet<Tag> Tag { get; set; }

        ///// <summary>
        ///// Gets or sets the role action.
        ///// </summary>
        ///// <value>The role action.</value>
        //public DbSet<RoleAction> RoleAction { get; set; }
        ///// <summary>
        ///// Gets or sets the user role permission group.
        ///// </summary>
        ///// <value>The user role permission group.</value>
        //public DbSet<UserRolePermissionGroup> UserRolePermissionGroup { get; set; }
        ///// <summary>
        ///// Gets or sets the role permission.
        ///// </summary>
        ///// <value>The role permission.</value>
        //public DbSet<RolePermission> RolePermission { get; set; }
        ///// <summary>
        ///// Gets or sets the user role.
        ///// </summary>
        ///// <value>The user role.</value>
        //public DbSet<UserRole> UserRole { get; set; }

        ///// <summary>
        ///// Gets or sets the task.
        ///// </summary>
        ///// <value>The task.</value>
        //public DbSet<Task> Task { get; set; }
        ///// <summary>
        ///// Gets or sets the task attachment.
        ///// </summary>
        ///// <value>The task attachment.</value>
        //public DbSet<TaskAttachment> TaskAttachment { get; set; }
        ///// <summary>
        ///// Gets or sets the task category.
        ///// </summary>
        ///// <value>The task category.</value>
        //public DbSet<TaskCategory> TaskCategory { get; set; }
        ///// <summary>
        ///// Gets or sets the task comment.
        ///// </summary>
        ///// <value>The task comment.</value>
        //public DbSet<TaskComment> TaskComment { get; set; }
        ///// <summary>
        ///// Gets or sets the task link.
        ///// </summary>
        ///// <value>The task link.</value>
        //public DbSet<TaskLink> TaskLink { get; set; }
        ///// <summary>
        ///// Gets or sets the type of the task.
        ///// </summary>
        ///// <value>The type of the task.</value>
        //public DbSet<TaskType> TaskType { get; set; }
        ///// <summary>
        ///// Gets or sets the task status.
        ///// </summary>
        ///// <value>The task status.</value>
        //public DbSet<TaskStatus> TaskStatus { get; set; }

        ///// <summary>
        ///// Gets or sets the project.
        ///// </summary>
        ///// <value>The project.</value>
        //public DbSet<Project> Project { get; set; }
        ///// <summary>
        ///// Gets or sets the project tag.
        ///// </summary>
        ///// <value>The project tag.</value>
        //public DbSet<ProjectTag> ProjectTag { get; set; }
        ///// <summary>
        ///// Gets or sets the project category.
        ///// </summary>
        ///// <value>The project category.</value>
        //public DbSet<ProjectCategory> ProjectCategory { get; set; }

        ///// <summary>
        ///// Gets or sets the user.
        ///// </summary>
        ///// <value>The user.</value>
        //public DbSet<User> User { get; set; }
        ///// <summary>
        ///// Gets or sets the refresh token.
        ///// </summary>
        ///// <value>The refresh token.</value>
        //public DbSet<RefreshToken> RefreshToken { get; set; }
        ///// <summary>
        ///// Gets or sets the client.
        ///// </summary>
        ///// <value>The client.</value>
        //public DbSet<Client> Client { get; set; }

        ///// <summary>
        ///// Gets or sets the sector.
        ///// </summary>
        ///// <value>The sector.</value>
        //public DbSet<Sector> Sector { get; set; }
        ///// <summary>
        ///// Gets or sets the work title.
        ///// </summary>
        ///// <value>The work title.</value>
        //public DbSet<WorkTitle> WorkTitle { get; set; }

        ///// <summary>
        ///// Gets or sets the company.
        ///// </summary>
        ///// <value>The company.</value>
        //public DbSet<Company> Company { get; set; }
        ///// <summary>
        ///// Gets or sets the company tag.
        ///// </summary>
        ///// <value>The company tag.</value>
        //public DbSet<CompanyTag> CompanyTag { get; set; }
        ////public DbSet<CompanyTitle> CompanyTitle { get; set; }

        ///// <summary>
        ///// Gets or sets the individual.
        ///// </summary>
        ///// <value>The individual.</value>
        //public DbSet<Individual> Individual { get; set; }
        ///// <summary>
        ///// Gets or sets the individual tag.
        ///// </summary>
        ///// <value>The individual tag.</value>
        //public DbSet<IndividualTag> IndividualTag { get; set; }

        ///// <summary>
        ///// Gets or sets the contact source.
        ///// </summary>
        ///// <value>The contact source.</value>
        //public DbSet<ContactSource> ContactSource { get; set; }
        ///// <summary>
        ///// Gets or sets the contact status.
        ///// </summary>
        ///// <value>The contact status.</value>
        //public DbSet<ContactStatus> ContactStatus { get; set; }

        ///// <summary>
        ///// Gets or sets the product.
        ///// </summary>
        ///// <value>The product.</value>
        //public DbSet<Product> Product { get; set; }
        ///// <summary>
        ///// Gets or sets the product category.
        ///// </summary>
        ///// <value>The product category.</value>
        //public DbSet<ProductCategory> ProductCategory { get; set; }
        ///// <summary>
        ///// Gets or sets the product tag.
        ///// </summary>
        ///// <value>The product tag.</value>
        //public DbSet<ProductTag> ProductTag { get; set; }

        ///// <summary>
        ///// Gets or sets the library.
        ///// </summary>
        ///// <value>The library.</value>
        //public DbSet<Library> Library { get; set; }

        ///// <summary>
        ///// Gets or sets the province.
        ///// </summary>
        ///// <value>The province.</value>
        //public DbSet<Province> Province { get; set; }
        ///// <summary>
        ///// Gets or sets the district.
        ///// </summary>
        ///// <value>The district.</value>
        //public DbSet<District> District { get; set; }
        ///// <summary>
        ///// Gets or sets the country.
        ///// </summary>
        ///// <value>The country.</value>
        //public DbSet<Country> Country { get; set; }

        ///// <summary>
        ///// Gets or sets the link.
        ///// </summary>
        ///// <value>The link.</value>
        //public DbSet<Link> Link { get; set; }
        ///// <summary>
        ///// Gets or sets the type of the link.
        ///// </summary>
        ///// <value>The type of the link.</value>
        //public DbSet<LinkType> LinkType { get; set; }
        ///// <summary>
        ///// Gets or sets the link category.
        ///// </summary>
        ///// <value>The link category.</value>
        //public DbSet<LinkCategory> LinkCategory { get; set; }
        ///// <summary>
        ///// Gets or sets the comment.
        ///// </summary>
        ///// <value>The comment.</value>
        //public DbSet<Comment> Comment { get; set; }

        ///// <summary>
        ///// Gets or sets the social media.
        ///// </summary>
        ///// <value>The social media.</value>
        //public DbSet<SocialMedia> SocialMedia { get; set; }
        ///// <summary>
        ///// Gets or sets the phone.
        ///// </summary>
        ///// <value>The phone.</value>
        //public DbSet<Phone> Phone { get; set; }
        ///// <summary>
        ///// Gets or sets the address.
        ///// </summary>
        ///// <value>The address.</value>
        //public DbSet<Address> Address { get; set; }

        ///// <summary>
        ///// Gets or sets the potential.
        ///// </summary>
        ///// <value>The potential.</value>
        //public DbSet<Potential> Potential { get; set; }

        ///// <summary>
        ///// Gets or sets the potential source.
        ///// </summary>
        ///// <value>The potential source.</value>
        //public DbSet<PotentialSource> PotentialSource { get; set; }
        ///// <summary>
        ///// Gets or sets the state of the potential.
        ///// </summary>
        ///// <value>The state of the potential.</value>
        //public DbSet<PotentialState> PotentialState { get; set; }
        ///// <summary>
        ///// Gets or sets the potential tag.
        ///// </summary>
        ///// <value>The potential tag.</value>
        //public DbSet<PotentialTag> PotentialTag { get; set; }

        //public DbSet<Visibility> Visibility { get; set; }
        //public DbSet<Department> Department { get; set; }
        //public DbSet<TeamMember> TeamMember { get; set; }
        //public DbSet<Team> Team { get; set; }
        //public DbSet<IndividualAccess> IndividualAccess { get; set; }

        //#endregion
        //#region DbSets Trash_Ecra 1.02
        //public DbSet<Trash_RoleAction> Trash_RoleAction { get; set; }
        //public DbSet<Trash_UserRolePermissionGroup> Trash_UserRolePermissionGroup { get; set; }
        //public DbSet<Trash_RolePermission> Trash_RolePermission { get; set; }
        //public DbSet<Trash_UserRole> Trash_UserRole { get; set; }
        //public DbSet<Trash_Task> Trash_Task { get; set; }
        //public DbSet<Trash_TaskAttachment> Trash_TaskAttachment { get; set; }
        //public DbSet<Trash_TaskCategory> Trash_TaskCategory { get; set; }
        //public DbSet<Trash_TaskComment> Trash_TaskComment { get; set; }
        //public DbSet<Trash_TaskLink> Trash_TaskLink { get; set; }
        //public DbSet<Trash_TaskType> Trash_TaskType { get; set; }
        //public DbSet<Trash_TaskStatus> Trash_TaskStatus { get; set; }
        //public DbSet<Trash_Project> Trash_Project { get; set; }
        //public DbSet<Trash_ProjectTag> Trash_ProjectTag { get; set; }
        //public DbSet<Trash_ProjectCategory> Trash_ProjectCategory { get; set; }
        //public DbSet<Trash_User> Trash_User { get; set; }
        //public DbSet<Trash_Sector> Trash_Sector { get; set; }
        //public DbSet<Trash_WorkTitle> Trash_WorkTitle { get; set; }
        //public DbSet<Trash_Company> Trash_Company { get; set; }
        //public DbSet<Trash_CompanyTag> Trash_CompanyTag { get; set; }
        //public DbSet<Trash_Individual> Trash_Individual { get; set; }
        //public DbSet<Trash_IndividualTag> Trash_IndividualTag { get; set; }
        //public DbSet<Trash_ContactSource> Trash_ContactSource { get; set; }
        //public DbSet<Trash_ContactStatus> Trash_ContactStatus { get; set; }
        //public DbSet<Trash_Product> Trash_Product { get; set; }
        //public DbSet<Trash_ProductCategory> Trash_ProductCategory { get; set; }
        //public DbSet<Trash_ProductTag> Trash_ProductTag { get; set; }
        //public DbSet<Trash_Library> Trash_Library { get; set; }
        //public DbSet<Trash_Province> Trash_Province { get; set; }
        //public DbSet<Trash_District> Trash_District { get; set; }
        //public DbSet<Trash_Country> Trash_Country { get; set; }
        //public DbSet<Trash_Link> Trash_Link { get; set; }
        //public DbSet<Trash_LinkType> Trash_LinkType { get; set; }
        //public DbSet<Trash_LinkCategory> Trash_LinkCategory { get; set; }
        //public DbSet<Trash_Comment> Trash_Comment { get; set; }
        //public DbSet<Trash_SocialMedia> Trash_SocialMedia { get; set; }
        //public DbSet<Trash_Phone> Trash_Phone { get; set; }
        //public DbSet<Trash_Address> Trash_Address { get; set; }
        //public DbSet<Trash_Potential> Trash_Potential { get; set; }
        //public DbSet<Trash_PotentialSource> Trash_PotentialSource { get; set; }
        //public DbSet<Trash_PotentialState> Trash_PotentialState { get; set; }
        //public DbSet<Trash_PotentialTag> Trash_PotentialTag { get; set; }
        //public DbSet<Trash_Visibility> Trash_Visibility { get; set; }
        //public DbSet<Trash_Department> Trash_Department { get; set; }
        //public DbSet<Trash_TeamMember> Trash_TeamMember { get; set; }
        //public DbSet<Trash_Team> Trash_Team { get; set; }
        //public DbSet<Trash_IndividualAccess> Trash_IndividualAccess { get; set; }
        //#endregion

        #endregion

        #region DbSets SecmenApi
        //public DbSet<User> User { get; set; }
        //public DbSet<RolePermission> RolePermission { get; set; }
        //public DbSet<UserRole> UserRole { get; set; }
        //public DbSet<RefreshToken> RefreshToken { get; set; }
        //public DbSet<Client> Client { get; set; }
        //public DbSet<Province> Province { get; set; }
        //public DbSet<District> District { get; set; }
        //public DbSet<Country> Country { get; set; }
        //public DbSet<Neighborhood> Neighborhood { get; set; }
        //public DbSet<Survey> Survey { get; set; }
        //public DbSet<Opinion> Opinion { get; set; }
        #endregion

        #region DbSets YTT
        public DbSet<File> File { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<UserStatus> UserStatus { get; set; }
        public DbSet<UserRole> UserRole { get; set; }
        public DbSet<RefreshToken> RefreshToken { get; set; }
        public DbSet<Client> Client { get; set; }
        public DbSet<Arbitration> Arbitration { get; set; }
        public DbSet<InsuranceCompany> InsuranceCompany { get; set; }
        public DbSet<FleetLeasingCompany> FleetLeasingCompany { get; set; }
        public DbSet<Reminder> Reminder { get; set; }
        public DbSet<Repo> Repo { get; set; }
        public DbSet<Collection> Collection { get; set; }
        public DbSet<TeamMember> TeamMember { get; set; }
        public DbSet<Team> Team { get; set; }
        public DbSet<FileAccess> FileAccess { get; set; }
        public DbSet<Note> Note { get; set; }
        public DbSet<UserRolePermissionGroup> UserRolePermissionGroup { get; set; }
        public DbSet<UserRolePermission> UserRolePermission { get; set; }
        public DbSet<UserRoleAction> UserRoleAction { get; set; }
        public DbSet<UserRoleActionValue> UserRoleActionValue { get; set; }
        public DbSet<Comment> Comment { get; set; }

        public DbSet<TableReplace> TableReplace { get; set; }

        public DbSet<Trash_File> Trash_File { get; set; }
        public DbSet<Trash_User> Trash_User { get; set; }
        public DbSet<Trash_UserStatus> Trash_UserStatus { get; set; }
        public DbSet<Trash_UserRole> Trash_UserRole { get; set; }
        public DbSet<Trash_Arbitration> Trash_Arbitration { get; set; }
        public DbSet<Trash_InsuranceCompany> Trash_InsuranceCompany { get; set; }
        public DbSet<Trash_FleetLeasingCompany> Trash_FleetLeasingCompany { get; set; }
        public DbSet<Trash_Reminder> Trash_Reminder { get; set; }
        public DbSet<Trash_Repo> Trash_Repo { get; set; }
        public DbSet<Trash_Collection> Trash_Collection { get; set; }
        public DbSet<Trash_Team> Trash_Team { get; set; }
        public DbSet<Trash_TeamMember> Trash_TeamMember { get; set; }
        public DbSet<Trash_FileAccess> Trash_FileAccess { get; set; }
        public DbSet<Trash_Note> Trash_Note { get; set; }
        public DbSet<Trash_UserRolePermissionGroup> Trash_UserRolePermissionGroup { get; set; }
        public DbSet<Trash_UserRolePermission> Trash_UserRolePermission { get; set; }
        public DbSet<Trash_UserRoleAction> Trash_UserRoleAction { get; set; }
        public DbSet<Trash_UserRoleActionValue> Trash_UserRoleActionValue { get; set; }
        public DbSet<Trash_Comment> Trash_Comment { get; set; }

        #endregion

        #region DbSets ABIS
        //public DbSet<RefreshToken> RefreshToken { get; set; }
        //public DbSet<Client> Client { get; set; }
        //public DbSet<User> User { get; set; }
        //public DbSet<Neighborhood> Neighborhood { get; set; }
        //public DbSet<Street> Street { get; set; }
        //public DbSet<Entities.ABIS.Address> Address { get; set; }
        //public DbSet<Coordinate> Coordinate { get; set; }
        #endregion

        #region DbSets SportsNet Poll
        //public DbSet<Category> Category { get; set; }
        //public DbSet<Option> Option { get; set; }
        //public DbSet<Jury> Jury { get; set; }
        //public DbSet<JuryLine> JuryLine { get; set; }
        #endregion

        #region StoreProcedures
        /// <summary>
        /// Get User by Email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public virtual User GetNtCUserByEmail(string email)
        {
              var data=  Database.SqlQuery<User>("USP_GetNtCUserByEmail(@mail)",
                new MySqlParameter("mail", email)).FirstOrDefault();
            return data;
        }
        /// <summary>
        /// SP's the get row count.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="columnId">The column identifier.</param>
        /// <returns>System.Int32.</returns>
        public virtual int USP_GetRowCount(string tableName, string wherequery)
        {
            return Database.SqlQuery<int>("USP_GetRowCount(@tableName,@wherequery)",
                new MySqlParameter("tableName", tableName),
                new MySqlParameter("wherequery", wherequery)).First();
        }
        /// <summary>
        /// Usps the get any table by identifier.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="columnId">The column identifier.</param>
        /// <returns>IEnumerable&lt;System.Object&gt;.</returns>
        public virtual IEnumerable<object> USP_GetNtCByID(string tableName, string columnName, string columnId)
        {
            Database.Connection.Open();
            using (var cmd = Database.Connection.CreateCommand())
            {
                cmd.CommandText = "USP_GetNtC(@tableName,@columnName,@columnId)";

                cmd.Parameters.Add(new MySqlParameter("tableName", tableName));
                cmd.Parameters.Add(new MySqlParameter("columnName", columnName));
                cmd.Parameters.Add(new MySqlParameter("columnId", columnId));
                using (var dataReader = cmd.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        var dataRow = Extensions.GetDataRow(dataReader);
                        yield return dataRow;
                    }
                }
            }
            Database.Connection.Close();
        }

        public virtual IEnumerable<object> USP_GetNtCForWhere(string tableName, string wherequery)
        {
            Database.Connection.Open();
            using (var cmd = Database.Connection.CreateCommand())
            {
                cmd.CommandText = "USP_GetNtCForWhere(@tableName,@wherequery)";

                cmd.Parameters.Add(new MySqlParameter("tableName", tableName));
                cmd.Parameters.Add(new MySqlParameter("wherequery", wherequery));
                using (var dataReader = cmd.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        var dataRow = Extensions.GetDataRow(dataReader);
                        yield return dataRow;
                    }
                }
            }
            Database.Connection.Close();
        }

        /// <summary>
        /// SP the post insert.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="columnNames">The column names.</param>
        /// <param name="columnValues">The column values.</param>
        public virtual void USP_PostNtCInsert(string tableName, string columnNames, string columnValues)
        {

            Database.ExecuteSqlCommand("USP_PostNtCInsert(@tableName,@columnNames,@columnValues)",
              new MySqlParameter("tableName", tableName),
              new MySqlParameter("columnNames", columnNames),
              new MySqlParameter("columnValues", columnValues));
            Database.Connection.Close();
        }
        /// <summary>
        /// SP's the post update.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="columnValues">The column values.</param>
        /// <param name="changeColumn">The change column.</param>
        public virtual void USP_PostNtCUpdate(string tableName, string columnValues, string changeColumn)
        {
            Database.ExecuteSqlCommand("USP_PostNtCUpdate(@tableName,@columnValues,@changeColumn)",
              new MySqlParameter("tableName", tableName),
              new MySqlParameter("columnValues", columnValues),
              new MySqlParameter("changeColumn", changeColumn));
            Database.Connection.Close();
        }
        /// <summary>
        /// SP's the post delete.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="changeColumn">The change column.</param>
        public virtual void USP_PostNtCDelete(string tableName, string changeColumn)
        {
            Database.ExecuteSqlCommand("USP_PostNtCDelete(@tableName,@changeColumn)",
              new MySqlParameter("tableName", tableName),
              new MySqlParameter("changeColumn", changeColumn));
            Database.Connection.Close();

        }
        /// <summary>
        /// SP's the get search.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="columnValue">The column value.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="sortname">The sortname.</param>
        /// <param name="total">The total.</param>
        /// <param name="startcolumn">The startcolumn.</param>
        /// <returns>IEnumerable&lt;dynamic&gt;.</returns>
        public virtual IEnumerable<dynamic> USP_GetNtCSearch(string field, string tableName, string columnName, string columnValue, string filter, string sortname, int total, int startcolumn)
        {
            var search = new List<dynamic>();
            try
            {
                Database.Connection.Open();
                using (var cmd = Database.Connection.CreateCommand())
                {
                    cmd.CommandText = "USP_GetNtCSearch(@field,@tableName,@columnName,@columnValue,@filter,@sortname,@total,@startcolumn)";
                    cmd.Parameters.Add(new MySqlParameter("field", field));
                    cmd.Parameters.Add(new MySqlParameter("tableName", tableName));
                    cmd.Parameters.Add(new MySqlParameter("columnName", columnName));
                    cmd.Parameters.Add(new MySqlParameter("columnValue", columnValue));
                    cmd.Parameters.Add(new MySqlParameter("filter", filter));
                    cmd.Parameters.Add(new MySqlParameter("sortname", sortname));
                    cmd.Parameters.Add(new MySqlParameter("total", total));
                    cmd.Parameters.Add(new MySqlParameter("startcolumn", startcolumn));

                    using (var dataReader = cmd.ExecuteReader())
                    {

                        while (dataReader.Read())
                        {
                            var dataRow = Extensions.GetDataRow(dataReader);
                            search.Add(dataRow);
                        }

                    }


                }
                Database.Connection.Close();
                
            }
            catch (Exception ex)
            {
                //var email = new Test("Kayit cekerken problem olustu. "+ contextpath + " : "+ ex);
                //email.Send();
                throw;
            }
            return search;
        }
        /// <summary>
        /// Usps the get nt c search for filter.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="sortname">The sortname.</param>
        /// <param name="total">The total.</param>
        /// <param name="startcolumn">The startcolumn.</param>
        /// <returns>IEnumerable&lt;dynamic&gt;.</returns>
        public virtual IEnumerable<dynamic> USP_GetNtCSearchForFilter(string field, string tableName, string filter, string sortname, int total, int startcolumn)
        {
            var search = new List<dynamic>();
            try
            {
                Database.Connection.Open();
                using (var cmd = Database.Connection.CreateCommand())
                {
                    cmd.CommandText = "USP_GetNtCSearchForFilter(@field,@tableName,@filter,@sortname,@total,@startcolumn)";
                    cmd.Parameters.Add(new MySqlParameter("field", field));
                    cmd.Parameters.Add(new MySqlParameter("tableName", tableName));
                    cmd.Parameters.Add(new MySqlParameter("filter", filter));
                    cmd.Parameters.Add(new MySqlParameter("sortname", sortname));
                    cmd.Parameters.Add(new MySqlParameter("total", total));
                    cmd.Parameters.Add(new MySqlParameter("startcolumn", startcolumn));

                    using (var dataReader = cmd.ExecuteReader())
                    {

                        while (dataReader.Read())
                        {
                            var dataRow = Extensions.GetDataRow(dataReader);
                            search.Add(dataRow);
                        }

                    }


                }
                Database.Connection.Close();
            }
            catch (Exception ex)
            {
                //var email = new Test("Kayit çekerken problem olustu. " + contextpath + " : " + ex);
                //email.Send();
                throw;
            }
            return search;
        }

        public virtual void USP_NtCCreateView(string script)
        {
            Database.Connection.Open();
            Database.ExecuteSqlCommand("USP_NtCCreateView(@script)",
                new MySqlParameter("script", script));
            Database.Connection.Close();
        }

        #endregion


        
    }
}
