﻿using System;
using System.Linq;
using NtC.Netuce.Core.DataAccess.EntityFramework;
using NtC.Netuce.DataAccess.Abstract;
using NtC.Netuce.Entities.ABIS;

namespace NtC.Netuce.DataAccess.Concrete.EntityFramework.Utilities
{
    public class EfUserDal: EfEntityRepositoryBase,IUserDal
    {
        private readonly IUserDal _ntcDal;
        public EfUserDal(IUserDal ntcDal)
        {
            _ntcDal = ntcDal;
        }
        public User GetUserByEmail(string email)
        {
            try
            {
                var user = _ntcDal.GetUserByEmail(email);
                return user;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
