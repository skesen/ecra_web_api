﻿using NtC.Netuce.Core.DataAccess;
using NtC.Netuce.Entities.ABIS;

namespace NtC.Netuce.DataAccess.Abstract
{
    public interface IUserDal : IEntityRepository
    {
        User GetUserByEmail(string email);
    }
}
