namespace NtC.Netuce.DataAccess.Concrete.EntityFramework
{
    using System;
    using System.Data.Entity.Migrations;

    //  public partial class a : DbMigration
    //  {
    //      public override void Up()
    //      {
    //  #region USP_GetNtCUserByEmail
    //  Sql(@"CREATE PROCEDURE `USP_GetNtCUserByEmail`(mail nvarchar(300))
    //      BEGIN
    //    declare myQuery nvarchar(255);
    //    declare `_rollback` BOOL DEFAULT 0;
    //	declare CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
    //	START TRANSACTION;
    // set @myQuery = concat('select * from user where email =',mail);
    //      IF `_rollback` THEN
    //      ROLLBACK;
    //          ELSE
    //          COMMIT;
    //          END IF;
    //          prepare statement from @myQuery;
    //          execute statement;
    //          deallocate prepare statement;
    //END");
    //  #endregion

    //          #region USP_PostNtCDelete
    //          Sql(@"CREATE PROCEDURE `USP_PostNtCDelete`(tableName nvarchar(50),changeColumn nvarchar(1000))

    //      BEGIN

    //      declare myQuery nvarchar(255);
    //          declare `_rollback` BOOL DEFAULT 0;
    //          declare CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
    //          START TRANSACTION;
    //          set @myQuery = concat('DELETE FROM ', tableName, ' WHERE ', changeColumn);
    //          IF `_rollback` THEN
    //          ROLLBACK;
    //          ELSE
    //          COMMIT;
    //          END IF;
    //          prepare statement from @myQuery;
    //          execute statement;
    //          deallocate prepare statement;
    //          END");
    //          #endregion
    //          #region USP_PostNtCUpdate
    //          Sql(@"CREATE PROCEDURE `USP_PostNtCUpdate`(tableName nvarchar(50),columnValues nvarchar(1000),changeColumn nvarchar(1000))
    //BEGIN
    //declare myQuery nvarchar(255);
    //declare `_rollback` BOOL DEFAULT 0;
    //	declare CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
    //	START TRANSACTION;
    //set @myQuery = concat('UPDATE ',tableName,' Set ',columnValues ,' WHERE ',changeColumn);
    //IF `_rollback` THEN
    //ROLLBACK;
    //ELSE
    //COMMIT;
    //END IF;
    //prepare statement from @myQuery;
    //execute statement;
    //deallocate prepare statement;
    //END");
    //          #endregion
    //          #region USP_PostNtCInsert
    //          Sql(@"CREATE PROCEDURE `USP_PostNtCInsert` (tableName nvarchar(50),columnNames nvarchar(1000),columnValues nvarchar(1000))
    //BEGIN
    //    declare myQuery nvarchar(255);
    //    declare `_rollback` BOOL DEFAULT 0;
    //	declare CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
    //	START TRANSACTION;
    //set @myQuery = concat('INSERT INTO ',tableName,'(',columnNames,') VALUES(',columnValues,')');
    //IF `_rollback` THEN
    //ROLLBACK;
    //ELSE
    //COMMIT;
    //END IF;
    //prepare statement from @myQuery;
    //execute statement;
    //deallocate prepare statement;
    //END");
    //          #endregion
    //          #region USP_GetNtCSearchForFilter
    //          Sql(@"CREATE PROCEDURE `USP_GetNtCSearchForFilter`(field nvarchar(300),tableName nvarchar(50),filter nvarchar(300), sortname nvarchar(350),total int, startcolumn int)
    //      BEGIN
    //   declare myQuery nvarchar(255);

    //         case when (filter is null) then 
    //         set @myQuery = concat('select ',field,' from ',tableName, ' order by ', sortname,' LIMIT ',total,' OFFSET ',startcolumn);	

    //   else 
    //         set @myQuery = concat('select ',field,' from ',tableName,' where ',filter,' order by ', sortname,' LIMIT ',total,' OFFSET ',startcolumn);


    //end case;
    //prepare statement from @myQuery;
    //execute statement;
    //deallocate prepare statement;
    //END");
    //          #endregion
    //          #region USP_GetNtCSearch

    //          Sql(@"CREATE PROCEDURE `USP_GetNtCSearch`(field nvarchar(300),tableName nvarchar(50),columnName nvarchar(50),columnValue nvarchar(50),filter nvarchar(50),sortname nvarchar(350),total int, startcolumn int)
    //      BEGIN
    //    declare myQuery nvarchar(255);
    //    declare `_rollback` BOOL DEFAULT 0;
    //	declare CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
    //	START TRANSACTION;
    //case when (columnName is null) then 
    //   set @myQuery = concat('select ',field,' from ',tableName, ' order by ', sortname,' LIMIT ',total,' OFFSET ',startcolumn);		   
    //when (filter ='like') then   
    //   set @myQuery = concat('select ',field,' from ',tableName,' where ',columnName,' ',filter,' ''%', columnValue , '%''', ' order by ', sortname,' LIMIT ',total,' OFFSET ',startcolumn);		 
    //when (filter ='=') then 
    //   set @myQuery = concat('select ',field,' from ',tableName,' where ',columnName,' ',filter,'''',columnValue,'''', ' order by ', sortname,' LIMIT ',total,' OFFSET ',startcolumn);  		   
    //when (filter ='is') then 
    //   set @myQuery = concat('select ',field,' from ',tableName,' where ',columnName,' ',filter,' ',columnValue,' ', ' order by ', sortname,' LIMIT ',total,' OFFSET ',startcolumn);
    //end case;
    //IF `_rollback` THEN
    //ROLLBACK;
    //ELSE
    //COMMIT;
    //END IF;
    //prepare statement from @myQuery;
    //execute statement;
    //deallocate prepare statement;
    //END");
    //          #endregion
    //          #region USP_GetNtC
    //          Sql(@"CREATE PROCEDURE `USP_GetNtC`(tableName nvarchar(50),columnName nvarchar(50),columnId nvarchar(50))
    //BEGIN
    //    declare myQuery nvarchar(255);
    //	declare `_rollback` BOOL DEFAULT 0;
    //	declare CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
    //	START TRANSACTION;
    //case when (columnName is null) then 
    //          set @myQuery = concat('select * from ',tableName);
    //else
    //    set @myQuery = concat('select * from ',tableName,' where ',columnName,' like ''%',columnId,'%''');
    //end case;
    //IF `_rollback` THEN
    //ROLLBACK;
    //ELSE
    //COMMIT;
    //END IF;
    //prepare statement from @myQuery;
    //execute statement;
    //deallocate prepare statement;
    //END");
    //          #endregion
    //          #region USP_GetRowCount
    //          Sql(@"CREATE PROCEDURE `USP_GetRowCount`(tableName nvarchar(50),columnName nvarchar(50),columnId nvarchar(50))
    //BEGIN
    //	declare myQuery nvarchar(255);
    //	declare `_rollback` BOOL DEFAULT 0;
    //	declare CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
    //	START TRANSACTION;
    //case when (columnName is null) then 
    //   set @myQuery = concat('select count(1) from ',tableName);
    //else
    //   set @myQuery = concat('select count(1) from ',tableName,' where ',columnName,'=',columnId);
    //end case;
    //IF `_rollback` THEN
    //ROLLBACK;
    //ELSE
    //COMMIT;
    //END IF;
    //prepare statement from @myQuery;
    //execute statement;
    //deallocate prepare statement;
    //END");
    //          #endregion

    //      }

    //      public override void Down()
    //      {
    //          Sql("DROP PROCEDURE dbo.USP_GetRowCount");
    //          Sql("DROP PROCEDURE dbo.USP_GetNtC");
    //          Sql("DROP PROCEDURE dbo.USP_GetNtCSearch");
    //          Sql("DROP PROCEDURE dbo.USP_GetNtCSearchForFilter");
    //          Sql("DROP PROCEDURE dbo.USP_PostNtCInsert");
    //          Sql("DROP PROCEDURE dbo.USP_PostNtCUpdate");
    //          Sql("DROP PROCEDURE dbo.USP_PostNtCDelete");
    //      }
    //  }
}
