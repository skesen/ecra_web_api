﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using Ninject;
using Ninject.Modules;
using NtC.Netuce.Core.Abstract;

namespace NtC.Netuce.Core.Concrete.EntityFramework.Utilities.Tests
{
    [TestClass()]
    public class EfNtCDalTests : NinjectModule
    {

        [TestMethod()]
        public void GetTest()
        {
            var kernel = new StandardKernel();
            var result = kernel.Get<EfNtCDal>();



            
            Assert.IsTrue(result.Get("domain", "file", new[] 
            {
                new KeyValuePair<string, string>("where","\"insuranceapplicationdate < '2017-05-01' && insuranceapplicationdate > '2017-01-31'\""),
                new KeyValuePair<string, string>("join","\"file\":\"fileid\",\"arbitration\":\"fileid\""),  
                new KeyValuePair<string, string>("field","\"file\":\"fileno,creator,insuranceapplicationdate\",\"arbitration\":\"arbitrationstatusid,arbitrationno\""), 
            }).Count()>=0);

        }

        public override void Load()
        {
            Bind<INtCDal>().To<EfNtCDal>();
        }
    }
}
