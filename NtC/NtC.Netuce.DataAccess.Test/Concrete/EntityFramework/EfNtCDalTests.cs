﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NtC.Netuce.Core.Utilities.ExtensionMethods;

namespace NtC.Netuce.Core.Concrete.EntityFramework.Tests
{
    [TestClass()]
    public class EfNtCDalTests
    {
        [TestMethod()]
        public void GetNtCUserByEmailTest()
        {
            var aa = "test";
            var bb=aa.GrabFromRight(3);
            var cc = aa.TrimFromRight(2);
            var db = new NtCContext();
            var request = db.GetNtCUserByEmail("'test@netuce.com'");
            Assert.IsTrue(request.email!="");
        }
    }
}