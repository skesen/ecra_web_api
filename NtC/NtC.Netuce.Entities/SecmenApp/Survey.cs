﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NtC.Netuce.Core.Entities;

namespace NtC.Netuce.Entities.SecmenApp
{
    public class Survey : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string surveyid { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string fullname { get; set; }
        public string neighborhoodid { get; set; }
        public string opinionid { get; set; }
        public string comment { get; set; }
        public string request { get; set; }
        public string pollsterid { get; set; }
        public string editorid { get; set; }
        public DateTime date { get; set; }
    }

    public class Opinion : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string opinionid { get; set; }
        public string name { get; set; }
    }
}
