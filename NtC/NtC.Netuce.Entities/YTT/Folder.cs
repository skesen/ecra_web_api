﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NtC.Netuce.Core.Entities;

namespace NtC.Netuce.Entities.YTT
{
    public class File : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string fileid { get; set; }
        public string fileno { get; set; }
        public string clientlicenseplate { get; set; }
        public string opponentlicenseplate { get; set; }
        public string opponentvehicleownername { get; set; }
        public string opponentvehicleownerid { get; set; }
        public string opponentdriverfullname { get; set; }
        public string opponentdriverid { get; set; }
        public string insurancecompanyid { get; set; }
        public string faultyrate { get; set; }
        public string accidentdate { get; set; }
        public string valueloss { get; set; }
        public string laborloss { get; set; }
        public string expercost { get; set; }
        public string insuranceapplicationdate { get; set; }
        public string fleetleasingcompanyid { get; set; }
        public string experuserid { get; set; }
        public string responsibleuserid { get; set; }
        public bool isclosed { get; set; }
    }

    public class FileAccess : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string fileaccessid { get; set; }
        public string fileid { get; set; }
        public string accessibleelementid { get; set; }
        public string accessibleelementtype { get; set; }
    }

    public class FleetLeasingCompany : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string fleetleasingcompanyid { get; set; }
        public string name { get; set; }
    }

    public class InsuranceCompany : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string insurancecompanyid { get; set; }
        public string name { get; set; }
    }

    public class Note : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string noteid { get; set; }
        public string content { get; set; }
        public DateTime date { get; set; }
        public string userid { get; set; }
        public string notedid { get; set; }
        public string notedtable { get; set; }
    }
    public class Comment : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string commentid { get; set; }
        public string content { get; set; }
        public DateTime date { get; set; }
        public string userid { get; set; }
        public string commentedid { get; set; }
        public string commentedtable { get; set; }
    }
}
