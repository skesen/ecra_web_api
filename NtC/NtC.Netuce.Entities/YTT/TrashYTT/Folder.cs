﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NtC.Netuce.Core.Entities;

namespace NtC.Netuce.Entities.YTT.TrashYTT
{
    public class Trash_File : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string fileid { get; set; }
        public string fileno { get; set; }
        public string clientlicenseplate { get; set; }
        public string opponentlicenseplate { get; set; }
        public string opponentvehicleownername { get; set; }
        public string opponentvehicleownerid { get; set; }
        public string opponentdriverfullname { get; set; }
        public string opponentdriverid { get; set; }
        public string insurancecompanyid { get; set; }
        public string faultyrate { get; set; }
        public string accidentdate { get; set; }
        public string valueloss { get; set; }
        public string laborloss { get; set; }
        public string expercost { get; set; }
        public string insuranceapplicationdate { get; set; }
        public string fleetleasingcompanyid { get; set; }
        public string experuserid { get; set; }
        public string responsibleuserid { get; set; }
        public bool isclosed { get; set; }
    }

    public class Trash_FleetLeasingCompany : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string fleetleasingcompanyid { get; set; }
        public string name { get; set; }
    }

    public class Trash_InsuranceCompany : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string insurancecompanyid { get; set; }
        public string name { get; set; }
    }

    public class Trash_Note : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string noteid { get; set; }
        public string content { get; set; }
        public DateTime date { get; set; }
        public string userid { get; set; }
        public string notedid { get; set; }
        public string notedtable { get; set; }
    }

    public class Trash_Comment : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string commentid { get; set; }
        public string content { get; set; }
        public DateTime date { get; set; }
        public string userid { get; set; }
        public string commentedid { get; set; }
        public string commentedtable { get; set; }
    }
}
