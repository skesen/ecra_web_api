﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NtC.Netuce.Core.Entities;

namespace NtC.Netuce.Entities.YTT.TrashYTT
{
    public class Trash_Repo : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string repoid { get; set; }
        public string applicationdate { get; set; }
        public string arbitrationid { get; set; }
        public string repono { get; set; }
        public string repoamount { get; set; }
    }

    public class Trash_Collection : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string collectionid { get; set; }
        public string linkedelementtype { get; set; }
        public string linkedelementid { get; set; }
        public string amount { get; set; }
        public string sourceid { get; set; }
        public string collectiondate { get; set; }
        public string sourcetype { get; set; }
        public string collectionno { get; set; }
    }

    public class Trash_Reminder : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string reminderid { get; set; }

        public string name { get; set; }
        public string note { get; set; }
        public string reminddate { get; set; }
        public string linkedelementtype { get; set; }
        public string linkedelementid { get; set; }
        public DateTime referencedate { get; set; }
    }

    public class Trash_Arbitration : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string arbitrationid { get; set; }
        public string applicationdate { get; set; }
        public string fileid { get; set; }
        public string arbitrationno { get; set; }
        public string arbitrationstatusid { get; set; }
    }

    public class Trash_ArbitrationStatus : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string arbitrationstatusid { get; set; }
        public string name { get; set; }
    }
}
