﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NtC.Netuce.Core.Entities;

namespace NtC.Netuce.Entities.Asoy.TrashAsoy
{
    public class Trash_ContactAdditionPerson : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string contactadditionpersonid { get; set; }
        public string personid { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string jobid { get; set; }
        public string headworkerid { get; set; }
        public string constructionsiteid { get; set; }
        public string tcno { get; set; }
        public string bloodtypeid { get; set; }
        public string dateofemployment { get; set; }
        public string lastdateofemployment { get; set; }
        public string workingstatusid { get; set; }
    }

    public class Trash_Job : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string jobid { get; set; }
        public string name { get; set; }
    }

    public class Trash_BloodType : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string bloodtypeid { get; set; }
        public string name { get; set; }
    }

    public class Trash_WorkingStatus : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string workingstatusid { get; set; }
        public string name { get; set; }
    }
}
