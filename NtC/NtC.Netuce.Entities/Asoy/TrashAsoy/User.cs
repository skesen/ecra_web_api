﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NtC.Netuce.Core.Entities;

namespace NtC.Netuce.Entities.Asoy.TrashAsoy
{
    public class Trash_User : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string userid { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string userroleid { get; set; }
        public string userstatusid { get; set; }
    }
    public class Trash_Team : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string teamid { get; set; }
        public string name { get; set; }

    }
    public class Trash_TeamMember : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string teammemberid { get; set; }
        public string teamid { get; set; }
        public string userid { get; set; }
    }
    public class Trash_FileAccess : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string fileaccessid { get; set; }
        public string fileaccess { get; set; }
        public string fileid { get; set; }
        public string accessibleelementtype { get; set; }
        public string accessibleelementid { get; set; }
    }

    public class Trash_UserStatus : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string userstatusid { get; set; }

        public string name { get; set; }
    }

    public class Trash_UserRole : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string userroleid { get; set; }
        public string name { get; set; }
    }

    public class Trash_RolePermission : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string rolepermissionid { get; set; }
        public bool value { get; set; }
        public string roleactionid { get; set; }
        public string userroleid { get; set; }
    }

    public class Trash_UserRolePermissionGroup : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string userrolepermissiongroupid { get; set; }
        public string name { get; set; }
    }

    public class Trash_UserRolePermission : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string userrolepermissionid { get; set; }
        public string value { get; set; }
        public string userroleactionid { get; set; }
        public string userroleid { get; set; }
    }

    public class Trash_UserRoleAction : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string userroleactionid { get; set; }
        public string name { get; set; }
        public string actiongroupid { get; set; }
        public string description { get; set; }
    }

    public class Trash_UserRoleActionValue : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string userroleactionvalueid { get; set; }
        public string name { get; set; }
    }
    public class Trash_Client
    {
        [Key]
        public string clientid { get; set; }
        [Required]
        public string secret { get; set; }
        [Required]
        [MaxLength(100)]
        public string name { get; set; }
        public string applicationtype { get; set; }
        public bool active { get; set; }
        public int refreshtokenlifetime { get; set; }
        [MaxLength(100)]
        public string allowedorigin { get; set; }
    }

    public class Trash_RefreshToken
    {
        [Key]
        public string refreshtokenid { get; set; }
        [Required]
        [MaxLength(50)]
        public string subject { get; set; }
        [Required]
        [MaxLength(50)]
        public string clientid { get; set; }
        public DateTime issuedutc { get; set; }
        public DateTime expiresutc { get; set; }
        [Required]
        public string protectedticket { get; set; }
    }
}
