﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NtC.Netuce.Core.Entities;

namespace NtC.Netuce.Entities.Asoy.TrashAsoy
{
    public class Trash_Contact : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string contactid { get; set; }
        public string contacttypeid { get; set; }
    }

    public class Trash_ContactType : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string contacttypeid { get; set; }
        public string name { get; set; }
        public string table { get; set; }
    }

    public class Trash_ContactAdditionCompany : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string contactadditioncompanyid { get; set; }
        public string contactid { get; set; }
        public string legalname { get; set; }
        public string taxoffice { get; set; }
        public string taxno { get; set; }
        public string phone { get; set; }
        public string fax { get; set; }
    }
}
