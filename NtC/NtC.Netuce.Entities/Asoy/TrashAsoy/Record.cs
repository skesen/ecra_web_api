﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NtC.Netuce.Core.Entities;

namespace NtC.Netuce.Entities.Asoy.TrashAsoy
{
    public class Trash_Record : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string recordid { get; set; }
        public string transactiontypeid { get; set; }
        public string recordcategoryid { get; set; }
        public string tradingwithid { get; set; }
        public string consructionsiteid { get; set; }
        public string accountid { get; set; }
    }

    public class Trash_RecordCategory : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string recordcategoryid { get; set; }
        public string name { get; set; }
    }

    public class Trash_TransactionType : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string transactiontypeid { get; set; }
        public string name { get; set; }
    }
}
