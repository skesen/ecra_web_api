﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NtC.Netuce.Core.Entities;

namespace NtC.Netuce.Entities.Asoy.TrashAsoy
{
    public class Trash_Account : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string accountid { get; set; }
        public string name { get; set; }
    }
    public class Trash_AccountType : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string accounttypeid { get; set; }
        public string name { get; set; }
    }

    public class Trash_Bank : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string bankid { get; set; }
        public string name { get; set; }
    }

    public class Trash_Currency : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string currencyid { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string symbol { get; set; }
    }

    public class Trash_AccountAdditionBank : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string accountadditionbankid { get; set; }
        public string name { get; set; }
        public string bankid { get; set; }
        public string branch { get; set; }
        public string branchcode { get; set; }
        public string accountno { get; set; }
        public string iban { get; set; }
        public string swiftcode { get; set; }
        public string branchphone { get; set; }
        public string branchfax { get; set; }
        public string customerrepresentative { get; set; }
    }
}
