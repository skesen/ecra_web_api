﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NtC.Netuce.Core.Entities;

namespace NtC.Netuce.Entities.Asoy.TrashAsoy
{
    public class Trash_ConstructionSite : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string constructionsiteid { get; set; }
        public string name { get; set; }
        public string supervisor { get; set; }
    }
}
