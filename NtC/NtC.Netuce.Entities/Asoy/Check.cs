﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NtC.Netuce.Core.Entities;

namespace NtC.Netuce.Entities.Asoy
{
    public class BankCheck : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string bankcheckid { get; set; }
        public string recordid { get; set; }
        public string checkno { get; set; }
        public DateTime date { get; set; }
    }

    public class AccountAdditionCheck : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string accountadditioncheckid { get; set; }
        public string accountid { get; set; }
        public string bankaccountid { get; set; }
    }
}
