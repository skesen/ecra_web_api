﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NtC.Netuce.Core.Entities;

namespace NtC.Netuce.Entities.Asoy
{
    public class Record:BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string recordid { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string transactiontypeid { get; set; }
        public string recordcategoryid { get; set; }
        public string tradingwithid { get; set; }
        public string constructionsiteid { get; set; }
        public string accountid { get; set; }
        public DateTime date { get; set; }
     
        public decimal amount { get; set; }
    }

    public class RecordCategory : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string recordcategoryid { get; set; }
        public string name { get; set; }
    }

    public class TransactionType : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string transactiontypeid { get; set; }
        public string name { get; set; }
    }
}
