﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NtC.Netuce.Core.Entities;

namespace NtC.Netuce.Entities.Asoy
{
    public class TableReplace : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string tablereplaceid { get; set; }
        public string tableid { get; set; }
        public string trash_tableid { get; set; }
        public string tablename { get; set; }
        public string trash_tablename { get; set; }
    }
}
