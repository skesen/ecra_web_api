﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NtC.Netuce.Core.Entities;

namespace NtC.Netuce.Entities.Concrete
{
    public class WorkFlow : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string workflowid { get; set; }
        public string workflowtriggerid { get; set; }
        public string workflowactionid { get; set; }
        public string workflowdata { get; set; }
    }

    public class WorkFlowAction : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string workflowactionid { get; set; }
        public string name { get; set; }

    }
    public class WorkFlowTrigger : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string workflowtriggerid { get; set; }
        public string name { get; set; }
    
    }
    public class WorkFlowData : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string workflowdataid { get; set; }
        public string workflowid { get; set; }
        public string key { get; set; }
        public string value { get; set; }
        public string parentid { get; set; }

    }
}
