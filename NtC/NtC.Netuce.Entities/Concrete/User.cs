﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NtC.Netuce.Core.Entities;

namespace NtC.Netuce.Entities.Concrete
{
    public class User:BaseEntity
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string userid { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string userroleid { get; set; }
        public string status { get; set; }
    }
    public class Team : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string teamid { get; set; }
        public string name { get; set; }
    }
    public class TeamMember : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string teammemberid { get; set; }
        public string teamid { get; set; }
        public string userid { get; set; }
    }
    public class IndividualAccess : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string individualaccessid { get; set; }
        public string individualid { get; set; }
        public string accessid { get; set; }
        public string accessidtype { get; set; }
    }

    public class UserRole : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string userroleid { get; set; }
        public string name { get; set; }
    }

    public class RolePermission : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string rolepermissionid { get; set; }
        public bool value { get; set; }
        public string roleactionid { get; set; }
        public string userroleid { get; set; }
    }

    public class UserRolePermissionGroup : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string userpermissiongroupid { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }

    public class RoleAction : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string roleactionid { get; set; }
        public string name { get; set; }
        public string actiongroupid { get; set; }
        public string description { get; set; }
    }


    public class Client
    {
        [Key]
        public string clientid { get; set; }
        [Required]
        public string secret { get; set; }
        [Required]
        [MaxLength(100)]
        public string name { get; set; }
        public string applicationtype { get; set; }
        public bool active { get; set; }
        public int refreshtokenlifetime { get; set; }
        [MaxLength(100)]
        public string allowedorigin { get; set; }
    }

    public class RefreshToken
    {
        [Key]
        public string refreshtokenid { get; set; }
        [Required]
        [MaxLength(50)]
        public string subject { get; set; }
        [Required]
        [MaxLength(50)]
        public string clientid { get; set; }
        public DateTime issuedutc { get; set; }
        public DateTime expiresutc { get; set; }
        [Required]
        public string protectedticket { get; set; }
    }


}
