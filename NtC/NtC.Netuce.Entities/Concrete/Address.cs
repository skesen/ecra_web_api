﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NtC.Netuce.Entities.Concrete
{
    public class Country
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string countryid { get; set; }
        public string name { get; set; }
    }
    public class District
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string districtid { get; set; }
        public string name { get; set; }
        public string provinceid { get; set; }
    }

    public class Province
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string provinceid { get; set; }
        public string name { get; set; }
        public string countryid { get; set; }
    }
    
}
