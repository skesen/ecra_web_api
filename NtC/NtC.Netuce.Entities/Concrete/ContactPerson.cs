﻿using System.Security.AccessControl;
using NtC.Netuce.Core.Entities;

namespace NtC.Netuce.Entities.Concrete
{
    public class ContactPerson : BaseEntity
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public int? ContactCompanyId { get; set; }
        public string Title { get; set; }
        public string Photo { get; set; }
        public string Mail { get; set; }
        public string MobilePhone { get; set; }
        public string SocialMediaLinks { get; set; }
        public string Address { get; set; }
        public string Comment { get; set; }
        public char Gender { get; set; }
        public string Source { get; set; }
        public int Headed { get; set; }
        public virtual ContactCompany contactcompany { get; set; }
    }
}
