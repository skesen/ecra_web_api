﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NtC.Netuce.Core.Entities;

namespace NtC.Netuce.Entities.Concrete
{
    public class Product:BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string productid { get; set; }

        public string code { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int? count { get; set; }
        public string currency { get; set; }
        public string unit { get; set; }
        public decimal price { get; set; }
        public string productcategoryid { get; set; }
        public string source { get; set; }
    }

    public class ProductTag
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string producttagid { get; set; }
        public string productid { get; set; }
        public string tagid { get; set; }
    }

    public class ProductCategory : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string productcategoryid { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string parentid { get; set; }
    }
    public class ProductAttachment
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string productattachmentid { get; set; }
        public string productid { get; set; }
        public string folderid { get; set; }
        public string url { get; set; }
        public int type { get; set; }
    }
}
