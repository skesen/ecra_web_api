﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NtC.Netuce.Core.Entities;

namespace NtC.Netuce.Entities.Concrete
{
    public class SocialMedia:BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string socialmediaid { get; set; }

        public string name { get; set; }
        public string url { get; set; }
        public string tablename { get; set; }
    }

    public class Phone:BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string phoneid { get; set; }

        public string name { get; set; }
        public string phonenumber { get; set; }
        public string tablename { get; set; }
    }

    public class Address :BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string addressid { get; set; }

        public string name { get; set; }
        public string provinceid { get; set; }
        public string districtid { get; set; }
        public string countryid { get; set; }
        public string openaddress { get; set; }
        public string description { get; set; }
        public string tablename { get; set; }
    }

}
