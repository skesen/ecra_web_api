﻿using NtC.Netuce.Core.Entities;

namespace NtC.Netuce.Entities.Concrete
{
    public class ContactCompany:BaseEntity
    {
        public string Name { get; set; }
        public int Headed { get; set; }
        public string Logo { get; set; }
        public string Mail { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public char StaffCount { get; set; }
        public string Sector { get; set; }
        public string AnnualTurnover { get; set; }//yıllık ciro
        public string Comment { get; set; }
        public string Address { get; set; }
        public string SocialMediaLinks { get; set; }
        public virtual BillingInfo billinginfoes { get; set; }
    }

    public class BillingInfo : BaseEntity
    {
        public string BillingAddress { get; set; }
        public string Title { get; set; }
        public string TaxOffice { get; set; }
        public string TaxNo { get; set; }
        public string MersisNo { get; set; }
    }
}
