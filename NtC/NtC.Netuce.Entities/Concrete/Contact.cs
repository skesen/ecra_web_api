﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NtC.Netuce.Core.Entities;

namespace NtC.Netuce.Entities.Concrete
{
    public class Company:BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string companyid { get; set; }

        public string name { get; set; }
        public string description { get; set; }
        public string logo { get; set; }
        public string contactpersonid { get; set; }
        public string addressid { get; set; }
        public string legaladdress { get; set; }
        public string note { get; set; }
        public string annualincome { get; set; }
        public string employees { get; set; }
        public string companytype { get; set; }
        public string phone { get; set; }
        public string mail { get; set; }
        public string provinceid { get; set; }
        public string districtid { get; set; }
        public string countryid { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string taxno { get; set; }
        public string taxoffice { get; set; }
        public string sectorid { get; set; }
        public string point { get; set; }
        public string address { get; set; }
        public string source { get; set; }
        //public string companyscore { get; set; }
        public string fax { get; set; }
        public string legalname { get; set; }
        public string contactsourceid { get; set; }
        public string contactstatusid { get; set; }

    }

    //public class CompanyScore
    //{
    //    [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    //    public string companyscoreid { get; set; }

    //    public string name { get; set; }
    //}

    public class CompanyTag
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string companytagid { get; set; }
        public string companyid { get; set; }
        public string tagid { get; set; }
    }

    public class Individual : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string individualid { get; set; }

        public string companyid { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string photo { get; set; }
        public string note { get; set; }
        public string title { get; set; }
        public string gender { get; set; }
        public string source { get; set; }
        public char restrictiontype { get; set; }
        public string headed { get; set; }
        public string phone { get; set; }
        public string mobilephone { get; set; }
        public string mail { get; set; }
        public string provinceid { get; set; }
        public string districtid { get; set; }
        public string countryid { get; set; }
        public string state { get; set; }
        public string worktitleid { get; set; }
        public string address { get; set; }
        public string contactsourceid { get; set; }
        public string contactstatusid { get; set; }

    }

    public class IndividualTag
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string individualtagid { get; set; }
        public string individualid { get; set; }
        public string tagid { get; set; }
    }
    

    public class Sector 
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string sectorid { get; set; }
        public string name { get; set; }
    }

    public class WorkTitle
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string worktitleid { get; set; }
        public string name { get; set; }
    }

    public class ContactSource
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string contactsourceid { get; set; }

        public string name { get; set; }
        public string category { get; set; }
    }

    public class ContactStatus
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string contactstatusid { get; set; }

        public string name { get; set; }
        public string category { get; set; }
    }

    //public class CompanyTitle
    //{
    //    [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    //    public string companytitleid { get; set; }
    //    public string name { get; set; }
    //}

    public class Potential : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string potentialid { get; set; }
        public string companyname { get; set; }
        public string individualname { get; set; }
        public string individualsurname { get; set; }
        public string description { get; set; }
        public string provinceid { get; set; }
        public string districtid { get; set; }
        public string countryid { get; set; }
        public string mail { get; set; }
        public string phone { get; set; }
        public string fax { get; set; }
        public string mobilephone { get; set; }
        public string facebookurl { get; set; }
        public string instagramurl { get; set; }
        public string address { get; set; }
        public string twitterurl { get; set; }
        public string linkedinurl { get; set; }
        public string url { get; set; }
        public string annualincome { get; set; }
        public string employees { get; set; }
        public string referencephone { get; set; }
        public string worktitleid { get; set; }
        public string referencemail { get; set; }
        public string reference { get; set; }
        public string departmentid { get; set; }
        public string potentialsourceid { get; set; }
        public string potentialdescription { get; set; }
        public string potentialstateid { get; set; }
        public string potentialstatedescription { get; set; }
        public char restrictiontype { get; set; }
        public string libraryid { get; set; }
        public string visibilityid { get; set; }
        public string headed { get; set; }
    }

    public class Visibility
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string visibilityid { get; set; }

        public string name { get; set; }
        public string category { get; set; }
    }
    public class Department
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string departmentid { get; set; }

        public string name { get; set; }
        public string category { get; set; }
    }
    public class PotentialTag
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string potentialtagid { get; set; }
        public string potentialid { get; set; }
        public string tagid { get; set; }
    }

    public class PotentialSource
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string potentialsourceid { get; set; }

        public string name { get; set; }
        public string category { get; set; }
    }

    public class PotentialState
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string potentialstateid { get; set; }

        public string name { get; set; }
        public string category { get; set; }
    }
    //public class PotentialAddress
    //{
    //    [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    //    public string potentialaddressid { get; set; }
    //    public string potentialid { get; set; }
    //    public string addressid { get; set; }

    //}
    //public class PotentialPhone
    //{
    //    [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    //    public string potentialphoneid { get; set; }
    //    public string potentialid { get; set; }
    //    public string phoneid { get; set; }
    //}

    //public class PotentialSocialMedia
    //{
    //    [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    //    public string potentialsocialmediaid { get; set; }
    //    public string potentialid { get; set; }
    //    public string socialmediaid { get; set; }
    //}

}



