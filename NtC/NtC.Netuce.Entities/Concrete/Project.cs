﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NtC.Netuce.Core.Entities;

namespace NtC.Netuce.Entities.Concrete
{
    public class Project : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string projectid { get; set; }
        public string name { get; set; }
        public int reporter { get; set; }
        public string shortname { get; set; }
        public string label { get; set; }
        public string projectcategoryid { get; set; }
        public string projectmanagerid { get; set; }
        public DateTime? startdate { get; set; }
        public DateTime? enddate { get; set; }
        public char restrictiontype { get; set; } //Görünüm kısıtlama 
    }
    public class Tag : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string tagid { get; set; }
        public string name { get; set; }
        public string category { get; set; }

        public virtual ICollection<Project> projects { get; set; }
    }

    public class ProjectUser : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string projectuserid { get; set; }
        public string projectid { get; set; }
        public string userid { get; set; }
    }

    public class ProjectCategory : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string projectcategoryid { get; set; }
        public string name { get; set; }
    }

    public class ProjectTag
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string projecttagid { get; set; }
        public string projectid { get; set; }
        public string tagid { get; set; }
        
    }

}
