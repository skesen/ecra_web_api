﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NtC.Netuce.Core.Entities;

namespace NtC.Netuce.Entities.Concrete
{
    public class Task : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string taskid { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string projectid { get; set; }
        public string parenttaskid { get; set; }
        public DateTime? startdate { get; set; }
        public DateTime? enddate { get; set; }
        public string reporterid { get; set; }
        public string assigneeid { get; set; }
        public string taskcategoryid { get; set; }
        public string tasktypeid { get; set; }
        public string shortname { get; set; }
        public string priority { get; set; }
        public string repeattypeid { get; set; }
        public string taskstatusid { get; set; }
    }

    public class TaskStatus
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string taskstatusid { get; set; }
        public string name { get; set; }
    }

    public class TaskTag
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string tasktagid { get; set; }
        public string taskid { get; set; }
        public string tagid { get; set; }
    }

    public class TaskType
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string tasktypeid { get; set; }
        public string name { get; set; }
    }
    public class TaskCategory
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string taskcategoryid { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }
    public class TaskLink : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string tasklinkid { get; set; }
        public string taskid { get; set; }
        public string linktype { get; set; }
        public string linkid { get; set; }

    }
    public class TaskAttachment
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string taskattachmentid { get; set; }
        public string taskid { get; set; }
        public string folderid { get; set; }
        public string url { get; set; }
        public int type { get; set; }
        public virtual Task task { get; set; }
    }
    public class TaskComment : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string taskcommentid { get; set; }
        public string taskid { get; set; }
        public string note { get; set; }
        public string userid { get; set; }
    }
    
}
