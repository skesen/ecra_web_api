﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NtC.Netuce.Core.Entities;

namespace NtC.Netuce.Entities.Concrete
{
    public class Comment:BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string commentid { get; set; }
        public string content { get; set; }
        public string date { get; set; }
        public string userid { get; set; }
        public string commentedid { get; set; }
        public string commentedtable { get; set; }
    }
}
