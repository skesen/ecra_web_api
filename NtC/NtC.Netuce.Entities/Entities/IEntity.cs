﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NtC.Netuce.Core.Entities
{
    public interface IRowVersion
    {
        int rowversion { get; set; }
    }
    public interface IBaseEntity:IRowVersion
    {
        string editor { get; set; }
        string creator { get; set; }
        DateTime? datecreated { get; set; }
        DateTime? datemodified { get; set; }
    }
    public class BaseEntity : IBaseEntity
    {
        public BaseEntity()
        {
            datecreated = DateTime.Now;
            datemodified = DateTime.Now;
            creator = "Netuce.Creator";
            editor = "Netuce.Editor";
            rowversion = 1;
        }
       
        [MaxLength(128)]
        public string editor { get; set; }
        [Required, MaxLength(128)]
        public string creator { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? datemodified { get; set; }
        [Required]
        public int rowversion { get; set; }
    }
}
