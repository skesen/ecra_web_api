﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NtC.Netuce.Core.Entities;

namespace NtC.Netuce.Entities.TrashConcrete
{
    public class Trash_Link : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string linkid { get; set; }
        public string alinkid { get; set; }
        public string blinkid { get; set; }
        public string linktypeid { get; set; }
    }

    public class Trash_LinkType : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string linktypeid { get; set; }
        public string name { get; set; }
        public string siblingid { get; set; }
        public string linkcategoryid { get; set; }

    }

    public class Trash_LinkCategory : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string linkcategoryid { get; set; }
        public string alinktable { get; set; }
        public string blinktable { get; set; }
    }
}
