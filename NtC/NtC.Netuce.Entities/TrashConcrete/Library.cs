﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NtC.Netuce.Core.Entities;

namespace NtC.Netuce.Entities.TrashConcrete
{
    public class Trash_Library : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string libraryid { get; set; }

        public string name { get; set; }
        public string path { get; set; }
        public string identity { get; set; }
        public string size { get; set; }
        public string type { get; set; }
        public string extension { get; set; }
    }
}
