﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NtC.Netuce.Entities.TrashConcrete
{
    public class Trash_Country
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string countryid { get; set; }
        public string name { get; set; }
    }
    public class Trash_District
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string districtid { get; set; }
        public string name { get; set; }
        public string provinceid { get; set; }
    }

    public class Trash_Province
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string provinceid { get; set; }
        public string name { get; set; }
        public string countryid { get; set; }
    }
    
}
