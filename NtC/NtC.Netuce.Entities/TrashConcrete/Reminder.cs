﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NtC.Netuce.Core.Entities;

namespace NtC.Netuce.Entities.TrashConcrete
{
    public class Trash_Reminder : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string reminderid { get; set; }

        public string name { get; set; }
        public string note { get; set; }
        public string reminddate { get; set; }
        public DateTime referencedate { get; set; }
    }
}
