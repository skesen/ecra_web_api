﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NtC.Netuce.Core.Entities;

namespace NtC.Netuce.Entities.ABIS
{
    public class Address:BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string addressid { get; set; }
        public string neighborhood{ get; set; }
        public string street { get; set; }
        public string addresstype { get; set; }
        public string doorno { get; set; }
        public string area { get; set; }
    }

    public class Coordinate : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string coordinateid { get; set; }
        public string area { get; set; }
        public string latitude { get; set; }//enlem
        public string longitude { get; set; }//boylam
    }

    public class Neighborhood
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string id { get; set; }
        public string neighborhoodid { get; set; }
        public string neighborhoodname { get; set; }
    }
    public class Street
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string id { get; set; }
        public string streetid { get; set; }
        public string streetname { get; set; }
    }
}
