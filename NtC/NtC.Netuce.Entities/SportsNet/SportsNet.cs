﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NtC.Netuce.Core.Entities;

namespace NtC.Netuce.Entities.SportsNet
{
    public class Category 
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int categoryid { get; set; }
        public string name { get; set; }
    }

    public class Option 
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int optionid { get; set; }
        public int categoryid { get; set; }
        public string fullname { get; set; }
    }

    public class Jury 
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int juryid { get; set; }

        public string name { get; set; }
        public string surname { get; set; }
        public string token { get; set; }
        public bool isactivetoken { get; set; }
    }

    public class JuryLine : BaseEntity
    {
        public int jurylineid { get; set; }
        public int juryid { get; set; }
        public int categoryid { get; set; }
        public int optionid { get; set; }
    }
}
