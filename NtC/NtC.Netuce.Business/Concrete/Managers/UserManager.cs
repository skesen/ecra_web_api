﻿using System;
using NtC.Netuce.Business.Abstract;
using NtC.Netuce.Core.DataAccess.EntityFramework;

using NtC.Netuce.Entities.Asoy;

//using NtC.Netuce.Entities.Concrete;

//using NtC.Netuce.Entities.YTT;


namespace NtC.Netuce.Business.Concrete.Managers
{
    public class UserManager : EfEntityRepositoryBase, IUserService
    {
        public User GetUserByEmail(string email)
        {
            try
            {
                var user = db.GetNtCUserByEmail(email);
                return user;
            }
            catch (Exception)
            {
                return null;
            }
            
        }
    }
}
