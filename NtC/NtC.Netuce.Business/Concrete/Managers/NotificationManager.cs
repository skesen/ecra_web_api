﻿using System;
using System.Collections.Generic;
using NtC.Netuce.Business.Abstract;
using NtC.Netuce.Core.Abstract;
using NtC.Netuce.Core.DataAccess.EntityFramework;

namespace NtC.Netuce.Business.Concrete.Managers
{
    public class NotificationManager : EfEntityRepositoryBase,INtCNotification
    {
        private readonly INotificationDal _ntcDal;
        public NotificationManager(INotificationDal ntcDal)
        {
            _ntcDal = ntcDal;
        }
        public bool SendEmail(string baseUrl, KeyValuePair<string, string>[] model)
        {
            try
            {
                return _ntcDal.SendEmail(baseUrl, model);
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
