﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using NtC.Netuce.Business.Abstract;
using NtC.Netuce.Core.DataAccess.EntityFramework;
using NtC.Netuce.Entities.SportsNet;

namespace NtC.Netuce.Business.Concrete.Managers
{
    public class SportsPollRepo: EfEntityRepositoryBase, ISportsPoll
    {
        
        public bool getvote(string token, IEnumerable<JuryLine> model)
        {
            //var isToken = db.Jury.FirstOrDefault(x => x.token.ToLower() == token.ToLower() && !x.isactivetoken);
            //if (isToken != null && !isToken.isactivetoken)
            //{
            //    foreach (var juryLine in model)
            //    {
            //        juryLine.juryid = isToken.juryid;
            //        Create(juryLine);

            //    }

            //    isToken.isactivetoken = true;
            //    db.Entry(isToken).State = EntityState.Modified;
            //    Save();
            //    return true;
            //}

            return false;
        }

        public IEnumerable<object> oykullanmayanjuriler()
        {
            return db.Jury.Where(x => x.isactivetoken == false).Select(x=>new
            {
                JuriAdi=x.name
            });
        }
    }
}
