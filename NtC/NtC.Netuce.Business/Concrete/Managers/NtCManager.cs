﻿using System;
using System.Collections.Generic;
using NtC.Netuce.Business.Abstract;
using NtC.Netuce.Core.Abstract;
using NtC.Netuce.Core.DataAccess.EntityFramework;

namespace NtC.Netuce.Business.Concrete.Managers
{
    public class NtCManager : EfEntityRepositoryBase, INtCService
    {
        private readonly INtCDal _ntcDal;
        public NtCManager(INtCDal ntcDal)
        {
            _ntcDal = ntcDal;
        }

        public object Post(string baseUrl, string table, KeyValuePair<string, string>[] model, string data)
        {
            try
            {
                return _ntcDal.Post(baseUrl,table, model, data);
            }
            catch (Exception)
            {
                return "Ekleme işleminde bir hata oluştu. Serkan Kesen'e Başvurunuz :)";
            }
        }

        public int Count(string table, KeyValuePair<string, string>[] keyValuePairs)
        {
            try
            {
                return _ntcDal.Count(table, keyValuePairs);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public IEnumerable<object> Get(string baseUrl,string table, KeyValuePair<string, string>[] keyValuePairs)
        {
            try
            {
                return _ntcDal.Get(baseUrl,table, keyValuePairs);
            }
            catch (Exception)
            {
                return null;
                throw;
            }
        }

        public object CreateView(string baseUrl, string viewname, KeyValuePair<string, string>[] model)
        {
            try
            {
                return _ntcDal.CreateView(baseUrl, viewname, model);
            }
            catch (Exception)
            {
                return "Ekleme işleminde bir hata oluştu. Serkan Kesen'e Başvurunuz :)";
            }
        }
    }
}
