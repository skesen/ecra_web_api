﻿using System.Collections.Generic;
using NtC.Netuce.Entities.SportsNet;

namespace NtC.Netuce.Business.Abstract
{
    public interface ISportsPoll
    {
        bool getvote(string token, IEnumerable<JuryLine> model);
        IEnumerable<object> oykullanmayanjuriler();
    }
}
