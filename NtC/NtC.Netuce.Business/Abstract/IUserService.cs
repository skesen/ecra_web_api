﻿using NtC.Netuce.Entities.Asoy;

//using NtC.Netuce.Entities.YTT;

//using NtC.Netuce.Entities.Concrete;

namespace NtC.Netuce.Business.Abstract
{
    public interface IUserService
    {
        User GetUserByEmail(string email);
    }
}
