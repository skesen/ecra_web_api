﻿using System.Collections.Generic;

namespace NtC.Netuce.Business.Abstract
{

    public interface INtCService
    {
        object Post(string baseUrl, string table, KeyValuePair<string, string>[] model, string data);
        int Count(string table, KeyValuePair<string, string>[] keyValuePairs);
        IEnumerable<object> Get(string baseUrl, string table,KeyValuePair<string, string>[] keyValuePairs);
        object CreateView(string baseUrl, string viewname, KeyValuePair<string, string>[] model);

    }
}
