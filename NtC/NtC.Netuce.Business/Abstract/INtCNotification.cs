﻿using System.Collections.Generic;

namespace NtC.Netuce.Business.Abstract
{
    public interface INtCNotification
    {
        bool SendEmail(string baseUrl, KeyValuePair<string, string>[] model);

    }
}
