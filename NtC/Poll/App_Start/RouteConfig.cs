﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Poll
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");



            routes.MapRoute(
               "Anket",
               "anket/{id}",
                new { controller = "Poll", action = "Anket", id = UrlParameter.Optional },

               new string[] { "Poll.Controllers" });
            routes.MapRoute(
             name: "Default",
             url: "{controller}/{action}/{id}",
             defaults: new { controller = "Poll", action = "Index", id = UrlParameter.Optional },
             namespaces: new string[] { "Poll.Controllers" });
        }
    }
}
