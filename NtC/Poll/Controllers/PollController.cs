﻿using System.Collections.Generic;
using System.Web.Mvc;
using Newtonsoft.Json;
using NtC.Netuce.Business.Abstract;
using NtC.Netuce.Entities.SportsNet;

namespace Poll.Controllers
{
    public class PollController : Controller
    {
        private ISportsPoll repo;
        public PollController(ISportsPoll repo)
        {
            this.repo = repo;
        }
        // GET: Poll
        public JsonResult setpoll(string token,string model)
        {
            var obj = JsonConvert.DeserializeObject<IEnumerable<JuryLine>>(model);
         
            var query = obj != null && repo.getvote(token, obj);
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        public JsonResult oykullanmayanjuriler()
        {
            return Json(repo.oykullanmayanjuriler(), JsonRequestBehavior.AllowGet);
        }
      
        public ActionResult Anket(string id)
        {
            return View();
        }
    }
}