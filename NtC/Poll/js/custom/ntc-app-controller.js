/*
Created by Netuce | ©2017
info@netuce.com
www.netuce.com
*/
$(document).ready(function () {
    var pathArray = window.location.pathname.split('/');
    sessionStorage.setItem("token",pathArray[2]);
    changePage(); 
    pullSubmit();
});
params = {};
function pullSubmit(){
    $('#ntc-pull').submit(function(e){
        e.preventDefault;
        var pullObject = $('form').serializeArray();
        var token = sessionStorage.getItem("token");
        if (pullObject.length == 7) {
            var model = [];
            $.each(pullObject, function (index, value) {
                var splt = value.value.split("-");
                model.push({ categoryid: splt[0].replace("q", ""), optionid: splt[1].replace("o", "") });
            });
            params.token = token;
            params.model =  JSON.stringify(model);
            $.get("/poll/setpoll", $.param(params, true), function (data) {
                if (data) {
                    redirectPage('#ntci-thank-you');
                    console.log("başarılı");
                } else {
                    alert("Oylama bitmiştir. Teşekkürler.");
                    location.reload();
                }

            }).error(function (jqXHR, textStatus, errorThrown) {
                params = {}
                alert("Hata Mesajı:" + errorThrown);
            }).complete(function () {
            });
           
        }else{
            alert('İşleminizi tamamlayabilmeniz için tüm kategorilerilerde seçim yapmalısınız.');
        }
        
        return false;
    });
};

