﻿using System;
using System.Web.Mvc;
using Ninject;
using NtC.Netuce.Business.Abstract;
using NtC.Netuce.Business.Concrete.Managers;

namespace Poll.Infrastructre
{
    public class NinjectFactory : DefaultControllerFactory
    {
        private IKernel ninjectKernel;
        public NinjectFactory()
        {
            ninjectKernel = new StandardKernel();
            AddBindings();
        }
        protected override IController GetControllerInstance(System.Web.Routing.RequestContext requestContext, Type controllerType)
        {
            return controllerType == null
            ? null
            : (IController)ninjectKernel.Get(controllerType);
        }

        private void AddBindings()
        {
            ninjectKernel.Bind<ISportsPoll>().To<SportsPollRepo>();

        }
    }
}