﻿using System.Collections.Generic;

namespace NtC.Netuce.Core.CrossCuttingConcern.EmailSender
{
    public class CustomMail : BaseEmail
    {

        public CustomMail(string body, string to,string displayname,string cc,string bcc,string subject)
        {
            Body = Resources.CustomMail.Body;
            Replacements.Add("{CONTENT}", body);
            To = to;
            Subject = subject;
            DisplayName = displayname;
            Cc = cc;
            

            CreateEmail();


        }
        public CustomMail(string body, List<string> toList)
        {
            Body = Resources.CustomMail.Body;
            Replacements.Add("{CONTENT}", body);
            ToList = toList;

            CreateEmail();
        }

        public CustomMail(string body, string to, IEnumerable<KeyValuePair<string, string>> replacements)
        {
            Body = Resources.CustomMail.Body;
            Replacements.Add("{CONTENT}", body);
            To = to;

            foreach (var replacement in replacements)
            {
                Replacements.Add(replacement);
            }

            CreateEmail();
        }


    }
}
