﻿using NtC.Netuce.Core.Properties;

namespace NtC.Netuce.Core.CrossCuttingConcern.EmailSender
{
    public class InvitationEmail:BaseEmail
    {
        public InvitationEmail(string userid, string username, string email, string token)
        {
            From = Settings.Default.NoReply;
            Body = Resources.InvitationEmail.ecradegerkaybibody;
            Subject = Resources.InvitationEmail.Subject;
            DisplayName = Resources.InvitationEmail.DisplayName;
            Replacements.Add("{{TOKEN}}", token);
            Replacements.Add("{{USERID}}", userid);
            Replacements.Add("{{USERNAME}}", username);
            To = email;
            
            CreateEmail();
        }
    }
}
