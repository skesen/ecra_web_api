﻿using System;
using System.Collections;
using System.Data.SqlClient;
using System.Text;
using System.Web.Services.Protocols;
using MySql.Data.MySqlClient;
using NtC.Netuce.Core.Properties;

namespace NtC.Netuce.Core.CrossCuttingConcern.EmailSender
{
    public class ExceptionEmail : BaseEmail
    {
        public ExceptionEmail(Exception ex, string subject)
        {
            Subject = subject;
            Construct(ex, string.Empty, string.Empty);
        }



        public ExceptionEmail(Exception ex, string subject, string message, string url)
        {

            Subject = subject;
            Construct(ex, url, message);

        }

        public ExceptionEmail(Exception ex, string subject, string message)
        {

            Subject = subject;
            Construct(ex, string.Empty, message);

        }

        public ExceptionEmail(Exception ex)
        {
            Construct(ex, string.Empty, string.Empty);
            Subject = "An error has occured";
        }

        private void Construct(Exception ex, string url, string message)
        {
            var sb = new StringBuilder();
            sb.Append("An Error has occurred. Relax, everything is going to be OK");
            sb.Append("<br /><br />");
            sb.Append(message);
            if (!string.IsNullOrEmpty(url))
            {
                sb.Append("<br /><br />URL: ");
                sb.Append(url);
                sb.Append("<br />");
            }
            sb.Append("<br /><br />Source: <br />");
            sb.Append(ex.Source);
            sb.Append("<br />Message: <br />");
            sb.Append(ex.Message);
            sb.Append("<br />Stack: <br />");
            sb.Append(ex.StackTrace);

            if (ex.InnerException != null)
            {
                sb.Append("<br />Inner Stack: <br />");
                sb.Append(ex.InnerException.StackTrace);
                sb.Append("<br /><br />Inner Source: <br />");
                sb.Append(ex.InnerException.Source);
                sb.Append("<br />Inner Error: <br />");
                sb.Append("<br />Inner Message: <br />");
                sb.Append(ex.InnerException.Message);
                sb.Append("<br />Inner Stack: <br />");
                sb.Append(ex.InnerException.StackTrace);

            }

            if (ex.Data.Count > 0)
            {
                sb.Append("<br /><br />Exception Data keys: <br />");
                foreach (DictionaryEntry entry in ex.Data)
                {
                    sb.Append("Key: ");
                    sb.Append(entry.Key);
                    sb.Append("<br />");
                    sb.Append("Value: ");
                    sb.Append(entry.Value);
                    sb.Append("<br /><br />");
                }
            }

            if (ex.InnerException != null && ex.InnerException.Data.Count > 0)
            {
                sb.Append("<br /><br />InnerException Data keys: <br />");
                foreach (DictionaryEntry entry in ex.InnerException.Data)
                {
                    sb.Append("Key: ");
                    sb.Append(entry.Key);
                    sb.Append("<br />");
                    sb.Append("Value: ");
                    sb.Append(entry.Value);
                    sb.Append("<br /><br />");
                }
            }

            if (ex is SqlException)
            {
                AppendSQLInfo((SqlException)ex, sb);
            }

            if (ex is SoapException)
            {
                AppendSOAPInfo((SoapException)ex, sb);
            }
            if (ex is MySqlException)
            {
                AppendMySQLInfo((MySqlException)ex, sb);
            }
            Body = sb.ToString();
            From = Settings.Default.NoReply;
            To = Settings.Default.serkanmail;
            ToList.Add(Settings.Default.serkanmail);

            CreateEmail();
        }

        private static void AppendSOAPInfo(SoapException ex, StringBuilder sb)
        {
            sb.Append("SOAP info: \n");
            sb.Append(ex.Detail.InnerXml);
        }

        private static void AppendSQLInfo(SqlException ex, StringBuilder sb)
        {
            foreach (SqlError error in ex.Errors)
            {
                sb.Append("Procedure: \n");
                sb.Append(error.Procedure);
            }
        }
        private static void AppendMySQLInfo(MySqlException ex, StringBuilder sb)
        {
            var exc = new MySqlError(ex.HelpLink, ex.Number, ex.Message);

            sb.Append("Message: \n");
            sb.Append(exc.Message);
            sb.Append("Code: \n");
            sb.Append(exc.Code);
            sb.Append("Level: \n");
            sb.Append(exc.Level);
        }
    }
}
