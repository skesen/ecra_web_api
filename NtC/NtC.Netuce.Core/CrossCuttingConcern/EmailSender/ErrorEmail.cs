﻿using System.Collections.Generic;
using NtC.Netuce.Core.Properties;

namespace NtC.Netuce.Core.CrossCuttingConcern.EmailSender
{
    public class ErrorEmail : BaseEmail
    {
        public ErrorEmail(string baseURl,string message)
        {
            From = Settings.Default.NoReply;
            Body = Resources.Test.Body;
            Subject = Resources.Test.Subject;
            DisplayName = Resources.Test.DisplayName;
            Replacements.Add("{ERROR}", message);
            Replacements.Add("{DOMAIN}","Request Url : "+baseURl);
            var str = message.Contains("YTT_")
                ? new List<string>
                {
                    "serkankesen@netuce.com",
                    "kutaykilic@netuce.com",
                    "ahmetdogan@netuce.com",
                }
                : new List<string>
                {
                    "ahmetdogan@netuce.com",
                    "serkankesen@netuce.com",
                    "kutaykilic@netuce.com"
                };
            ToList = str;
            CreateEmail();
        }
    }
}
