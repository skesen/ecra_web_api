﻿using System;

namespace NtC.Netuce.Core.CrossCuttingConcern.EmailSender
{
    public static class Emailer
    {

        public static void SendException(string message, Exception ex)
        {

            var mailEx = new Exception(message, ex);
            var email = new ExceptionEmail(mailEx);
            email.Send();

        }

        public static void SendException(string subject, string message, Exception ex)
        {

            var email = new ExceptionEmail(ex, subject, string.Empty, message);
            email.Send();

        }
        public static void SendException(string subject, Exception ex, string url)
        {
            var email = new ExceptionEmail(ex, url, subject);
            email.Send();
        }


        public static void SendException(Exception ex)
        {

            var email = new ExceptionEmail(ex);
            email.Send();

        }






    }
}
