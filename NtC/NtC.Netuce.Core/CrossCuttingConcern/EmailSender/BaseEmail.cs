﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using NtC.Netuce.Core.Concrete.EntityFramework;
using NtC.Netuce.Core.Properties;
using NtC.Netuce.Core.Utilities.EnumUtilities;

namespace NtC.Netuce.Core.CrossCuttingConcern.EmailSender
{
    public class BaseEmail
    {

        protected IDictionary<string, string> Replacements = new Dictionary<string, string>();
        protected string Body = string.Empty;
        protected MailMessage BaseMessage = new MailMessage();
        protected string From = string.Empty;
        protected string To = string.Empty;
        protected string Cc = string.Empty;
        public string Subject = string.Empty;
        protected List<String> ToList = new List<string>();
        protected string DisplayName = "NtC";


        protected void CreateEmail()
        {
            if (string.IsNullOrEmpty(Body)) throw new ArgumentException("Body is not loaded.");

            BaseMessage.Body = Body;
            BaseMessage.Subject = Subject;
            BaseMessage.IsBodyHtml = true;
            BaseMessage.BodyEncoding = Encoding.UTF8;
            SetFromAndTo();
            AddRSContactReplacement();
            MakeReplacements();
        }

        protected void AddRSContactReplacement()
        {
            Replacements.Add("{ADDRESS}", Resources.BaseMail.Address);
            Replacements.Add("{TELEPHONE}", Resources.BaseMail.Telephone);
            //Replacements.Add("{FAX}", Resources.BaseMail.Fax);
        }

        protected void SetFromAndTo()
        {
            if (ToList.Count == 0)
            {
                BaseMessage.To.Add(string.IsNullOrEmpty(To) ? Settings.Default.To : To);
            }
            else
            {
                foreach (var to in ToList)
                {
                    BaseMessage.To.Add(to);
                }
            }

            if (!string.IsNullOrEmpty(Cc))
            {
                BaseMessage.CC.Add(Cc);
            }

            BaseMessage.From = string.IsNullOrEmpty(From) ? new MailAddress(Settings.Default.From, DisplayName) : new MailAddress(From, From);

        }


        protected void MakeReplacements()
        {
            var newBody = Replacements.Aggregate(BaseMessage.Body, (current, pair) => current.Replace(pair.Key, pair.Value));
            BaseMessage.Body = newBody;

        }


        

        public void Send(Enums.EmailType type = Enums.EmailType.error)
        {

            if (BaseMessage.Subject != Subject && this.Subject.Length > 0)
                BaseMessage.Subject = Subject;
            if (BaseMessage.To.Count < 1) return;

            var client = new SmtpClient();

            if (string.IsNullOrEmpty(client.Host))
            {
                client.Host = Settings.Default.SMTP;
            }

            if (client.Port == 25)
            {
                client.Port = Settings.Default.Port;
            }
            client.UseDefaultCredentials = false;
            if (type==Enums.EmailType.invitation && (NtCContext.contextpath== "YTT_Prod" || NtCContext.contextpath == "YTT_Dev"))
            {
                client.Credentials = new NetworkCredential("crm@ytthukuk.com", "2017@2017");
            }
            else
            {
                client.Credentials = new NetworkCredential("ecranetuce@gmail.com", "Serkan.1");
            }



            client.EnableSsl = true;

            try
            {
                client.Send(BaseMessage);
            }
            catch
            {
                SendBackUpEmail();
            }

        }

        private void SendBackUpEmail()
        {
            var client = new SmtpClient();

            var message = new MailMessage();
            message.To.Add(new MailAddress(Settings.Default.To));
            message.To.Add(new MailAddress(Settings.Default.serkanmail));
            message.From = new MailAddress(Settings.Default.From);

            string body = Settings.Default.BackupBody;

            body = body.Replace("{EMAIL}", BaseMessage.To[0].Address);
            body = body.Replace("{BODY}", BaseMessage.Body);

            message.Body = body;

            if (string.IsNullOrEmpty(client.Host))
            {
                client.Host = Settings.Default.SMTP;
            }

            if (client.Port == 25)
            {
                client.Port = Settings.Default.Port;
            }

            client.EnableSsl = true;

            try
            {
                client.Send(BaseMessage);
            }
            catch
            {
                // Empty
            }


        }


    }
}
