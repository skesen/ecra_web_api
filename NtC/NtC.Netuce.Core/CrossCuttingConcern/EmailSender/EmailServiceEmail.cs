﻿using System.Collections.Generic;
using System.Net.Mail;
using NtC.Netuce.Core.Utilities.EnumUtilities;

namespace NtC.Netuce.Core.CrossCuttingConcern.EmailSender
{
    public class EmailServiceEmail : BaseEmail
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="body"></param>
        /// <param name="fromEmail"></param>
        /// <param name="subject"></param>
        /// <param name="receivers">Dictionary object of email receivers. Key is type, and Value is email address</param>
        public EmailServiceEmail(string body, string fromEmail, string subject, Dictionary<Enums.EmailAddressType, string> receivers)
        {
            Body = body;
            From = fromEmail;
            Subject = subject;

            foreach (var destination in receivers)
            {
                switch (destination.Key)
                {
                    case Enums.EmailAddressType.To:
                        ToList.Add(destination.Value);
                        break;
                    case Enums.EmailAddressType.CC:
                        BaseMessage.CC.Add(new MailAddress(destination.Value));
                        break;
                    case Enums.EmailAddressType.BCC:
                        BaseMessage.Bcc.Add(new MailAddress(destination.Value));
                        break;
                }
            }

            CreateEmail();

        }

    }
}
