﻿using NtC.Netuce.Core.Properties;

namespace NtC.Netuce.Core.CrossCuttingConcern.EmailSender
{
    public class TaskOperationEmail:BaseEmail
    {
        public TaskOperationEmail(string issue,string username,string owneruser,string url)
        {
            From = Settings.Default.NoReply;
            Body = Resources.TaskOperationEmail.Body;
            Subject = issue;
            DisplayName = Resources.TaskOperationEmail.DisplayName;
            Replacements.Add("{URL}", url);
            Replacements.Add("{USERNAME}",username);
            To = owneruser;
            CreateEmail();
        }
    }
}
