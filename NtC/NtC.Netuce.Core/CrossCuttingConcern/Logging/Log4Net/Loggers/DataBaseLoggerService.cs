﻿using log4net;

namespace NtC.Netuce.Core.CrossCuttingConcern.Logging.Log4Net.Loggers
{
    public class DataBaseLoggerService : LoggerService
    {
        public DataBaseLoggerService() : base(LogManager.GetLogger("DataBaseLogger"))
        {
            
        }
    }
}
