﻿// ***********************************************************************
// Assembly         : NtC.Netuce.DataAccess
// Author           : Serkan KESEN
// Created          : 12-29-2016
//
// Last Modified By : Serkan KESEN
// Last Modified On : 01-30-2017
// ***********************************************************************
// <copyright file="EfNtCDal.cs" company="Netuce">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using NtC.Netuce.Core.CrossCuttingConcern.EmailSender;
using NtC.Netuce.Core.DataAccess.EntityFramework;
using NtC.Netuce.Core.Utilities.ExtensionMethods;
using NtC.Netuce.Core.Abstract;

namespace NtC.Netuce.Core.Concrete.EntityFramework.Utilities
{
    /// <summary>
    /// Class EfNtCDal. Management Layer
    /// </summary>
    /// <seealso cref="EfEntityRepositoryBase" />
    /// <seealso cref="INtCDal" />
    public class EfNtCDal : EfEntityRepositoryBase, INtCDal
    {
        #region GlobalVariables
        /// <summary>
        /// The table
        /// </summary>
        private string _join = "";
        /// <summary>
        /// The table
        /// </summary>
        private string _leftjoin = "";
        /// <summary>
        /// The table
        /// </summary>
        private string _rightjoin = "";
        /// <summary>
        /// The filter
        /// </summary>
        private string _filter = "=";
        /// <summary>
        /// The range
        /// </summary>
        private string _range = null;
        /// <summary>
        /// The field
        /// </summary>
        private string _field = "*";
        /// <summary>
        /// The columnkeys
        /// </summary>
        private string _columnkeys;
        /// <summary>
        /// The columnvalues
        /// </summary>
        private string _columnvalues;
        /// <summary>
        /// The method
        /// </summary>
        private string _method = "";
        /// <summary>
        /// The changecolumns
        /// </summary>
        private string _changecolumns = "";
        /// <summary>
        /// The sort
        /// </summary>
        private string _sort = "";
        /// <summary>
        /// The splcol
        /// </summary>
        private Dictionary<string, string> _splcol;
        /// <summary>
        /// The splval
        /// </summary>
        private Dictionary<string, string> _splval;
        /// <summary>
        /// Gets the tableid.
        /// </summary>
        /// <value>The tableid.</value>
        private string Tableid { get; } = Extensions.Createkey().ToLower();

        #endregion

        #region GetTransaction
        /// <summary>
        /// Gets the specified table.
        /// </summary>
        /// <param name="table">The table.</param>
        /// <param name="keyValuePairs">The key value pairs.</param>
        /// <returns>IEnumerable&lt;System.Object&gt;.</returns>
        public IEnumerable<object> Get(string baseUrl, string table, KeyValuePair<string, string>[] keyValuePairs)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                IEnumerable<object> retList = new List<object>();
                var isfield = false;
                string jointable = "";
                var fieldname = "";

                if (keyValuePairs == null) return retList;
                foreach (var k in keyValuePairs)
                {
                    if (!isfield)
                        isfield = k.Key.ContainsText("where");

                    if (k.Key.ContainsText("filter"))
                        _filter = k.Value == "equal" ? "=" : k.Value;

                    if (k.Key.ContainsText("field"))
                        _field = k.Value;
                    if (k.Key.ContainsText("sort"))
                        _sort = k.Value;
                    else if (string.IsNullOrEmpty(_sort))
                        _sort = "1";
                    if (k.Key.ContainsText("range"))
                        _range = k.Value;
                    if (k.Key.ContainsText("method"))
                        _method = k.Value;
                    if (k.Key.EqualText("join"))
                    {
                        if (_join == "")
                        {
                            _join = k.Value;
                        }
                        else
                        {
                            _join += "," + k.Value;

                        }
                    }
                    if (k.Key.EqualText("leftjoin"))
                    {
                        if (_leftjoin == "")
                        {
                            _leftjoin = k.Value;

                        }
                        else
                        {
                            _leftjoin += "," + k.Value;

                        }
                    }
                    if (k.Key.EqualText("rightjoin"))
                    {
                        if (_rightjoin == "")
                        {
                            _rightjoin = k.Value;

                        }
                        else
                        {
                            _rightjoin += "," + k.Value;

                        }
                    }
                }

                if (!string.IsNullOrEmpty(_join) || !string.IsNullOrEmpty(_leftjoin) || !string.IsNullOrEmpty(_rightjoin))
                {
                    if (!string.IsNullOrEmpty(_field) && !_field.EqualText("*"))
                    {
                        var cleartext = _field.ObjectClearText();
                        var gs = cleartext.StringClearText();
                        for (int i = 0; i < gs.Count; i++)
                        {
                            //ParseColumnNameForView methodu ile as ifadesi kullanılıyor
                            fieldname += gs.ToList()[i].Value.ParseColumnNameForView("-", gs.ToList()[i].Key);
                        }
                    }
                    else
                        fieldname = "*";
                    fieldname = fieldname == "*" ? "*" : fieldname.Remove(fieldname.Length - 2);
                }


                if (_method == "count")
                {
                    IEnumerable<object> count = new[] { Count(table, keyValuePairs).ToString() };

                    return count;
                }
                if (!string.IsNullOrEmpty(_sort))
                {
                    _sort = _sort.ObjectClearTextForSpace();
                    if (!_sort.Contains("1"))
                    {

                        var tr = _sort.Split(',').ToDictionary(c => c.Split(' ')[0], c => Uri.UnescapeDataString(c.Split(' ')[1]));
                        _sort = "";
                        for (int i = 0; i < tr.Count; i++)
                        {
                            var qu = tr.ToList()[i].Value.ParseColumnNameForSort("=>", tr.ToList()[i].Key,table);
                            _sort = _sort + qu + (i < tr.Count - 1 ? "," : "");
                        }
                    }
                }
                if (keyValuePairs.Length == 0 || string.IsNullOrEmpty(_range))
                {
                    var count = db.USP_GetRowCount(table, null);
                    _range = "0," + count;
                }
                if (keyValuePairs.Length == 0)
                    _sort = " 1 ";

                var parserange = Regex.Split(_range, ",");

                //join içerisindeki tablolar ayrıştırılıyor.
                if (!string.IsNullOrEmpty(_join))
                    jointable = SetJoinTable(jointable, " INNER", _join);
                if (!string.IsNullOrEmpty(_leftjoin))
                    jointable = SetJoinTable(jointable, " LEFT", _leftjoin);
                if (!string.IsNullOrEmpty(_rightjoin))
                    jointable = SetJoinTable(jointable, " RIGHT", _rightjoin);

                if (keyValuePairs.Length > 0)
                {
                    foreach (var k in keyValuePairs)
                    {
                        if (k.Key.ContainsText("where") && isfield)
                        {
                            var cleartext = k.Value.ObjectClearTextForJoinQuery(table);
                            var total = Convert.ToInt32(parserange[1]) - Convert.ToInt32(parserange[0]) + 1;


                            retList = db.USP_GetNtCSearchForFilter(field: string.IsNullOrEmpty(jointable) ? _field : fieldname, tableName: string.IsNullOrEmpty(jointable) ? table : jointable, filter: cleartext, sortname: _sort, total: total, startcolumn: Convert.ToInt32(parserange[0]));


                        }
                        else if (!isfield)
                            retList = db.USP_GetNtCSearch(string.IsNullOrEmpty(jointable) ? _field : fieldname, string.IsNullOrEmpty(jointable) ? table : jointable, null, null, null, _sort, Convert.ToInt32(parserange[1]) - Convert.ToInt32(parserange[0]) + 1, Convert.ToInt32(parserange[0]));
                    }
                }
                else
                    retList = db.USP_GetNtCSearch(string.IsNullOrEmpty(jointable) ? _field : fieldname, string.IsNullOrEmpty(jointable) ? table : jointable, null, null, null, _sort, Convert.ToInt32(parserange[1]) - Convert.ToInt32(parserange[0]) + 1, Convert.ToInt32(parserange[0]));

                watch.Stop();
                return retList;
            }
            catch (Exception ex)
            {
                watch.Stop();
                //var output = JsonConvert.SerializeObject(keyValuePairs);
                string parameters = string.Join(";", keyValuePairs.Select(x => x.Key + "=" + x.Value).ToArray());
                var elapsedMs = watch.ElapsedMilliseconds;
                var email = new ErrorEmail(baseUrl, "Islem yapilan sure: " + elapsedMs + "ms<br>Gelen parametreler:" + parameters + "<br>Ram'de duruma ayrilan alan:" + GC.GetTotalMemory(true) + " byte<br> Kayit cekerken problem olustu. " + NtCContext.contextpath + " : " + ex);
                email.Send();
                throw ex;
            }

        }

        public string SetJoinTable(string jointable, string jointype, string contextofjoin)
        {
            var cleartext = contextofjoin.ObjectClearText();
            var gs = cleartext.StringClearTextForJoin(jointype);
          

            return gs;
        }
        #endregion

        #region GetOperations 
        /// <summary>
        /// Request'e verilecek result'un hazırlanması. key parametresi işlenerek döngüde tüketiliyor.
        /// </summary>
        /// <param name="key">The dic.</param>
        /// <param name="field">The field.</param>
        /// <param name="table">The table.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="sort">The sort.</param>
        /// <param name="total">The total.</param>
        /// <param name="startcolumn">The startcolumn.</param>
        /// <returns>IEnumerable&lt;System.Object&gt;.</returns>
        [Obsolete("Bu metod ön tarafta geliştirildi.")]
        public IEnumerable<object> fieldsrepeat(KeyValuePair<string, string> key, string field, string table, string filter, string sort, int total, int startcolumn)
        {
            try
            {
                IEnumerable<object> retList = null;
                var colVal = Regex.Split(key.Value, ",");
                if (colVal.Length > 0)
                {
                    foreach (var s in colVal)
                    {
                        if (s == "null")
                            filter = "is";
                        var sclear = s.Replace("'", string.Empty);
                        var retListsub = db.USP_GetNtCSearch(field, table, key.Key, sclear, filter, sort, total, startcolumn);
                        //CALL `ntc_ecra`.`USP_GetNtCSearch`("*", "sector", "sectorid", "z54s34c","=", 1, 0);
                        retList = retList == null ? retListsub : retList.Union(retListsub);
                    }
                }
                else
                {
                    retList = db.USP_GetNtCSearch(field, table, key.Key, key.Value, filter, sort, total, startcolumn);
                    var enumerable = retList as object[] ?? retList.ToArray();
                    retList = enumerable.Union(enumerable);
                }
                return retList;
            }
            catch (Exception ex)
            {
                //var email = new Test("Kayit çekerken problem olustu. " + ex);
                //email.Send();
                throw;
            }

        }
        #endregion

        #region PostTransaction
        /// <summary>
        /// Functions performs all post processes (Create, Update, Delete).
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="model">QueryString Parameters</param>
        /// <param name="data">Data to be changed</param>
        /// <returns>The process itself</returns>
        public object Post(string baseUrl, string table, KeyValuePair<string, string>[] model, string data)
        {
            try
            {
                string columnvalues = null;
                var isfield = false;
                //model: querystring
                //model hangi metodu çalıştıracak: metod = Create, Update, Count, Trash
                //model hangi sorguları içeriyor: where
                foreach (var k in model)
                {
                    if (!isfield)
                        isfield = k.Key.ContainsText("where");

                    if (k.Key.ContainsText("method"))
                        _method = k.Value.ToLower();
                    if (k.Key.ContainsText("where"))
                    {
                        _changecolumns = k.Value.Replace("{", "").Replace("}", "").Replace(":", "=");
                        _splcol = _changecolumns.Replace(@"""", "").Replace(@"'", "").StringToDictionary();
                    }
                }

                if (!string.IsNullOrEmpty(data) && data != "null")
                {
                    var cleartext = data.Replace("{", "").Replace("}", "").Replace(":", "=");
                    columnvalues = cleartext;
                    var gs = cleartext.Replace(@"""", "").StringToDictionary();
                    if (_method == "update")
                    {
                        _columnvalues = SplitDataUpdate(gs).Item1;
                        _columnkeys = SplitDataUpdate(gs).Item2;
                    }
                    else
                    {
                        _columnvalues = SplitDataCreate(gs, table).Item1;
                        _columnkeys = SplitDataCreate(gs, table).Item2;
                    }

                }
                switch (_method)
                {
                    case "create":
                    case "insert":
                        db.USP_PostNtCInsert(table, _columnkeys.Replace(@"""", ""), _columnvalues);

                        return db.USP_GetNtCByID(table, table + "id", Tableid);

                    case "count":
                        return Count(table, model);

                    case "update":
                        if (_changecolumns == null)
                            return false; // güncellenecek.

                        _splval = columnvalues.StringToDictionary();
                        var wherequeryupdate =
                            _changecolumns.ObjectClearText().Replace("\"", "");

                        var updatedata = SplitDataMultiUpdate(_splval);
                        db.USP_PostNtCUpdate(table, updatedata, wherequeryupdate);
                        //CALL `ntc_ecra`.`USP_PostNtCUpdate`("individual", "phone='3232'", "individualid='ind1'");
                        return Get(baseUrl, table, model);
                    case "trash":

                        var wherequery =
                            _changecolumns.ObjectClearText().Replace("\"", "");

                        var fortrash = db.USP_GetNtCForWhere(table, wherequery);
                        if (!fortrash.ToList().Any())
                            return new { message = string.Empty, answer = "1" }; //data bulunamayınca boş mesaj dönüyor.
                        var datam = Extensions.ConvertExpandotoJson(fortrash);
                        var trashtableid = "";

                        var cleartext = datam.Replace("{", "").Replace("}", "").Replace("[", "").Replace("]", "").Replace(":", "=");
                        var gs = cleartext.Replace(@"""", "").StringToDictionary();
                        var ks = new Dictionary<string, string>();
                        foreach (var item in gs)
                        {
                            if (!string.IsNullOrEmpty(item.Value) && item.Value != "null")
                            {

                                if (item.Key.Equals("datecreated"))
                                {
                                    ks.Add(item.Key, DateTime.Now.ToString());
                                }
                                else if (item.Key.Equals("datemodified"))
                                {
                                    ks.Add(item.Key, DateTime.Now.ToString());
                                }
                                else if (item.Key.Equals(table + "id"))
                                {
                                    ks.Add(item.Key, "tr_" + item.Value);
                                    trashtableid = item.Value;
                                }
                                else
                                {
                                    ks.Add(item.Key, item.Value);
                                }
                            }

                        }

                        _columnvalues = SplitDataDelete(ks, table).Item1;
                        _columnkeys = SplitDataDelete(ks, table).Item2;

                        db.USP_PostNtCInsert("tablereplace", "tablereplaceid,tableid,trash_tableid,tablename,trash_tablename,datecreated", "'" + Tableid + "','" + trashtableid + "','tr_" + trashtableid + "','" + table + "','trash_" + table + "','" + DateTime.Now + "'");
                        db.USP_PostNtCInsert("trash_" + table, _columnkeys.Replace(@"""", ""), _columnvalues);
                        _changecolumns = Regex.Split(_changecolumns, "=")[0].Replace("\"", "") + "=" + Regex.Split(_changecolumns, "=")[1].Replace("\"", "'");
                        db.USP_PostNtCDelete(table, wherequery);
                        var retremoveddata = gs;
                        //using postman: api/user?where={"userid":"u12ser3"}&method=trash
                        return retremoveddata;


                }
                return "Method gönderilmedi yada model oluşturulmadı.";
            }
            catch (Exception ex)
            {
                var parameters = string.Join(";", model.Select(x => x.Key + "=" + x.Value).ToArray());
                var email = new ErrorEmail(baseUrl, "Kayit olusturulurken problem olustu. <br>Gelen parametreler:" + parameters + "<br>" + NtCContext.contextpath + " : " + ex);
                email.Send();
                return new { message = ex, answer = "0" };
            }
        }

        #endregion

        #region PostOperations
        /// <summary>
        /// Update Fonksiyonu için Sql Injection'dan koruma
        /// Gelen datayı temizle
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns>Tuple&lt;System.String, System.String&gt;.</returns>
        private Tuple<string, string> SplitDataUpdate(Dictionary<string, string> data)
        {
            string _whereval = null, _wherekey = null;
            var _retvalue = "";
            foreach (var txt in data)
            {
                string[] colVal = { };
                string[] colkey = { };
                if (txt.Value == "\"\"")
                {
                    colVal = Regex.Split("", ",");
                    colkey = Regex.Split(txt.Key, ",");
                }
                else
                {
                    var regex = new Regex("\"(.*)\"");
                    var value = regex.Match(txt.Value == "" ? "null" : txt.Value).ToString();
                    var nullclear = Regex.Match(value, string.Format("{0}(.*){1}", "\"", "\"")).Groups[1].Value;


                    value = nullclear == "" ?
                        (txt.Value == "" ? "null" : txt.Value) :
                        nullclear;
                    colVal = Regex.Split(value.Replace(",", @"[[***]]").Replace("'", @"\'"), ",");
                    colkey = Regex.Split(txt.Key, ",");
                }

                foreach (var s in colVal)
                {
                    if (_whereval == null)
                        _whereval = s.Contains("null") ? "NULL" : s.ToLower().Equals("true") ? "'1'" : "'" + s.Replace(@"[[***]]", @"\,") + "'";
                    else
                        _whereval = _whereval + ",'" + (s.Contains("null") ? "NULL" : s.ToLower().Equals("true") ? "'1'" : s.Replace(@"[[*,*]]", @"\,") + "'");
                }
                foreach (var s in colkey)
                {
                    var key = Regex.Match(s, string.Format("{0}(.*){1}", "\"", "\"")).Groups[1].Value;
                    key = key == "" ? s : key;
                    if (_wherekey == null)
                        _wherekey = key;
                    else
                        _wherekey = _wherekey + "," + key;
                }
                if (string.IsNullOrEmpty(_retvalue))
                {
                    _retvalue = HttpUtility.UrlDecode(_wherekey, System.Text.Encoding.Default).Replace("\"", "") + " = " +
                                HttpUtility.UrlDecode(_whereval, System.Text.Encoding.Default).Replace("\\\"", "");
                }
                else
                {
                    _retvalue += " , " + HttpUtility.UrlDecode(_wherekey, System.Text.Encoding.Default).Replace("\"", "") + "=" +
                                HttpUtility.UrlDecode(_whereval, System.Text.Encoding.Default).Replace("\\\"", "");
                }
            }

            return new Tuple<string, string>(HttpUtility.UrlDecode(_wherekey, System.Text.Encoding.Default), HttpUtility.UrlDecode(_whereval, System.Text.Encoding.Default));
        }

        /// <summary>
        /// Multi field update function
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private string SplitDataMultiUpdate(Dictionary<string, string> data)
        {

            var _retvalue = "";
            foreach (var txt in data)
            {
                string _whereval = null, _wherekey = null;
                string[] colVal = { };
                string[] colkey = { };
                if (txt.Value == "\"\"")
                {
                    colVal = Regex.Split("", ",");
                    colkey = Regex.Split(txt.Key, ",");
                }
                else
                {
                    var regex = new Regex("\"(.*)\"");
                    var value = regex.Match(txt.Value == "" ? "null" : txt.Value).ToString();
                    var nullclear = Regex.Match(value, string.Format("{0}(.*){1}", "\"", "\"")).Groups[1].Value;


                    value = nullclear == "" ?
                        (txt.Value == "" ? "null" : txt.Value) :
                        nullclear;
                    colVal = Regex.Split(value.Replace(",", @"[[***]]").Replace("'", @"\'"), ",");
                    colkey = Regex.Split(txt.Key, ",");
                }

                foreach (var s in colVal)
                {
                    if (_whereval == null)
                        _whereval = s.Contains("null") ? "NULL" : s.ToLower().Equals("true") ? "'1'" : "'" + s.Replace(@"[[***]]", @"\,") + "'";
                    else
                        _whereval = _whereval + ",'" + (s.Contains("null") ? "NULL" : s.ToLower().Equals("true") ? "'1'" : s.Replace(@"[[*,*]]", @"\,") + "'");
                }
                foreach (var s in colkey)
                {
                    var key = Regex.Match(s, string.Format("{0}(.*){1}", "\"", "\"")).Groups[1].Value;
                    key = key == "" ? s : key;
                    if (_wherekey == null)
                        _wherekey = key;
                    else
                        _wherekey = _wherekey + "," + key;
                }
                if (string.IsNullOrEmpty(_retvalue))
                {
                    _retvalue = HttpUtility.UrlDecode(_wherekey, System.Text.Encoding.Default).Replace("\"", "") + " = " +
                                HttpUtility.UrlDecode(_whereval, System.Text.Encoding.Default).Replace("\\\"", "");
                }
                else
                {
                    _retvalue += " , " + HttpUtility.UrlDecode(_wherekey, System.Text.Encoding.Default).Replace("\"", "") + "=" +
                                HttpUtility.UrlDecode(_whereval, System.Text.Encoding.Default).Replace("\\\"", "");
                }
            }

            return _retvalue;
        }
        /// <summary>
        /// Create Fonksiyonu için Sql Injection'dan koruma
        /// Gelen datayı temizle
        /// </summary>
        /// <param name="data">Gelen ham data</param>
        /// <param name="table">Tablo adı</param>
        /// <returns>Tuple&lt;System.String, System.String&gt;.</returns>
        private Tuple<string, string> SplitDataCreate(Dictionary<string, string> data, string table)
        {
            string _whereval = null, _wherekey = null;
            foreach (var value in data)
            {
                var val = Regex.Match(value.Value, "(.*)").Groups[1].Value;
                val = val == "" ? (value.Value == "" ? null : value.Value) : val;
                var colVal = Regex.Split(val.Replace(",", @"[[***]]").Replace("'", @"[[**]]")/*.Replace("\"\"","\"").Replace("\"\'", "\'").Replace("\'\"", "\'").Replace("'", @"\'")*/, ",");
                var colname = Regex.Split(value.Key, ",");
                foreach (var s in colVal)
                {
                    if (_whereval == null)
                        _whereval = "'" + Tableid + "','" + s.Replace(@"[[***]]", @"\,").Replace(@"[[**]]", @"\'")/*.Replace(@"""","\"").Replace("\\\"","")*/+ "'";
                    else
                        _whereval = _whereval + ",'" + s.Replace(@"[[***]]", @"\,").Replace(@"[[**]]", @"\'")/*.Replace(@"""", "\"").Replace("\\\"", "")*/ + "'";
                }
                foreach (var s in colname)
                {
                    if (_wherekey == null)
                        _wherekey = table.ToLower() + "id," + s;
                    else
                        _wherekey = _wherekey + "," + s;
                }
            }
            return new Tuple<string, string>(HttpUtility.UrlDecode(_whereval, System.Text.Encoding.Default), HttpUtility.UrlDecode(_wherekey.Replace("\\r\\n", "").Replace("\"", ""), System.Text.Encoding.Default));
        }
        /// <summary>
        /// Delete Fonksiyonu için Sql Injection'dan koruma
        /// Gelen datayı temizle
        /// </summary>
        /// <param name="data">Gelen ham data</param>
        /// <param name="table">Tablo adı</param>
        /// <returns>Tuple&lt;System.String, System.String&gt;.</returns>
        private Tuple<string, string> SplitDataDelete(Dictionary<string, string> data, string table)
        {
            string _whereval = null, _wherekey = null;
            foreach (var value in data)
            {
                var val = Regex.Match(value.Value, "(.*)").Groups[1].Value;
                val = val == "" ? (value.Value == "" ? null : value.Value) : val;
                var colVal = Regex.Split(val.Replace(",", @"[[***]]").Replace("'", @"[[**]]")/*.Replace("\"\"","\"").Replace("\"\'", "\'").Replace("\'\"", "\'").Replace("'", @"\'")*/, ",");
                var colname = Regex.Split(value.Key, ",");
                foreach (var s in colVal)
                {
                    if (_whereval == null)
                        _whereval = "'" + s.Replace(@"[[***]]", @"\,").Replace(@"[[**]]", @"\'")/*.Replace(@"""","\"").Replace("\\\"","")*/+ "'";
                    else
                        _whereval = _whereval + ",'" + s.Replace(@"[[***]]", @"\,").Replace(@"[[**]]", @"\'")/*.Replace(@"""", "\"").Replace("\\\"", "")*/ + "'";
                }
                foreach (var s in colname)
                {
                    if (_wherekey == null)
                        _wherekey = s;
                    else
                        _wherekey = _wherekey + "," + s;
                }
            }
            return new Tuple<string, string>(HttpUtility.UrlDecode(_whereval, System.Text.Encoding.Default), HttpUtility.UrlDecode(_wherekey.Replace("\\r\\n", "").Replace("\"", ""), System.Text.Encoding.Default));
        }
        #endregion

        #region ViewOperations
        public object CreateView(string baseUrl, string viewname, KeyValuePair<string, string>[] keyValuePairs)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                IEnumerable<object> retList = new List<object>();
                var isfield = false;
                var jointable = "";
                var fieldname = "";
                if (keyValuePairs == null) return retList;
                foreach (var k in keyValuePairs)
                {
                    if (!isfield)
                        isfield = k.Key.ContainsText("where");

                    if (k.Key.ContainsText("filter"))
                        _filter = k.Value == "equal" ? "=" : k.Value;

                    if (k.Key.ContainsText("field"))
                        _field = k.Value;
                    if (k.Key.ContainsText("join"))
                        _join = k.Value;

                    if (k.Key.ContainsText("range"))
                        _range = k.Value;
                }
                //select sorgusundaki fieldlar popule ediliyor.
                if (!string.IsNullOrEmpty(_field))
                {
                    var cleartext = _field.ObjectClearText();
                    var gs = cleartext.StringClearText();
                    for (int i = 0; i < gs.Count; i++)
                    {
                        //ParseColumnNameForView methodu ile as ifadesi kullanılıyor
                        fieldname += gs.ToList()[i].Value.ParseColumnNameForView("-", gs.ToList()[i].Key);
                    }
                }
                else
                    fieldname = "*";
                fieldname = fieldname == "*" ? "*" : fieldname.Remove(fieldname.Length - 2);

                //join içerisindeki tablolar ayrıştırılıyor.
                //join içerisindeki tablolar ayrıştırılıyor.
                if (!string.IsNullOrEmpty(_join))
                    jointable = SetJoinTable(jointable, " INNER", _join);
                if (!string.IsNullOrEmpty(_leftjoin))
                    jointable = SetJoinTable(jointable, " LEFT", _leftjoin);
                if (!string.IsNullOrEmpty(_rightjoin))
                    jointable = SetJoinTable(jointable, " RIGHT", _rightjoin);

                foreach (var k in keyValuePairs)
                {

                    if (k.Key.ContainsText("where") && isfield)
                    {
                        var cleartext = k.Value.ObjectClearText();
                        var gs = cleartext.StringClearText();
                        var forfilter = "";
                        //where sorgusu için parametreler 1'den fazla ise
                        if (gs.Count > 0)
                        {
                            var i = gs.Count;
                            foreach (var dic in gs)
                            {
                                i--;
                                forfilter += dic.Key + "=" + "'" + dic.Value + "' ";
                                forfilter += i != 0 ? "and " : "";
                            }
                            db.USP_NtCCreateView("CREATE OR REPLACE VIEW " + viewname + " as SELECT " + fieldname + " FROM " + jointable + " where " + forfilter);
                        }
                    }
                }
                if (!isfield)
                    //eğer ilişki yoksa istenen view
                    db.USP_NtCCreateView("CREATE OR REPLACE VIEW " + viewname + " as SELECT " + fieldname + " FROM " + jointable);
                watch.Stop();
                return viewname;
            }
            catch (Exception ex)
            {
                watch.Stop();
                var parameters = string.Join(";", keyValuePairs.Select(x => x.Key + "=" + x.Value).ToArray());
                var elapsedMs = watch.ElapsedMilliseconds;
                var email = new ErrorEmail(baseUrl, "Islem yapilan sure: " + elapsedMs + "ms<br>Gelen parametreler:" + parameters + "<br> View olusturulurken problem olustu. " + NtCContext.contextpath + " : " + ex);
                email.Send();
                throw ex;
            }
        }
        #endregion

        #region Table Count Query

        /// <summary>
        /// Counts the specified table.
        /// </summary>
        /// <param name="table">The table.</param>
        /// <param name="keyValuePairs">The key value pairs.</param>
        /// <returns>System.Int32.</returns>
        public int Count(string table, KeyValuePair<string, string>[] keyValuePairs)
        {
            var isfield = false;
            var tblcount = 0;

            foreach (var k in keyValuePairs)
            {
                if (!isfield)
                    isfield = k.Key.ContainsText("where");

            }

            if (keyValuePairs.Length == 0)
            {
                var count = db.USP_GetRowCount(table, null);
                tblcount = count;
            }
            if (keyValuePairs.Length > 0)
            {
                foreach (var k in keyValuePairs)
                {
                    if (k.Key.ContainsText("where") && isfield)
                    {

                        var cleartext = k.Value.ObjectClearText();

                        tblcount = db.USP_GetRowCount(table, cleartext);

                    }
                }
            }
            return tblcount;
        }

        #endregion
    }
}
