﻿using System;
using System.Collections.Generic;
using System.Linq;
using NtC.Netuce.Core.Abstract;
using NtC.Netuce.Core.CrossCuttingConcern.EmailSender;
using NtC.Netuce.Core.DataAccess.EntityFramework;
using NtC.Netuce.Core.Utilities.ExtensionMethods;

namespace NtC.Netuce.Core.Concrete.EntityFramework.Utilities
{
    public class EfNotificationDal : EfEntityRepositoryBase, INotificationDal
    {
        #region GlobalVariables

        private string _to="";
        private string _subject="";
        private string _cc="";
        private string _bcc="";
        private string _body="";
        private string _displayname="";
        #endregion
        public bool SendEmail(string baseUrl,KeyValuePair<string, string>[] model)
        {
            try
            {
                foreach (var k in model)
                {
                    if (k.Key.ContainsText("to"))
                        _to = k.Value.ToLower();
                    if (k.Key.ContainsText("subject"))
                        _subject = k.Value;
                    if (k.Key.ContainsText("cc"))
                        _cc = k.Value.ToLower();
                    if (k.Key.ContainsText("bcc"))
                        _bcc = k.Value.ToLower();
                    if (k.Key.ContainsText("body"))
                        _body = k.Value;
                    if (k.Key.ContainsText("displayname"))
                        _displayname = k.Value;
                }

                var email = new CustomMail(_body,_to,_displayname,_cc,_bcc,_subject);
                email.Send();
                return true;
            }
            catch (Exception ex)
            {
                var parameters = string.Join(";", model.Select(x => x.Key + "=" + x.Value).ToArray());

                var email = new ErrorEmail(baseUrl, "Mail gönderilirken hata oluştu.<br>Gelen parametreler:" + parameters + "<br>" + NtCContext.contextpath + " : " + ex);
                email.Send();
                return false;
            }
        }
    }
}
