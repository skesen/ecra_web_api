﻿//using System.Collections.Generic;
//using System.Data.Entity.Migrations;
//using System.Linq;

//namespace NtC.Netuce.Core.Concrete.EntityFramework
//{
//    internal sealed class Initializer : DbMigrationsConfiguration<NtCContext>
//    {
//        private readonly bool _pendingMigrations;
//        private readonly IEnumerable<string> _migrationversion;
//        public Initializer()
//        {
//            AutomaticMigrationsEnabled = true;
//            AutomaticMigrationDataLossAllowed = false;//dataları kaybetme
//            var migrator = new DbMigrator(this);
//            ContextKey = "cr_dbhistorykey";
//            var migratorlist = migrator.GetPendingMigrations();
//            var migrationversion = migratorlist as string[] ?? migratorlist.ToArray();
//            _migrationversion =migrationversion ;
//            _pendingMigrations = migrationversion.Any();
//            if (_pendingMigrations)
//            {
//                migrator.Update();
//            }
//        }

//        protected override void Seed(NtCContext context)
//        {
//            if (!_pendingMigrations)
//                return;
//            if (_migrationversion==null)
//            {
//                return;
//            }
//            else
//            {
//                foreach (var version in _migrationversion)
//                {
//                    if (version.Contains("v1.000"))
//                    {
//                        context.Database.ExecuteSqlCommand(TriggerScripts.Trash_TeamMember_AFTER_INSERT); //for all project 
//                        context.Database.ExecuteSqlCommand(TriggerScripts.TeamMember_AFTER_INSERT); //for all project 
//                        context.Database.ExecuteSqlCommand(TriggerScripts.Trash_Team_AFTER_INSERT); //for all project 
//                        context.Database.ExecuteSqlCommand(TriggerScripts.Team_AFTER_INSERT); //for all project 
//                        context.Database.ExecuteSqlCommand(TriggerScripts.tablereplace_AFTER_INSERT); //for all project 
//                        context.Database.ExecuteSqlCommand(TriggerScripts.Reminder_AFTER_INSERT); //for all project 1.03
//                        context.Database.ExecuteSqlCommand(TriggerScripts.Trash_Reminder_AFTER_INSERT); //for all project 1.03

//                        #region StoredProcedure

//                        context.Database.ExecuteSqlCommand(StoreProcedureScripts.GetRowCount);
//                        context.Database.ExecuteSqlCommand(StoreProcedureScripts.GetNtCById);
//                        context.Database.ExecuteSqlCommand(StoreProcedureScripts.GetNtCSearch);
//                        context.Database.ExecuteSqlCommand(StoreProcedureScripts.GetNtCSearchForFilter);
//                        context.Database.ExecuteSqlCommand(StoreProcedureScripts.PostNtCInsert);
//                        context.Database.ExecuteSqlCommand(StoreProcedureScripts.PostNtCUpdate);
//                        context.Database.ExecuteSqlCommand(StoreProcedureScripts.PostNtCDelete);
//                        context.Database.ExecuteSqlCommand(StoreProcedureScripts.CreateNtCCreateView);
//                        context.Database.ExecuteSqlCommand(StoreProcedureScripts.GetNtCForWhere);

//                        #endregion

//                        #region ECRA Triggers

//                        #region T Ecra1.02

//                        context.Database.ExecuteSqlCommand(TriggerScripts.task_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.address_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.comment_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.company_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.individual_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.library_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.link_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.linkcategory_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.linktype_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.phone_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.potential_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.product_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.productcategory_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.roleaction_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.rolepermission_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.socialmedia_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.tasklink_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.userrolepermissiongroup_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.user_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.userrole_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.taskcomment_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.project_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.projectcategory_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.tag_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.IndividualAccess_AFTER_INSERT);

//                        context.Database.ExecuteSqlCommand(TriggerScripts.trash_task_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.trash_address_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.trash_comment_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.trash_company_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.trash_individual_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.trash_library_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.trash_link_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.trash_linkcategory_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.trash_linktype_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.trash_phone_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.trash_potential_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.trash_product_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.trash_productcategory_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.trash_roleaction_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.trash_rolepermission_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.trash_socialmedia_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.trash_tasklink_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.trash_userrolepermissiongroup_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.trash_user_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.trash_userrole_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.trash_taskcomment_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.trash_project_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.trash_projectcategory_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.Trash_IndividualAccess_AFTER_INSERT);

//                        #endregion

//                        #region T Ecra1.03

//                        context.Database.ExecuteSqlCommand(TriggerScripts.WorkFlow_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.WorkFlowAction_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.WorkFlowData_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.WorkFlowTrigger_AFTER_INSERT);

//                        context.Database.ExecuteSqlCommand(TriggerScripts.Trash_WorkFlow_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.Trash_WorkFlowAction_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.Trash_WorkFlowData_AFTER_INSERT);
//                        context.Database.ExecuteSqlCommand(TriggerScripts.Trash_WorkFlowTrigger_AFTER_INSERT);

//                        #endregion

//                        #endregion
//                    }
//                }
//            }


//            #region YTT T 1.02
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_Comment_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_UserRoleActionValue_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_UserRolePermissionGroup_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_UserRolePermission_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_UserRoleAction_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_Arbitration_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_Collection_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_File_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_FleetLeasingCompany_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_InsuranceCompany_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_Repo_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_UserRole_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_UserStatus_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_User_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_FileAccess_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_Note_AFTER_INSERT);

//            //context.Database.ExecuteSqlCommand(TriggerScripts.Comment_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.UserRoleActionValue_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.UserRolePermissionGroup_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.UserRolePermission_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.UserRoleAction_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Arbitration_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Collection_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.File_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.FleetLeasingCompany_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.InsuranceCompany_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Repo_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.UserRole_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.UserStatus_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.User_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.FileAccess_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Note_AFTER_INSERT);

//            #endregion
//            #region ASOY Triggers
//            //context.Database.ExecuteSqlCommand(TriggerScripts.AccountAdditionBank_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.AccountType_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Account_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Bank_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.BloodType_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.ConstructionSite_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.ContactAdditionCompany_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.ContactAdditionPerson_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.ContactType_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Contact_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Currency_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Job_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.RecordCategory_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.TransactionType_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.RecordCategory_AFTER_INSERT);



//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_AccountAdditionBank_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_AccountType_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_Account_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_Bank_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_BloodType_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_ConstructionSite_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_ContactAdditionCompany_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_ContactAdditionPerson_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_ContactType_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_Contact_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_Currency_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_Job_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_RecordCategory_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_TransactionType_AFTER_INSERT);
//            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_RecordCategory_AFTER_INSERT);
//            #endregion



//            context.SaveChanges();
//            base.Seed(context);



//        }

//        public static class StoreProcedureScripts
//        {
//            public const string GetNtCForWhere =
//                @"CREATE PROCEDURE `USP_GetNtCForWhere`(tableName nvarchar(250),wherequery nvarchar(250))
//			BEGIN
//		   declare myQuery nvarchar(255);

//		   case when (wherequery is null) then set @myQuery = concat('select * from ',tableName);
//		   else
//		   set @myQuery = concat('select * from ',tableName,' where ',wherequery);
//		end case;
//		prepare statement from @myQuery;
//		execute statement;
//		deallocate prepare statement;
//		END";

//            public const string GetRowCount = @"
//		CREATE DEFINER=`root`@`%` PROCEDURE `USP_GetRowCount`(tableName nvarchar(250),wherequery nvarchar(250))
//	BEGIN
//		   declare myQuery nvarchar(255);
//		case when (wherequery is null) then set @myQuery = concat('select count(1) from ',tableName);
//		   else
//		   set @myQuery = concat('select count(1) from ',tableName,' where ',wherequery);
//		end case;
//		prepare statement from @myQuery;
//		execute statement;
//		deallocate prepare statement;
//		END";
//            public const string GetNtCUserByEmail = @"CREATE PROCEDURE `USP_GetNtCUserByEmail`(mail nvarchar(300))
//		BEGIN
//			declare myQuery nvarchar(255);
//			declare `_rollback` BOOL DEFAULT 0;
//			declare CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
//			START TRANSACTION;
//		 set @myQuery = concat('select * from user where email =',mail);
//		IF `_rollback` THEN
//		ROLLBACK;
//			ELSE
//			COMMIT;
//			END IF;
//			prepare statement from @myQuery;
//			execute statement;
//			deallocate prepare statement;
//		END";
//            public const string GetNtCById = @"
//		CREATE PROCEDURE `USP_GetNtC`(tableName nvarchar(50),columnName nvarchar(250),columnId nvarchar(50))
//		BEGIN
//			declare myQuery nvarchar(255);
//			declare `_rollback` BOOL DEFAULT 0;
//			declare CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
//			START TRANSACTION;
//		case when (columnName is null) then 
//			set @myQuery = concat('select * from ',tableName);
//		else
//			set @myQuery = concat('select * from ',tableName,' where ',columnName,' like ''%',columnId,'%''');
//		end case;
//		IF `_rollback` THEN
//		ROLLBACK;
//		ELSE
//		COMMIT;
//		END IF;
//		prepare statement from @myQuery;
//		execute statement;
//		deallocate prepare statement;
//		END";

//            public const string GetNtCSearch = @"
//		CREATE PROCEDURE `USP_GetNtCSearch`(field nvarchar(300),tableName nvarchar(50),columnName nvarchar(250),columnValue nvarchar(50),filter nvarchar(50),sortname nvarchar(350),total int, startcolumn int)
//		BEGIN
//			declare myQuery nvarchar(255);
//			declare `_rollback` BOOL DEFAULT 0;
//			declare CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
//			START TRANSACTION;
//		case when (columnName is null) then 
//		   set @myQuery = concat('select ',field,' from ',tableName, ' order by ', sortname,' LIMIT ',total,' OFFSET ',startcolumn);		   
//		when (filter ='like') then   
//		   set @myQuery = concat('select ',field,' from ',tableName,' where ',columnName,' ',filter,' ''%', columnValue , '%''', ' order by ', sortname,' LIMIT ',total,' OFFSET ',startcolumn);		 
//		when (filter ='=') then 
//		   set @myQuery = concat('select ',field,' from ',tableName,' where ',columnName,' ',filter,'''',columnValue,'''', ' order by ', sortname,' LIMIT ',total,' OFFSET ',startcolumn);  		   
//		when (filter ='is') then 
//		   set @myQuery = concat('select ',field,' from ',tableName,' where ',columnName,' ',filter,' ',columnValue,' ', ' order by ', sortname,' LIMIT ',total,' OFFSET ',startcolumn);
//		end case;
//		IF `_rollback` THEN
//		ROLLBACK;
//		ELSE
//		COMMIT;
//		END IF;
//		prepare statement from @myQuery;
//		execute statement;
//		deallocate prepare statement;
//		END";

//            public const string GetNtCSearchForFilter = @"
//		CREATE PROCEDURE `USP_GetNtCSearchForFilter`(field nvarchar(300),tableName nvarchar(50),filter nvarchar(300), sortname nvarchar(350),total int, startcolumn int)
//		BEGIN
//		   declare myQuery nvarchar(255);

//		   case when (filter is null) then 
//		   set @myQuery = concat('select ',field,' from ',tableName, ' order by ', sortname,' LIMIT ',total,' OFFSET ',startcolumn);	

//		   else 
//		   set @myQuery = concat('select ',field,' from ',tableName,' where ',filter,' order by ', sortname,' LIMIT ',total,' OFFSET ',startcolumn);


//		end case;
//		prepare statement from @myQuery;
//		execute statement;
//		deallocate prepare statement;
//		END";


//            public const string PostNtCInsert = @"
//		CREATE PROCEDURE `USP_PostNtCInsert` (tableName nvarchar(50),columnNames nvarchar(1000),columnValues nvarchar(1000))
//		BEGIN
//			declare myQuery nvarchar(255);
//			declare `_rollback` BOOL DEFAULT 0;
//			declare CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
//			START TRANSACTION;
//		set @myQuery = concat('INSERT INTO ',tableName,'(',columnNames,') VALUES(',columnValues,')');
//		IF `_rollback` THEN
//		ROLLBACK;
//		ELSE
//		COMMIT;
//		END IF;
//		prepare statement from @myQuery;
//		execute statement;
//		deallocate prepare statement;
//		END";
//            public const string PostNtCUpdate = @"
//		CREATE PROCEDURE `USP_PostNtCUpdate`(tableName nvarchar(50),columnValues nvarchar(1000),changeColumn nvarchar(1000))
//		BEGIN
//		declare myQuery nvarchar(255);
//		declare `_rollback` BOOL DEFAULT 0;
//			declare CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
//			START TRANSACTION;
//		set @myQuery = concat('UPDATE ',tableName,' Set ',columnValues ,' WHERE ',changeColumn);
//		IF `_rollback` THEN
//		ROLLBACK;
//		ELSE
//		COMMIT;
//		END IF;
//		prepare statement from @myQuery;
//		execute statement;
//		deallocate prepare statement;
//		END";

//            public const string PostNtCDelete = @"
//		CREATE DEFINER=`root`@`%` PROCEDURE `USP_PostNtCDelete`(tableName nvarchar(50),changeColumn nvarchar(1000))
//BEGIN
//		declare myQuery nvarchar(255);
//		declare `_rollback` BOOL DEFAULT 0;
//			declare CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
//			START TRANSACTION;
//		set @myQuery = concat('DELETE FROM ',tableName,' WHERE ' ,changeColumn);
//		 IF `_rollback` THEN
//		ROLLBACK;
//		ELSE
//		COMMIT;
//		END IF;
//		prepare statement from @myQuery;
//		execute statement;
//		deallocate prepare statement;
//		END";


//            public const string CreateNtCCreateView =
//                @"CREATE DEFINER=`root`@`%` PROCEDURE `USP_NtCCreateView`(script nvarchar(4000))
//		Begin
//		declare myQuery nvarchar(255);
//		set @myQuery = concat(script);
//		prepare statement from @myQuery;
//		execute statement;
//		deallocate prepare statement;
//		End";
//        }

//        private static class TriggerScripts
//        {
//            #region Live Trigger Ecra

//            public const string WorkFlow_AFTER_INSERT =
//            @"CREATE TRIGGER WorkFlow_AFTER_INSERT Before INSERT ON workflow FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string WorkFlowAction_AFTER_INSERT =
//            @"CREATE TRIGGER WorkFlowAction_AFTER_INSERT Before INSERT ON workflowaction FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string WorkFlowTrigger_AFTER_INSERT =
//            @"CREATE TRIGGER WorkFlowTrigger_AFTER_INSERT Before INSERT ON workflowtrigger FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string WorkFlowData_AFTER_INSERT =
//            @"CREATE TRIGGER WorkFlowData_AFTER_INSERT Before INSERT ON workflowdata FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";


//            public const string IndividualAccess_AFTER_INSERT =
//            @"CREATE TRIGGER IndividualAccess_AFTER_INSERT Before INSERT ON IndividualAccess FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string address_AFTER_INSERT =
//           @"CREATE TRIGGER address_AFTER_INSERT Before INSERT ON address FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string task_AFTER_INSERT =
//           @"CREATE DEFINER=`root`@`%` TRIGGER task_AFTER_INSERT Before INSERT ON task FOR EACH ROW
//			  begin
//			  declare count int;
//			  if new.projectid='' then
//			  set @count=(select count(*) from task where projectid=new.projectid); 
//			  else
//			   set @count=(select count(*) from task where projectid is null);
//			   end if; 
//			  SET New.datecreated = current_date(), New.datemodified=current_date(), New.shortname=@count+1;

//			  end";

//            public const string comment_AFTER_INSERT =
//           @"CREATE TRIGGER comment_AFTER_INSERT Before INSERT ON comment FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string company_AFTER_INSERT =
//           @"CREATE TRIGGER company_AFTER_INSERT Before INSERT ON company FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string individual_AFTER_INSERT =
//           @"CREATE TRIGGER individual_AFTER_INSERT Before INSERT ON individual FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string library_AFTER_INSERT =
//           @"CREATE TRIGGER library_AFTER_INSERT Before INSERT ON library FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string link_AFTER_INSERT =
//           @"CREATE TRIGGER link_AFTER_INSERT Before INSERT ON link FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string linkcategory_AFTER_INSERT =
//           @"CREATE TRIGGER linkcategory_AFTER_INSERT Before INSERT ON linkcategory FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string linktype_AFTER_INSERT =
//           @"CREATE TRIGGER linktype_AFTER_INSERT Before INSERT ON linktype FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string phone_AFTER_INSERT =
//           @"CREATE TRIGGER phone_AFTER_INSERT Before INSERT ON phone FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string potential_AFTER_INSERT =
//           @"CREATE TRIGGER potential_AFTER_INSERT Before INSERT ON potential FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string product_AFTER_INSERT =
//           @"CREATE TRIGGER product_AFTER_INSERT Before INSERT ON product FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string productcategory_AFTER_INSERT =
//           @"CREATE TRIGGER productcategory_AFTER_INSERT Before INSERT ON productcategory FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string project_AFTER_INSERT =
//           @"CREATE TRIGGER project_AFTER_INSERT Before INSERT ON project FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string projectcategory_AFTER_INSERT =
//           @"CREATE TRIGGER projectcategory_AFTER_INSERT Before INSERT ON projectcategory FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string roleaction_AFTER_INSERT =
//           @"CREATE TRIGGER roleaction_AFTER_INSERT Before INSERT ON roleaction FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string rolepermission_AFTER_INSERT =
//                       @"CREATE TRIGGER rolepermission_AFTER_INSERT Before INSERT ON rolepermission FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string socialmedia_AFTER_INSERT =
//                       @"CREATE TRIGGER socialmedia_AFTER_INSERT Before INSERT ON socialmedia FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string tablereplace_AFTER_INSERT =
//           @"CREATE TRIGGER tablereplace_AFTER_INSERT Before INSERT ON tablereplace FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string tag_AFTER_INSERT =
//                       @"CREATE TRIGGER tag_AFTER_INSERT Before INSERT ON tag FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string taskcomment_AFTER_INSERT =
//                       @"CREATE TRIGGER taskcomment_AFTER_INSERT Before INSERT ON taskcomment FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string tasklink_AFTER_INSERT =
//           @"CREATE TRIGGER tasklink_AFTER_INSERT Before INSERT ON tasklink FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string user_AFTER_INSERT =
//           @"CREATE TRIGGER user_AFTER_INSERT Before INSERT ON user FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string userrole_AFTER_INSERT =
//           @"CREATE TRIGGER userrole_AFTER_INSERT Before INSERT ON userrole FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string userrolepermissiongroup_AFTER_INSERT =
//           @"CREATE TRIGGER userrolepermissiongroup_AFTER_INSERT Before INSERT ON userrolepermissiongroup FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            #endregion
//            #region Trash Trigger Ecra

//            public const string Trash_WorkFlow_AFTER_INSERT =
//           @"CREATE TRIGGER Trash_WorkFlow_AFTER_INSERT Before INSERT ON trash_workflow FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_WorkFlowAction_AFTER_INSERT =
//            @"CREATE TRIGGER Trash_WorkFlowAction_AFTER_INSERT Before INSERT ON trash_workflowaction FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_WorkFlowTrigger_AFTER_INSERT =
//            @"CREATE TRIGGER Trash_WorkFlowTrigger_AFTER_INSERT Before INSERT ON trash_workflowtrigger FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_WorkFlowData_AFTER_INSERT =
//            @"CREATE TRIGGER Trash_WorkFlowData_AFTER_INSERT Before INSERT ON trash_workflowdata FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string Trash_UserRolePermissionGroup_AFTER_INSERT =
//           @"CREATE TRIGGER Trash_UserRolePermissionGroup_AFTER_INSERT Before INSERT ON Trash_UserRolePermissionGroup FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_UserRolePermission_AFTER_INSERT =
//            @"CREATE TRIGGER Trash_UserRolePermission_AFTER_INSERT Before INSERT ON Trash_UserRolePermission FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_UserRoleAction_AFTER_INSERT =
//            @"CREATE TRIGGER Trash_UserRoleAction_AFTER_INSERT Before INSERT ON Trash_UserRoleAction FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_UserRoleActionValue_AFTER_INSERT =
//            @"CREATE TRIGGER Trash_UserRoleActionValue_AFTER_INSERT Before INSERT ON Trash_UserRoleActionValue FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";


//            public const string Trash_IndividualAccess_AFTER_INSERT =
//           @"CREATE TRIGGER Trash_IndividualAccess_AFTER_INSERT Before INSERT ON Trash_IndividualAccess FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string trash_address_AFTER_INSERT =
//           @"CREATE TRIGGER trash_address_AFTER_INSERT Before INSERT ON trash_address FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string trash_task_AFTER_INSERT =
//           @"CREATE TRIGGER trash_task_AFTER_INSERT Before INSERT ON trash_task FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string trash_comment_AFTER_INSERT =
//           @"CREATE TRIGGER trash_comment_AFTER_INSERT Before INSERT ON trash_comment FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string trash_company_AFTER_INSERT =
//           @"CREATE TRIGGER trash_company_AFTER_INSERT Before INSERT ON trash_company FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string trash_individual_AFTER_INSERT =
//           @"CREATE TRIGGER trash_individual_AFTER_INSERT Before INSERT ON trash_individual FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string trash_library_AFTER_INSERT =
//           @"CREATE TRIGGER trash_library_AFTER_INSERT Before INSERT ON trash_library FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string trash_link_AFTER_INSERT =
//           @"CREATE TRIGGER trash_link_AFTER_INSERT Before INSERT ON trash_link FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string trash_linkcategory_AFTER_INSERT =
//           @"CREATE TRIGGER trash_linkcategory_AFTER_INSERT Before INSERT ON trash_linkcategory FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string trash_linktype_AFTER_INSERT =
//           @"CREATE TRIGGER trash_linktype_AFTER_INSERT Before INSERT ON trash_linktype FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string trash_phone_AFTER_INSERT =
//           @"CREATE TRIGGER trash_phone_AFTER_INSERT Before INSERT ON trash_phone FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string trash_potential_AFTER_INSERT =
//           @"CREATE TRIGGER trash_potential_AFTER_INSERT Before INSERT ON trash_potential FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string trash_product_AFTER_INSERT =
//           @"CREATE TRIGGER trash_product_AFTER_INSERT Before INSERT ON trash_product FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string trash_productcategory_AFTER_INSERT =
//           @"CREATE TRIGGER trash_productcategory_AFTER_INSERT Before INSERT ON trash_productcategory FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string trash_project_AFTER_INSERT =
//           @"CREATE TRIGGER trash_project_AFTER_INSERT Before INSERT ON trash_project FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string trash_projectcategory_AFTER_INSERT =
//           @"CREATE TRIGGER trash_projectcategory_AFTER_INSERT Before INSERT ON trash_projectcategory FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string trash_roleaction_AFTER_INSERT =
//           @"CREATE TRIGGER trash_roleaction_AFTER_INSERT Before INSERT ON trash_roleaction FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string trash_rolepermission_AFTER_INSERT =
//                       @"CREATE TRIGGER trash_rolepermission_AFTER_INSERT Before INSERT ON trash_rolepermission FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string trash_socialmedia_AFTER_INSERT =
//                       @"CREATE TRIGGER trash_socialmedia_AFTER_INSERT Before INSERT ON trash_socialmedia FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string trash_tablereplace_AFTER_INSERT =
//           @"CREATE TRIGGER trash_tablereplace_AFTER_INSERT Before INSERT ON trash_tablereplace FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string trash_tag_AFTER_INSERT =
//                       @"CREATE TRIGGER trash_tag_AFTER_INSERT Before INSERT ON trash_tag FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string trash_taskcomment_AFTER_INSERT =
//                       @"CREATE TRIGGER trash_taskcomment_AFTER_INSERT Before INSERT ON trash_taskcomment FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string trash_tasklink_AFTER_INSERT =
//           @"CREATE TRIGGER trash_tasklink_AFTER_INSERT Before INSERT ON trash_tasklink FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string trash_user_AFTER_INSERT =
//           @"CREATE TRIGGER trash_user_AFTER_INSERT Before INSERT ON trash_user FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string trash_userrole_AFTER_INSERT =
//           @"CREATE TRIGGER trash_userrole_AFTER_INSERT Before INSERT ON trash_userrole FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string trash_userrolepermissiongroup_AFTER_INSERT =
//           @"CREATE TRIGGER trash_userrolepermissiongroup_AFTER_INSERT Before INSERT ON trash_userrolepermissiongroup FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            #endregion

//            #region Live Trigger YTT
//            public const string Comment_AFTER_INSERT =
//            @"CREATE TRIGGER Comment_AFTER_INSERT Before INSERT ON Comment FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string UserRolePermissionGroup_AFTER_INSERT =
//            @"CREATE TRIGGER UserRolePermissionGroup_AFTER_INSERT Before INSERT ON UserRolePermissionGroup FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string UserRolePermission_AFTER_INSERT =
//            @"CREATE TRIGGER UserRolePermission_AFTER_INSERT Before INSERT ON UserRolePermission FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string UserRoleAction_AFTER_INSERT =
//            @"CREATE TRIGGER UserRoleAction_AFTER_INSERT Before INSERT ON UserRoleAction FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string UserRoleActionValue_AFTER_INSERT =
//            @"CREATE TRIGGER UserRoleActionValue_AFTER_INSERT Before INSERT ON UserRoleActionValue FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string Note_AFTER_INSERT =
//            @"CREATE TRIGGER Note_AFTER_INSERT Before INSERT ON Note FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string TeamMember_AFTER_INSERT =
//            @"CREATE TRIGGER TeamMember_AFTER_INSERT Before INSERT ON TeamMember FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Team_AFTER_INSERT =
//            @"CREATE TRIGGER Team_AFTER_INSERT Before INSERT ON Team FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string FileAccess_AFTER_INSERT =
//            @"CREATE TRIGGER FileAccess_AFTER_INSERT Before INSERT ON FileAccess FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";


//            public const string File_AFTER_INSERT =
//            @"CREATE TRIGGER File_AFTER_INSERT Before INSERT ON File FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string User_AFTER_INSERT =
//           @"CREATE TRIGGER User_AFTER_INSERT Before INSERT ON User FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string UserStatus_AFTER_INSERT =
//           @"CREATE TRIGGER UserStatus_AFTER_INSERT Before INSERT ON UserStatus FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string UserRole_AFTER_INSERT =
//           @"CREATE TRIGGER UserRole_AFTER_INSERT Before INSERT ON UserRole FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Arbitration_AFTER_INSERT =
//           @"CREATE TRIGGER Arbitration_AFTER_INSERT Before INSERT ON Arbitration FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string InsuranceCompany_AFTER_INSERT =
//           @"CREATE TRIGGER InsuranceCompany_AFTER_INSERT Before INSERT ON InsuranceCompany FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string FleetLeasingCompany_AFTER_INSERT =
//           @"CREATE TRIGGER FleetLeasingCompany_AFTER_INSERT Before INSERT ON FleetLeasingCompany FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Reminder_AFTER_INSERT =
//           @"CREATE TRIGGER Reminder_AFTER_INSERT Before INSERT ON Reminder FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Repo_AFTER_INSERT =
//           @"CREATE TRIGGER Repo_AFTER_INSERT Before INSERT ON Repo FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Collection_AFTER_INSERT =
//           @"CREATE TRIGGER Collection_AFTER_INSERT Before INSERT ON Collection FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            #endregion
//            #region Trash Trigger YTT
//            public const string Trash_Comment_AFTER_INSERT =
//            @"CREATE TRIGGER Trash_Comment_AFTER_INSERT Before INSERT ON Trash_Comment FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_Note_AFTER_INSERT =
//            @"CREATE TRIGGER Trash_Note_AFTER_INSERT Before INSERT ON Trash_Note FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_TeamMember_AFTER_INSERT =
//            @"CREATE TRIGGER Trash_TeamMember_AFTER_INSERT Before INSERT ON Trash_TeamMember FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_Team_AFTER_INSERT =
//            @"CREATE TRIGGER Trash_Team_AFTER_INSERT Before INSERT ON Trash_Team FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_FileAccess_AFTER_INSERT =
//            @"CREATE TRIGGER Trash_FileAccess_AFTER_INSERT Before INSERT ON Trash_FileAccess FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string Trash_File_AFTER_INSERT =
//            @"CREATE TRIGGER Trash_File_AFTER_INSERT Before INSERT ON Trash_File FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            public const string Trash_User_AFTER_INSERT =
//           @"CREATE TRIGGER Trash_User_AFTER_INSERT Before INSERT ON Trash_User FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_UserStatus_AFTER_INSERT =
//           @"CREATE TRIGGER Trash_UserStatus_AFTER_INSERT Before INSERT ON Trash_UserStatus FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_UserRole_AFTER_INSERT =
//           @"CREATE TRIGGER Trash_UserRole_AFTER_INSERT Before INSERT ON Trash_UserRole FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_Arbitration_AFTER_INSERT =
//           @"CREATE TRIGGER Trash_Arbitration_AFTER_INSERT Before INSERT ON Trash_Arbitration FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_InsuranceCompany_AFTER_INSERT =
//           @"CREATE TRIGGER Trash_InsuranceCompany_AFTER_INSERT Before INSERT ON Trash_InsuranceCompany FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_FleetLeasingCompany_AFTER_INSERT =
//           @"CREATE TRIGGER Trash_FleetLeasingCompany_AFTER_INSERT Before INSERT ON Trash_FleetLeasingCompany FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_Reminder_AFTER_INSERT =
//           @"CREATE TRIGGER Trash_Reminder_AFTER_INSERT Before INSERT ON Trash_Reminder FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_Repo_AFTER_INSERT =
//           @"CREATE TRIGGER Trash_Repo_AFTER_INSERT Before INSERT ON Trash_Repo FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_Collection_AFTER_INSERT =
//           @"CREATE TRIGGER Trash_Collection_AFTER_INSERT Before INSERT ON Trash_Collection FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            #endregion

//            #region Live Trigger Asoy
//            public const string Record_AFTER_INSERT =
//            @"CREATE TRIGGER Record_AFTER_INSERT Before INSERT ON Record FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Account_AFTER_INSERT =
//            @"CREATE TRIGGER Account_AFTER_INSERT Before INSERT ON Account FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string AccountAdditionBank_AFTER_INSERT =
//            @"CREATE TRIGGER AccountAdditionBank_AFTER_INSERT Before INSERT ON AccountAdditionBank FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string AccountType_AFTER_INSERT =
//            @"CREATE TRIGGER AccountType_AFTER_INSERT Before INSERT ON AccountType FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Bank_AFTER_INSERT =
//            @"CREATE TRIGGER Bank_AFTER_INSERT Before INSERT ON Bank FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string BloodType_AFTER_INSERT =
//            @"CREATE TRIGGER BloodType_AFTER_INSERT Before INSERT ON BloodType FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string ConstructionSite_AFTER_INSERT =
//            @"CREATE TRIGGER ConstructionSite_AFTER_INSERT Before INSERT ON ConstructionSite FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Contact_AFTER_INSERT =
//            @"CREATE TRIGGER Contact_AFTER_INSERT Before INSERT ON Contact FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string ContactAdditionCompany_AFTER_INSERT =
//            @"CREATE TRIGGER ContactAdditionCompany_AFTER_INSERT Before INSERT ON ContactAdditionCompany FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string ContactAdditionPerson_AFTER_INSERT =
//            @"CREATE TRIGGER ContactAdditionPerson_AFTER_INSERT Before INSERT ON ContactAdditionPerson FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string ContactType_AFTER_INSERT =
//            @"CREATE TRIGGER ContactType_AFTER_INSERT Before INSERT ON ContactType FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Currency_AFTER_INSERT =
//            @"CREATE TRIGGER Currency_AFTER_INSERT Before INSERT ON Currency FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Job_AFTER_INSERT =
//            @"CREATE TRIGGER Job_AFTER_INSERT Before INSERT ON Job FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string RecordCategory_AFTER_INSERT =
//            @"CREATE TRIGGER RecordCategory_AFTER_INSERT Before INSERT ON RecordCategory FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string TransactionType_AFTER_INSERT =
//            @"CREATE TRIGGER TransactionType_AFTER_INSERT Before INSERT ON TransactionType FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string WorkingStatus_AFTER_INSERT =
//            @"CREATE TRIGGER WorkingStatus_AFTER_INSERT Before INSERT ON WorkingStatus FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";

//            #endregion

//            #region Trash Trigger Asoy
//            public const string Trash_Record_AFTER_INSERT =
//            @"CREATE TRIGGER Trash_Record_AFTER_INSERT Before INSERT ON Trash_Record FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_Account_AFTER_INSERT =
//            @"CREATE TRIGGER Trash_Account_AFTER_INSERT Before INSERT ON Trash_Account FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_AccountAdditionBank_AFTER_INSERT =
//            @"CREATE TRIGGER Trash_AccountAdditionBank_AFTER_INSERT Before INSERT ON Trash_AccountAdditionBank FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_AccountType_AFTER_INSERT =
//            @"CREATE TRIGGER Trash_AccountType_AFTER_INSERT Before INSERT ON Trash_AccountType FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_Bank_AFTER_INSERT =
//            @"CREATE TRIGGER Trash_Bank_AFTER_INSERT Before INSERT ON Trash_Bank FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_BloodType_AFTER_INSERT =
//            @"CREATE TRIGGER Trash_BloodType_AFTER_INSERT Before INSERT ON Trash_BloodType FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_ConstructionSite_AFTER_INSERT =
//            @"CREATE TRIGGER Trash_ConstructionSite_AFTER_INSERT Before INSERT ON Trash_ConstructionSite FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_Contact_AFTER_INSERT =
//            @"CREATE TRIGGER Trash_Contact_AFTER_INSERT Before INSERT ON Trash_Contact FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_ContactAdditionCompany_AFTER_INSERT =
//            @"CREATE TRIGGER Trash_ContactAdditionCompany_AFTER_INSERT Before INSERT ON Trash_ContactAdditionCompany FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_ContactAdditionPerson_AFTER_INSERT =
//            @"CREATE TRIGGER Trash_ContactAdditionPerson_AFTER_INSERT Before INSERT ON Trash_ContactAdditionPerson FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_ContactType_AFTER_INSERT =
//            @"CREATE TRIGGER Trash_ContactType_AFTER_INSERT Before INSERT ON Trash_ContactType FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_Currency_AFTER_INSERT =
//            @"CREATE TRIGGER Trash_Currency_AFTER_INSERT Before INSERT ON Trash_Currency FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_Job_AFTER_INSERT =
//            @"CREATE TRIGGER Trash_Job_AFTER_INSERT Before INSERT ON Trash_Job FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_RecordCategory_AFTER_INSERT =
//            @"CREATE TRIGGER Trash_RecordCategory_AFTER_INSERT Before INSERT ON Trash_RecordCategory FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_TransactionType_AFTER_INSERT =
//            @"CREATE TRIGGER Trash_TransactionType_AFTER_INSERT Before INSERT ON Trash_TransactionType FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            public const string Trash_WorkingStatus_AFTER_INSERT =
//            @"CREATE TRIGGER Trash_WorkingStatus_AFTER_INSERT Before INSERT ON Trash_WorkingStatus FOR EACH ROW
//			set New.datecreated = current_date(), New.datemodified=current_date();";
//            #endregion
//        }

//    }


//}



////ALTER TABLE ntc_ytt_prod.arbitration CHARACTER SET utf8 COLLATE utf8_general_ci;
////ALTER TABLE ntc_ytt_prod.client CHARACTER SET utf8 COLLATE utf8_general_ci;
////ALTER TABLE ntc_ytt_prod.collection CHARACTER SET utf8 COLLATE utf8_general_ci;
////ALTER TABLE ntc_ytt_prod.file CHARACTER SET utf8 COLLATE utf8_general_ci;
////ALTER TABLE ntc_ytt_prod.fileaccess CHARACTER SET utf8 COLLATE utf8_general_ci;
////ALTER TABLE ntc_ytt_prod.fleetleasingcompany CHARACTER SET utf8 COLLATE utf8_general_ci;
////ALTER TABLE ntc_ytt_prod.insurancecompany CHARACTER SET utf8 COLLATE utf8_general_ci;
////ALTER TABLE ntc_ytt_prod.note CHARACTER SET utf8 COLLATE utf8_general_ci;
////ALTER TABLE ntc_ytt_prod.refreshtoken CHARACTER SET utf8 COLLATE utf8_general_ci;
////ALTER TABLE ntc_ytt_prod.reminder CHARACTER SET utf8 COLLATE utf8_general_ci;
////ALTER TABLE ntc_ytt_prod.repo CHARACTER SET utf8 COLLATE utf8_general_ci;
////ALTER TABLE ntc_ytt_prod.tablereplace CHARACTER SET utf8 COLLATE utf8_general_ci;
////ALTER TABLE ntc_ytt_prod.team CHARACTER SET utf8 COLLATE utf8_general_ci;
////ALTER TABLE ntc_ytt_prod.teammember CHARACTER SET utf8 COLLATE utf8_general_ci;
////ALTER TABLE ntc_ytt_prod.user CHARACTER SET utf8 COLLATE utf8_general_ci;
////ALTER TABLE ntc_ytt_prod.userrole CHARACTER SET utf8 COLLATE utf8_general_ci;
////ALTER TABLE ntc_ytt_prod.userstatus CHARACTER SET utf8 COLLATE utf8_general_ci;

////ALTER TABLE ntc_ytt_prod.trash_arbitration CHARACTER SET utf8 COLLATE utf8_general_ci;
////ALTER TABLE ntc_ytt_prod.trash_collection CHARACTER SET utf8 COLLATE utf8_general_ci;
////ALTER TABLE ntc_ytt_prod.trash_file CHARACTER SET utf8 COLLATE utf8_general_ci;
////ALTER TABLE ntc_ytt_prod.trash_fileaccess CHARACTER SET utf8 COLLATE utf8_general_ci;
////ALTER TABLE ntc_ytt_prod.trash_fleetleasingcompany CHARACTER SET utf8 COLLATE utf8_general_ci;
////ALTER TABLE ntc_ytt_prod.trash_insurancecompany CHARACTER SET utf8 COLLATE utf8_general_ci;
////ALTER TABLE ntc_ytt_prod.trash_note CHARACTER SET utf8 COLLATE utf8_general_ci;
////ALTER TABLE ntc_ytt_prod.trash_reminder CHARACTER SET utf8 COLLATE utf8_general_ci;
////ALTER TABLE ntc_ytt_prod.trash_repo CHARACTER SET utf8 COLLATE utf8_general_ci;
////ALTER TABLE ntc_ytt_prod.trash_team CHARACTER SET utf8 COLLATE utf8_general_ci;
////ALTER TABLE ntc_ytt_prod.trash_teammember CHARACTER SET utf8 COLLATE utf8_general_ci;
////ALTER TABLE ntc_ytt_prod.trash_user CHARACTER SET utf8 COLLATE utf8_general_ci;
////ALTER TABLE ntc_ytt_prod.trash_userrole CHARACTER SET utf8 COLLATE utf8_general_ci;
////ALTER TABLE ntc_ytt_prod.trash_userstatus CHARACTER SET utf8 COLLATE utf8_general_ci;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;

namespace NtC.Netuce.Core.Concrete.EntityFramework
{
    internal sealed class MyDbInitializer : DbMigrationsConfiguration<NtCContext>
    {

        public MyDbInitializer()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = false;//dataları kaybetme
        }

        protected override void Seed(NtCContext context)
        {
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_TeamMember_AFTER_INSERT);//for all project 
            //context.Database.ExecuteSqlCommand(TriggerScripts.TeamMember_AFTER_INSERT);//for all project 
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_Team_AFTER_INSERT);//for all project 
            //context.Database.ExecuteSqlCommand(TriggerScripts.Team_AFTER_INSERT);//for all project 
            //context.Database.ExecuteSqlCommand(TriggerScripts.tablereplace_AFTER_INSERT);//for all project 
            //context.Database.ExecuteSqlCommand(TriggerScripts.Reminder_AFTER_INSERT);//for all project 1.03
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_Reminder_AFTER_INSERT);//for all project 1.03


            #region YTT T 1.02
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_Comment_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_UserRoleActionValue_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_UserRolePermissionGroup_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_UserRolePermission_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_UserRoleAction_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_Arbitration_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_Collection_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_File_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_FleetLeasingCompany_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_InsuranceCompany_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_Repo_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_UserRole_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_UserStatus_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_User_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_FileAccess_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_Note_AFTER_INSERT);

            //context.Database.ExecuteSqlCommand(TriggerScripts.Comment_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.UserRoleActionValue_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.UserRolePermissionGroup_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.UserRolePermission_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.UserRoleAction_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Arbitration_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Collection_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.File_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.FleetLeasingCompany_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.InsuranceCompany_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Repo_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.UserRole_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.UserStatus_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.User_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.FileAccess_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Note_AFTER_INSERT);

            #endregion
            #region ECRA Triggers
            #region T Ecra1.02

            //context.Database.ExecuteSqlCommand(TriggerScripts.task_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.address_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.comment_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.company_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.individual_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.library_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.link_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.linkcategory_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.linktype_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.phone_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.potential_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.product_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.productcategory_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.roleaction_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.rolepermission_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.socialmedia_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.tasklink_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.userrolepermissiongroup_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.user_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.userrole_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.taskcomment_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.project_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.projectcategory_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.tag_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.IndividualAccess_AFTER_INSERT);

            //context.Database.ExecuteSqlCommand(TriggerScripts.trash_task_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.trash_address_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.trash_comment_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.trash_company_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.trash_individual_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.trash_library_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.trash_link_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.trash_linkcategory_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.trash_linktype_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.trash_phone_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.trash_potential_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.trash_product_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.trash_productcategory_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.trash_roleaction_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.trash_rolepermission_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.trash_socialmedia_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.trash_tasklink_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.trash_userrolepermissiongroup_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.trash_user_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.trash_userrole_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.trash_taskcomment_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.trash_project_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.trash_projectcategory_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_IndividualAccess_AFTER_INSERT);
            #endregion
            #region T Ecra1.03
            //context.Database.ExecuteSqlCommand(TriggerScripts.WorkFlow_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.WorkFlowAction_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.WorkFlowData_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.WorkFlowTrigger_AFTER_INSERT);

            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_WorkFlow_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_WorkFlowAction_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_WorkFlowData_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_WorkFlowTrigger_AFTER_INSERT);
            #endregion
            #endregion
            #region ASOY Triggers
            //context.Database.ExecuteSqlCommand(TriggerScripts.AccountAdditionBank_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.AccountType_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Account_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Bank_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.BloodType_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.ConstructionSite_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.ContactAdditionCompany_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.ContactAdditionPerson_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.ContactType_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Contact_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Currency_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Job_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.RecordCategory_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.TransactionType_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.RecordCategory_AFTER_INSERT);
            context.Database.ExecuteSqlCommand(TriggerScripts.BankCheck_AFTER_INSERT);
            context.Database.ExecuteSqlCommand(TriggerScripts.AccountAdditionCheck_AFTER_INSERT);

            context.Database.ExecuteSqlCommand(TriggerScripts.Trash_BankCheck_AFTER_INSERT);
            context.Database.ExecuteSqlCommand(TriggerScripts.Trash_AccountAdditionCheck_AFTER_INSERT);

            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_AccountAdditionBank_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_AccountType_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_Account_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_Bank_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_BloodType_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_ConstructionSite_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_ContactAdditionCompany_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_ContactAdditionPerson_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_ContactType_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_Contact_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_Currency_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_Job_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_RecordCategory_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_TransactionType_AFTER_INSERT);
            //context.Database.ExecuteSqlCommand(TriggerScripts.Trash_RecordCategory_AFTER_INSERT);
            #endregion
            #region StoredProcedure

            //context.Database.ExecuteSqlCommand(StoreProcedureScripts.GetRowCount);
            //context.Database.ExecuteSqlCommand(StoreProcedureScripts.GetNtCById);
            //context.Database.ExecuteSqlCommand(StoreProcedureScripts.GetNtCSearch);
            //context.Database.ExecuteSqlCommand(StoreProcedureScripts.GetNtCSearchForFilter);
            //context.Database.ExecuteSqlCommand(StoreProcedureScripts.PostNtCInsert);
            //context.Database.ExecuteSqlCommand(StoreProcedureScripts.PostNtCUpdate);
            //context.Database.ExecuteSqlCommand(StoreProcedureScripts.PostNtCDelete);
            //context.Database.ExecuteSqlCommand(StoreProcedureScripts.CreateNtCCreateView);
            //context.Database.ExecuteSqlCommand(StoreProcedureScripts.GetNtCForWhere);
            #endregion
            //context.SaveChanges();
            base.Seed(context);


        }

        public static class StoreProcedureScripts
        {
            public const string GetNtCForWhere =
                @"CREATE PROCEDURE `USP_GetNtCForWhere`(tableName nvarchar(1000),wherequery nvarchar(1000))
            BEGIN
		   declare myQuery nvarchar(255);
		   
           case when (wherequery is null) then set @myQuery = concat('select * from ',tableName);
		   else
		   set @myQuery = concat('select * from ',tableName,' where ',wherequery);
		end case;
		prepare statement from @myQuery;
		execute statement;
		deallocate prepare statement;
		END";

            public const string GetRowCount = @"
		CREATE DEFINER=`root`@`%` PROCEDURE `USP_GetRowCount`(tableName nvarchar(1000),wherequery nvarchar(1000))
    BEGIN
		   declare myQuery nvarchar(255);
		case when (wherequery is null) then set @myQuery = concat('select count(1) from ',tableName);
		   else
		   set @myQuery = concat('select count(1) from ',tableName,' where ',wherequery);
		end case;
		prepare statement from @myQuery;
		execute statement;
		deallocate prepare statement;
		END";
            public const string GetNtCUserByEmail = @"CREATE PROCEDURE `USP_GetNtCUserByEmail`(mail nvarchar(1000))
		BEGIN
			declare myQuery nvarchar(255);
			declare `_rollback` BOOL DEFAULT 0;
			declare CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
			START TRANSACTION;
		 set @myQuery = concat('select * from user where email =',mail);
		IF `_rollback` THEN
		ROLLBACK;
			ELSE
			COMMIT;
			END IF;
			prepare statement from @myQuery;
			execute statement;
			deallocate prepare statement;
		END";
            public const string GetNtCById = @"
		CREATE PROCEDURE `USP_GetNtC`(tableName nvarchar(1000),columnName nvarchar(1000),columnId nvarchar(50))
		BEGIN
			declare myQuery nvarchar(255);
			declare `_rollback` BOOL DEFAULT 0;
			declare CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
			START TRANSACTION;
		case when (columnName is null) then 
			set @myQuery = concat('select * from ',tableName);
		else
			set @myQuery = concat('select * from ',tableName,' where ',columnName,' like ''%',columnId,'%''');
		end case;
		IF `_rollback` THEN
		ROLLBACK;
		ELSE
		COMMIT;
		END IF;
		prepare statement from @myQuery;
		execute statement;
		deallocate prepare statement;
		END";

            public const string GetNtCSearch = @"
		CREATE PROCEDURE `USP_GetNtCSearch`(field nvarchar(1000),tableName nvarchar(1000),columnName nvarchar(1000),columnValue nvarchar(1000),filter nvarchar(1000),sortname nvarchar(350),total int, startcolumn int)
		BEGIN
			declare myQuery nvarchar(255);
			declare `_rollback` BOOL DEFAULT 0;
			declare CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
			START TRANSACTION;
		case when (columnName is null) then 
		   set @myQuery = concat('select ',field,' from ',tableName, ' order by ', sortname,' LIMIT ',total,' OFFSET ',startcolumn);		   
		when (filter ='like') then   
		   set @myQuery = concat('select ',field,' from ',tableName,' where ',columnName,' ',filter,' ''%', columnValue , '%''', ' order by ', sortname,' LIMIT ',total,' OFFSET ',startcolumn);		 
		when (filter ='=') then 
		   set @myQuery = concat('select ',field,' from ',tableName,' where ',columnName,' ',filter,'''',columnValue,'''', ' order by ', sortname,' LIMIT ',total,' OFFSET ',startcolumn);  		   
		when (filter ='is') then 
		   set @myQuery = concat('select ',field,' from ',tableName,' where ',columnName,' ',filter,' ',columnValue,' ', ' order by ', sortname,' LIMIT ',total,' OFFSET ',startcolumn);
		end case;
		IF `_rollback` THEN
		ROLLBACK;
		ELSE
		COMMIT;
		END IF;
		prepare statement from @myQuery;
		execute statement;
		deallocate prepare statement;
		END";

            public const string GetNtCSearchForFilter = @"
		CREATE PROCEDURE `USP_GetNtCSearchForFilter`(field nvarchar(1000),tableName nvarchar(1000),filter nvarchar(1000), sortname nvarchar(350),total int, startcolumn int)
		BEGIN
		   declare myQuery nvarchar(255);
		   
		   case when (filter is null) then 
		   set @myQuery = concat('select ',field,' from ',tableName, ' order by ', sortname,' LIMIT ',total,' OFFSET ',startcolumn);	
		   
		   else 
		   set @myQuery = concat('select ',field,' from ',tableName,' where ',filter,' order by ', sortname,' LIMIT ',total,' OFFSET ',startcolumn);
		  
		
		end case;
		prepare statement from @myQuery;
		execute statement;
		deallocate prepare statement;
		END";


            public const string PostNtCInsert = @"
		CREATE PROCEDURE `USP_PostNtCInsert` (tableName nvarchar(50),columnNames nvarchar(1000),columnValues nvarchar(1000))
		BEGIN
			declare myQuery nvarchar(255);
			declare `_rollback` BOOL DEFAULT 0;
			declare CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
			START TRANSACTION;
		set @myQuery = concat('INSERT INTO ',tableName,'(',columnNames,') VALUES(',columnValues,')');
		IF `_rollback` THEN
		ROLLBACK;
		ELSE
		COMMIT;
		END IF;
		prepare statement from @myQuery;
		execute statement;
		deallocate prepare statement;
		END";
            public const string PostNtCUpdate = @"
		CREATE PROCEDURE `USP_PostNtCUpdate`(tableName nvarchar(50),columnValues nvarchar(1000),changeColumn nvarchar(1000))
		BEGIN
		declare myQuery nvarchar(255);
		declare `_rollback` BOOL DEFAULT 0;
			declare CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
			START TRANSACTION;
		set @myQuery = concat('UPDATE ',tableName,' Set ',columnValues ,' WHERE ',changeColumn);
		IF `_rollback` THEN
		ROLLBACK;
		ELSE
		COMMIT;
		END IF;
		prepare statement from @myQuery;
		execute statement;
		deallocate prepare statement;
		END";

            public const string PostNtCDelete = @"
		CREATE DEFINER=`root`@`%` PROCEDURE `USP_PostNtCDelete`(tableName nvarchar(50),changeColumn nvarchar(1000))
BEGIN
		declare myQuery nvarchar(255);
        declare `_rollback` BOOL DEFAULT 0;
			declare CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
			START TRANSACTION;
		set @myQuery = concat('DELETE FROM ',tableName,' WHERE ' ,changeColumn);
         IF `_rollback` THEN
        ROLLBACK;
		ELSE
        COMMIT;
		END IF;
		prepare statement from @myQuery;
		execute statement;
		deallocate prepare statement;
		END";


            public const string CreateNtCCreateView =
                @"CREATE DEFINER=`root`@`%` PROCEDURE `USP_NtCCreateView`(script nvarchar(4000))
        Begin
		declare myQuery nvarchar(255);
		set @myQuery = concat(script);
        prepare statement from @myQuery;
		execute statement;
		deallocate prepare statement;
        End";
        }

        private static class TriggerScripts
        {
            #region Live Trigger Ecra

            public const string WorkFlow_AFTER_INSERT =
            @"CREATE TRIGGER WorkFlow_AFTER_INSERT Before INSERT ON workflow FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string WorkFlowAction_AFTER_INSERT =
            @"CREATE TRIGGER WorkFlowAction_AFTER_INSERT Before INSERT ON workflowaction FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string WorkFlowTrigger_AFTER_INSERT =
            @"CREATE TRIGGER WorkFlowTrigger_AFTER_INSERT Before INSERT ON workflowtrigger FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string WorkFlowData_AFTER_INSERT =
            @"CREATE TRIGGER WorkFlowData_AFTER_INSERT Before INSERT ON workflowdata FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";


            public const string IndividualAccess_AFTER_INSERT =
            @"CREATE TRIGGER IndividualAccess_AFTER_INSERT Before INSERT ON IndividualAccess FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string address_AFTER_INSERT =
           @"CREATE TRIGGER address_AFTER_INSERT Before INSERT ON address FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string task_AFTER_INSERT =
           @"CREATE DEFINER=`root`@`%` TRIGGER task_AFTER_INSERT Before INSERT ON task FOR EACH ROW
              begin
              declare count int;
              if new.projectid='' then
              set @count=(select count(*) from task where projectid=new.projectid); 
              else
               set @count=(select count(*) from task where projectid is null);
               end if; 
              SET New.datecreated = current_date(), New.datemodified=current_date(), New.shortname=@count+1;
  
              end";

            public const string comment_AFTER_INSERT =
           @"CREATE TRIGGER comment_AFTER_INSERT Before INSERT ON comment FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string company_AFTER_INSERT =
           @"CREATE TRIGGER company_AFTER_INSERT Before INSERT ON company FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string individual_AFTER_INSERT =
           @"CREATE TRIGGER individual_AFTER_INSERT Before INSERT ON individual FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string library_AFTER_INSERT =
           @"CREATE TRIGGER library_AFTER_INSERT Before INSERT ON library FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string link_AFTER_INSERT =
           @"CREATE TRIGGER link_AFTER_INSERT Before INSERT ON link FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string linkcategory_AFTER_INSERT =
           @"CREATE TRIGGER linkcategory_AFTER_INSERT Before INSERT ON linkcategory FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string linktype_AFTER_INSERT =
           @"CREATE TRIGGER linktype_AFTER_INSERT Before INSERT ON linktype FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string phone_AFTER_INSERT =
           @"CREATE TRIGGER phone_AFTER_INSERT Before INSERT ON phone FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string potential_AFTER_INSERT =
           @"CREATE TRIGGER potential_AFTER_INSERT Before INSERT ON potential FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string product_AFTER_INSERT =
           @"CREATE TRIGGER product_AFTER_INSERT Before INSERT ON product FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string productcategory_AFTER_INSERT =
           @"CREATE TRIGGER productcategory_AFTER_INSERT Before INSERT ON productcategory FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string project_AFTER_INSERT =
           @"CREATE TRIGGER project_AFTER_INSERT Before INSERT ON project FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string projectcategory_AFTER_INSERT =
           @"CREATE TRIGGER projectcategory_AFTER_INSERT Before INSERT ON projectcategory FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string roleaction_AFTER_INSERT =
           @"CREATE TRIGGER roleaction_AFTER_INSERT Before INSERT ON roleaction FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string rolepermission_AFTER_INSERT =
                       @"CREATE TRIGGER rolepermission_AFTER_INSERT Before INSERT ON rolepermission FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string socialmedia_AFTER_INSERT =
                       @"CREATE TRIGGER socialmedia_AFTER_INSERT Before INSERT ON socialmedia FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string tablereplace_AFTER_INSERT =
           @"CREATE TRIGGER tablereplace_AFTER_INSERT Before INSERT ON tablereplace FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string tag_AFTER_INSERT =
                       @"CREATE TRIGGER tag_AFTER_INSERT Before INSERT ON tag FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string taskcomment_AFTER_INSERT =
                       @"CREATE TRIGGER taskcomment_AFTER_INSERT Before INSERT ON taskcomment FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string tasklink_AFTER_INSERT =
           @"CREATE TRIGGER tasklink_AFTER_INSERT Before INSERT ON tasklink FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string user_AFTER_INSERT =
           @"CREATE TRIGGER user_AFTER_INSERT Before INSERT ON user FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string userrole_AFTER_INSERT =
           @"CREATE TRIGGER userrole_AFTER_INSERT Before INSERT ON userrole FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string userrolepermissiongroup_AFTER_INSERT =
           @"CREATE TRIGGER userrolepermissiongroup_AFTER_INSERT Before INSERT ON userrolepermissiongroup FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            #endregion
            #region Trash Trigger Ecra

            public const string Trash_WorkFlow_AFTER_INSERT =
           @"CREATE TRIGGER Trash_WorkFlow_AFTER_INSERT Before INSERT ON trash_workflow FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_WorkFlowAction_AFTER_INSERT =
            @"CREATE TRIGGER Trash_WorkFlowAction_AFTER_INSERT Before INSERT ON trash_workflowaction FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_WorkFlowTrigger_AFTER_INSERT =
            @"CREATE TRIGGER Trash_WorkFlowTrigger_AFTER_INSERT Before INSERT ON trash_workflowtrigger FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_WorkFlowData_AFTER_INSERT =
            @"CREATE TRIGGER Trash_WorkFlowData_AFTER_INSERT Before INSERT ON trash_workflowdata FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string Trash_UserRolePermissionGroup_AFTER_INSERT =
           @"CREATE TRIGGER Trash_UserRolePermissionGroup_AFTER_INSERT Before INSERT ON Trash_UserRolePermissionGroup FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_UserRolePermission_AFTER_INSERT =
            @"CREATE TRIGGER Trash_UserRolePermission_AFTER_INSERT Before INSERT ON Trash_UserRolePermission FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_UserRoleAction_AFTER_INSERT =
            @"CREATE TRIGGER Trash_UserRoleAction_AFTER_INSERT Before INSERT ON Trash_UserRoleAction FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_UserRoleActionValue_AFTER_INSERT =
            @"CREATE TRIGGER Trash_UserRoleActionValue_AFTER_INSERT Before INSERT ON Trash_UserRoleActionValue FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";


            public const string Trash_IndividualAccess_AFTER_INSERT =
           @"CREATE TRIGGER Trash_IndividualAccess_AFTER_INSERT Before INSERT ON Trash_IndividualAccess FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string trash_address_AFTER_INSERT =
           @"CREATE TRIGGER trash_address_AFTER_INSERT Before INSERT ON trash_address FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string trash_task_AFTER_INSERT =
           @"CREATE TRIGGER trash_task_AFTER_INSERT Before INSERT ON trash_task FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string trash_comment_AFTER_INSERT =
           @"CREATE TRIGGER trash_comment_AFTER_INSERT Before INSERT ON trash_comment FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string trash_company_AFTER_INSERT =
           @"CREATE TRIGGER trash_company_AFTER_INSERT Before INSERT ON trash_company FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string trash_individual_AFTER_INSERT =
           @"CREATE TRIGGER trash_individual_AFTER_INSERT Before INSERT ON trash_individual FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string trash_library_AFTER_INSERT =
           @"CREATE TRIGGER trash_library_AFTER_INSERT Before INSERT ON trash_library FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string trash_link_AFTER_INSERT =
           @"CREATE TRIGGER trash_link_AFTER_INSERT Before INSERT ON trash_link FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string trash_linkcategory_AFTER_INSERT =
           @"CREATE TRIGGER trash_linkcategory_AFTER_INSERT Before INSERT ON trash_linkcategory FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string trash_linktype_AFTER_INSERT =
           @"CREATE TRIGGER trash_linktype_AFTER_INSERT Before INSERT ON trash_linktype FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string trash_phone_AFTER_INSERT =
           @"CREATE TRIGGER trash_phone_AFTER_INSERT Before INSERT ON trash_phone FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string trash_potential_AFTER_INSERT =
           @"CREATE TRIGGER trash_potential_AFTER_INSERT Before INSERT ON trash_potential FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string trash_product_AFTER_INSERT =
           @"CREATE TRIGGER trash_product_AFTER_INSERT Before INSERT ON trash_product FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string trash_productcategory_AFTER_INSERT =
           @"CREATE TRIGGER trash_productcategory_AFTER_INSERT Before INSERT ON trash_productcategory FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string trash_project_AFTER_INSERT =
           @"CREATE TRIGGER trash_project_AFTER_INSERT Before INSERT ON trash_project FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string trash_projectcategory_AFTER_INSERT =
           @"CREATE TRIGGER trash_projectcategory_AFTER_INSERT Before INSERT ON trash_projectcategory FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string trash_roleaction_AFTER_INSERT =
           @"CREATE TRIGGER trash_roleaction_AFTER_INSERT Before INSERT ON trash_roleaction FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string trash_rolepermission_AFTER_INSERT =
                       @"CREATE TRIGGER trash_rolepermission_AFTER_INSERT Before INSERT ON trash_rolepermission FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string trash_socialmedia_AFTER_INSERT =
                       @"CREATE TRIGGER trash_socialmedia_AFTER_INSERT Before INSERT ON trash_socialmedia FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string trash_tablereplace_AFTER_INSERT =
           @"CREATE TRIGGER trash_tablereplace_AFTER_INSERT Before INSERT ON trash_tablereplace FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string trash_tag_AFTER_INSERT =
                       @"CREATE TRIGGER trash_tag_AFTER_INSERT Before INSERT ON trash_tag FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string trash_taskcomment_AFTER_INSERT =
                       @"CREATE TRIGGER trash_taskcomment_AFTER_INSERT Before INSERT ON trash_taskcomment FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string trash_tasklink_AFTER_INSERT =
           @"CREATE TRIGGER trash_tasklink_AFTER_INSERT Before INSERT ON trash_tasklink FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string trash_user_AFTER_INSERT =
           @"CREATE TRIGGER trash_user_AFTER_INSERT Before INSERT ON trash_user FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string trash_userrole_AFTER_INSERT =
           @"CREATE TRIGGER trash_userrole_AFTER_INSERT Before INSERT ON trash_userrole FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string trash_userrolepermissiongroup_AFTER_INSERT =
           @"CREATE TRIGGER trash_userrolepermissiongroup_AFTER_INSERT Before INSERT ON trash_userrolepermissiongroup FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            #endregion

            #region Live Trigger YTT
            public const string Comment_AFTER_INSERT =
            @"CREATE TRIGGER Comment_AFTER_INSERT Before INSERT ON Comment FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string UserRolePermissionGroup_AFTER_INSERT =
            @"CREATE TRIGGER UserRolePermissionGroup_AFTER_INSERT Before INSERT ON UserRolePermissionGroup FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string UserRolePermission_AFTER_INSERT =
            @"CREATE TRIGGER UserRolePermission_AFTER_INSERT Before INSERT ON UserRolePermission FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string UserRoleAction_AFTER_INSERT =
            @"CREATE TRIGGER UserRoleAction_AFTER_INSERT Before INSERT ON UserRoleAction FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string UserRoleActionValue_AFTER_INSERT =
            @"CREATE TRIGGER UserRoleActionValue_AFTER_INSERT Before INSERT ON UserRoleActionValue FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string Note_AFTER_INSERT =
            @"CREATE TRIGGER Note_AFTER_INSERT Before INSERT ON Note FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string TeamMember_AFTER_INSERT =
            @"CREATE TRIGGER TeamMember_AFTER_INSERT Before INSERT ON TeamMember FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Team_AFTER_INSERT =
            @"CREATE TRIGGER Team_AFTER_INSERT Before INSERT ON Team FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string FileAccess_AFTER_INSERT =
            @"CREATE TRIGGER FileAccess_AFTER_INSERT Before INSERT ON FileAccess FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";


            public const string File_AFTER_INSERT =
            @"CREATE TRIGGER File_AFTER_INSERT Before INSERT ON File FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string User_AFTER_INSERT =
           @"CREATE TRIGGER User_AFTER_INSERT Before INSERT ON User FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string UserStatus_AFTER_INSERT =
           @"CREATE TRIGGER UserStatus_AFTER_INSERT Before INSERT ON UserStatus FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string UserRole_AFTER_INSERT =
           @"CREATE TRIGGER UserRole_AFTER_INSERT Before INSERT ON UserRole FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Arbitration_AFTER_INSERT =
           @"CREATE TRIGGER Arbitration_AFTER_INSERT Before INSERT ON Arbitration FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string InsuranceCompany_AFTER_INSERT =
           @"CREATE TRIGGER InsuranceCompany_AFTER_INSERT Before INSERT ON InsuranceCompany FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string FleetLeasingCompany_AFTER_INSERT =
           @"CREATE TRIGGER FleetLeasingCompany_AFTER_INSERT Before INSERT ON FleetLeasingCompany FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Reminder_AFTER_INSERT =
           @"CREATE TRIGGER Reminder_AFTER_INSERT Before INSERT ON Reminder FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Repo_AFTER_INSERT =
           @"CREATE TRIGGER Repo_AFTER_INSERT Before INSERT ON Repo FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Collection_AFTER_INSERT =
           @"CREATE TRIGGER Collection_AFTER_INSERT Before INSERT ON Collection FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            #endregion
            #region Trash Trigger YTT
            public const string Trash_Comment_AFTER_INSERT =
            @"CREATE TRIGGER Trash_Comment_AFTER_INSERT Before INSERT ON Trash_Comment FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_Note_AFTER_INSERT =
          @"CREATE TRIGGER Trash_Note_AFTER_INSERT Before INSERT ON Trash_Note FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_TeamMember_AFTER_INSERT =
            @"CREATE TRIGGER Trash_TeamMember_AFTER_INSERT Before INSERT ON Trash_TeamMember FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_Team_AFTER_INSERT =
            @"CREATE TRIGGER Trash_Team_AFTER_INSERT Before INSERT ON Trash_Team FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_FileAccess_AFTER_INSERT =
            @"CREATE TRIGGER Trash_FileAccess_AFTER_INSERT Before INSERT ON Trash_FileAccess FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string Trash_File_AFTER_INSERT =
            @"CREATE TRIGGER Trash_File_AFTER_INSERT Before INSERT ON Trash_File FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            public const string Trash_User_AFTER_INSERT =
           @"CREATE TRIGGER Trash_User_AFTER_INSERT Before INSERT ON Trash_User FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_UserStatus_AFTER_INSERT =
           @"CREATE TRIGGER Trash_UserStatus_AFTER_INSERT Before INSERT ON Trash_UserStatus FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_UserRole_AFTER_INSERT =
           @"CREATE TRIGGER Trash_UserRole_AFTER_INSERT Before INSERT ON Trash_UserRole FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_Arbitration_AFTER_INSERT =
           @"CREATE TRIGGER Trash_Arbitration_AFTER_INSERT Before INSERT ON Trash_Arbitration FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_InsuranceCompany_AFTER_INSERT =
           @"CREATE TRIGGER Trash_InsuranceCompany_AFTER_INSERT Before INSERT ON Trash_InsuranceCompany FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_FleetLeasingCompany_AFTER_INSERT =
           @"CREATE TRIGGER Trash_FleetLeasingCompany_AFTER_INSERT Before INSERT ON Trash_FleetLeasingCompany FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_Reminder_AFTER_INSERT =
           @"CREATE TRIGGER Trash_Reminder_AFTER_INSERT Before INSERT ON Trash_Reminder FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_Repo_AFTER_INSERT =
           @"CREATE TRIGGER Trash_Repo_AFTER_INSERT Before INSERT ON Trash_Repo FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_Collection_AFTER_INSERT =
           @"CREATE TRIGGER Trash_Collection_AFTER_INSERT Before INSERT ON Trash_Collection FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            #endregion

            #region Live Trigger Asoy
            public const string BankCheck_AFTER_INSERT =
            @"CREATE TRIGGER BankCheck_AFTER_INSERT Before INSERT ON BankCheck FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string AccountAdditionCheck_AFTER_INSERT =
            @"CREATE TRIGGER AccountAdditionCheck_AFTER_INSERT Before INSERT ON AccountAdditionCheck FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Record_AFTER_INSERT =
            @"CREATE TRIGGER Record_AFTER_INSERT Before INSERT ON Record FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Account_AFTER_INSERT =
            @"CREATE TRIGGER Account_AFTER_INSERT Before INSERT ON Account FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string AccountAdditionBank_AFTER_INSERT =
            @"CREATE TRIGGER AccountAdditionBank_AFTER_INSERT Before INSERT ON AccountAdditionBank FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string AccountType_AFTER_INSERT =
            @"CREATE TRIGGER AccountType_AFTER_INSERT Before INSERT ON AccountType FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Bank_AFTER_INSERT =
            @"CREATE TRIGGER Bank_AFTER_INSERT Before INSERT ON Bank FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string BloodType_AFTER_INSERT =
            @"CREATE TRIGGER BloodType_AFTER_INSERT Before INSERT ON BloodType FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string ConstructionSite_AFTER_INSERT =
            @"CREATE TRIGGER ConstructionSite_AFTER_INSERT Before INSERT ON ConstructionSite FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Contact_AFTER_INSERT =
            @"CREATE TRIGGER Contact_AFTER_INSERT Before INSERT ON Contact FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string ContactAdditionCompany_AFTER_INSERT =
            @"CREATE TRIGGER ContactAdditionCompany_AFTER_INSERT Before INSERT ON ContactAdditionCompany FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string ContactAdditionPerson_AFTER_INSERT =
            @"CREATE TRIGGER ContactAdditionPerson_AFTER_INSERT Before INSERT ON ContactAdditionPerson FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string ContactType_AFTER_INSERT =
            @"CREATE TRIGGER ContactType_AFTER_INSERT Before INSERT ON ContactType FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Currency_AFTER_INSERT =
            @"CREATE TRIGGER Currency_AFTER_INSERT Before INSERT ON Currency FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Job_AFTER_INSERT =
            @"CREATE TRIGGER Job_AFTER_INSERT Before INSERT ON Job FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string RecordCategory_AFTER_INSERT =
            @"CREATE TRIGGER RecordCategory_AFTER_INSERT Before INSERT ON RecordCategory FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string TransactionType_AFTER_INSERT =
            @"CREATE TRIGGER TransactionType_AFTER_INSERT Before INSERT ON TransactionType FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string WorkingStatus_AFTER_INSERT =
            @"CREATE TRIGGER WorkingStatus_AFTER_INSERT Before INSERT ON WorkingStatus FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";

            #endregion
            #region Trash Trigger Asoy
            public const string Trash_BankCheck_AFTER_INSERT =
            @"CREATE TRIGGER Trash_BankCheck_AFTER_INSERT Before INSERT ON Trash_BankCheck FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_AccountAdditionCheck_AFTER_INSERT =
            @"CREATE TRIGGER Trash_AccountAdditionCheck_AFTER_INSERT Before INSERT ON Trash_AccountAdditionCheck FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_Record_AFTER_INSERT =
            @"CREATE TRIGGER Trash_Record_AFTER_INSERT Before INSERT ON Trash_Record FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_Account_AFTER_INSERT =
            @"CREATE TRIGGER Trash_Account_AFTER_INSERT Before INSERT ON Trash_Account FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_AccountAdditionBank_AFTER_INSERT =
            @"CREATE TRIGGER Trash_AccountAdditionBank_AFTER_INSERT Before INSERT ON Trash_AccountAdditionBank FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_AccountType_AFTER_INSERT =
            @"CREATE TRIGGER Trash_AccountType_AFTER_INSERT Before INSERT ON Trash_AccountType FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_Bank_AFTER_INSERT =
            @"CREATE TRIGGER Trash_Bank_AFTER_INSERT Before INSERT ON Trash_Bank FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_BloodType_AFTER_INSERT =
            @"CREATE TRIGGER Trash_BloodType_AFTER_INSERT Before INSERT ON Trash_BloodType FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_ConstructionSite_AFTER_INSERT =
            @"CREATE TRIGGER Trash_ConstructionSite_AFTER_INSERT Before INSERT ON Trash_ConstructionSite FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_Contact_AFTER_INSERT =
            @"CREATE TRIGGER Trash_Contact_AFTER_INSERT Before INSERT ON Trash_Contact FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_ContactAdditionCompany_AFTER_INSERT =
            @"CREATE TRIGGER Trash_ContactAdditionCompany_AFTER_INSERT Before INSERT ON Trash_ContactAdditionCompany FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_ContactAdditionPerson_AFTER_INSERT =
            @"CREATE TRIGGER Trash_ContactAdditionPerson_AFTER_INSERT Before INSERT ON Trash_ContactAdditionPerson FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_ContactType_AFTER_INSERT =
            @"CREATE TRIGGER Trash_ContactType_AFTER_INSERT Before INSERT ON Trash_ContactType FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_Currency_AFTER_INSERT =
            @"CREATE TRIGGER Trash_Currency_AFTER_INSERT Before INSERT ON Trash_Currency FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_Job_AFTER_INSERT =
            @"CREATE TRIGGER Trash_Job_AFTER_INSERT Before INSERT ON Trash_Job FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_RecordCategory_AFTER_INSERT =
            @"CREATE TRIGGER Trash_RecordCategory_AFTER_INSERT Before INSERT ON Trash_RecordCategory FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_TransactionType_AFTER_INSERT =
            @"CREATE TRIGGER Trash_TransactionType_AFTER_INSERT Before INSERT ON Trash_TransactionType FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            public const string Trash_WorkingStatus_AFTER_INSERT =
            @"CREATE TRIGGER Trash_WorkingStatus_AFTER_INSERT Before INSERT ON Trash_WorkingStatus FOR EACH ROW
			set New.datecreated = current_date(), New.datemodified=current_date();";
            #endregion

        }


    }


}



//ALTER TABLE ntc_ytt_prod.arbitration CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.client CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.collection CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.file CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.fileaccess CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.fleetleasingcompany CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.insurancecompany CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.note CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.refreshtoken CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.reminder CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.repo CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.tablereplace CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.team CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.teammember CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.user CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.userrole CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.userstatus CHARACTER SET utf8 COLLATE utf8_general_ci;

//ALTER TABLE ntc_ytt_prod.trash_arbitration CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.trash_collection CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.trash_file CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.trash_fileaccess CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.trash_fleetleasingcompany CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.trash_insurancecompany CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.trash_note CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.trash_reminder CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.trash_repo CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.trash_team CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.trash_teammember CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.trash_user CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.trash_userrole CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.trash_userstatus CHARACTER SET utf8 COLLATE utf8_general_ci;



//ALTER TABLE ntc_ytt_prod.arbitration CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.client CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.collection CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.file CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.fileaccess CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.fleetleasingcompany CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.insurancecompany CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.note CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.refreshtoken CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.reminder CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.repo CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.tablereplace CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.team CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.teammember CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.user CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.userrole CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.userstatus CHARACTER SET utf8 COLLATE utf8_general_ci;

//ALTER TABLE ntc_ytt_prod.trash_arbitration CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.trash_collection CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.trash_file CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.trash_fileaccess CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.trash_fleetleasingcompany CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.trash_insurancecompany CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.trash_note CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.trash_reminder CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.trash_repo CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.trash_team CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.trash_teammember CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.trash_user CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.trash_userrole CHARACTER SET utf8 COLLATE utf8_general_ci;
//ALTER TABLE ntc_ytt_prod.trash_userstatus CHARACTER SET utf8 COLLATE utf8_general_ci;
