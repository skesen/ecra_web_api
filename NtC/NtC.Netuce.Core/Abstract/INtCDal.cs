﻿using System.Collections.Generic;
using NtC.Netuce.Core.DataAccess;

namespace NtC.Netuce.Core.Abstract
{
    public interface INtCDal : IEntityRepository
    {
        /// <summary>
        /// Posts the specified table.
        /// </summary>
        /// <param name="table">The table.</param>
        /// <param name="model">The model.</param>
        /// <param name="data">The data.</param>
        /// <returns>System.Object.</returns>
        object Post(string baseUrl, string table,KeyValuePair<string,string>[] model, string data);
        /// <summary>
        /// Counts the specified table.
        /// </summary>
        /// <param name="table">The table.</param>
        /// <param name="keyValuePairs">The key value pairs.</param>
        /// <returns>System.Int32.</returns>
        int Count(string table, KeyValuePair<string, string>[] keyValuePairs);
        /// <summary>
        /// Gets the specified table.
        /// </summary>
        /// <param name="table">The table.</param>
        /// <param name="keyValuePairs">The key value pairs.</param>
        /// <returns>IEnumerable&lt;System.Object&gt;.</returns>
        IEnumerable<object> Get(string baseUrl,string table,KeyValuePair<string, string>[] keyValuePairs);
        /// <summary>
        /// purpose of function create new view 
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="viewname"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        object CreateView(string baseUrl, string viewname, KeyValuePair<string, string>[] model);
 
    }
}
