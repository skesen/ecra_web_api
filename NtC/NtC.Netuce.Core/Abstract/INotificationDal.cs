﻿using System.Collections.Generic;
using NtC.Netuce.Core.DataAccess;

namespace NtC.Netuce.Core.Abstract
{
    public interface INotificationDal : IEntityRepository
    {
        bool SendEmail(string baseUrl,KeyValuePair<string, string>[] model);

    }
}
