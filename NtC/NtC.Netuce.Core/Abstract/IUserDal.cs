﻿using NtC.Netuce.Core.DataAccess;
using NtC.Netuce.Entities.YTT;


namespace NtC.Netuce.Core.Abstract
{
    public interface IUserDal : IEntityRepository
    {
        User GetUserByEmail(string email);
    }
}
