﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtC.Netuce.Core.Utilities.ExtensionMethods
{
    public static partial class Extensions
    {
        public static string MakeString(this long input)
        {

            string str = input.ToString("N");

            return str.Substring(0, str.Length - 3);

        }

        public static bool IsNullOrLessThanOne(this long? input)
        {
            return (input == null || input < 1);
        }

    }
}
