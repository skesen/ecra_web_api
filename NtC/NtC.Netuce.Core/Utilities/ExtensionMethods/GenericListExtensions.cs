﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace NtC.Netuce.Core.Utilities.ExtensionMethods
{
    public static partial class Extensions
    {
        public static List<T> Truncate<T>(this List<T> input, int amount)
        {
            var returnValue = new List<T>();
            foreach (T item in input)
            {
                returnValue.Add(item);
                if (returnValue.Count == amount) return returnValue;
            }

            return returnValue;

        }

        public static IEnumerable<KeyValuePair<string, string>> ToPairs(this NameValueCollection collection)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }

            return collection.Cast<string>().Select(key => new KeyValuePair<string, string>(key, collection[key]));
        }

        public static List<T> SubList<T>(this List<T> input, int index, int length)
        {
            var returnValue = new List<T>();
            var indexCounter = 0;
            var lengthCounter = 1;
            foreach (T item in input)
            {
                if (indexCounter >= index)
                {
                    returnValue.Add(item);
                    lengthCounter++;
                    if (lengthCounter > length) return returnValue;
                }

                indexCounter++;

            }

            return returnValue;

        }

        public static List<T> SubList<T>(this List<T> input, int index)
        {
            var returnValue = new List<T>();
            var indexCounter = 0;
            var lengthCounter = 0;
            foreach (T item in input)
            {
                if (indexCounter >= index)
                {
                    returnValue.Add(item);
                }

                indexCounter++;
                lengthCounter++;
            }

            return returnValue;

        }

        public static List<T> GetCommaDelimited<T>(this List<T> input, int index)
        {
            var returnValue = new List<T>();
            var indexCounter = 0;
            var lengthCounter = 0;
            foreach (T item in input)
            {
                if (indexCounter >= index)
                {
                    returnValue.Add(item);
                }

                indexCounter++;
                lengthCounter++;
            }

            return returnValue;

        }

        public static List<T> ToList<T>(this IEnumerable input)
        {
            try
            {
                return (from object thing in input select thing.ToString() into str select (T)Convert.ChangeType(str, typeof(T))).ToList();
            }
            catch (FormatException ex)
            {
                throw new InvalidCastException(ex.Message);
            }
        }



    }
}
