﻿using System;
using System.Linq;
using System.Reflection;

namespace NtC.Netuce.Core.Utilities.ExtensionMethods
{
    public static class TypeExtensions
    {
        /// <summary>
        /// Generic tipler dahil tüm tiplerin daha anlaşılır ismini döner.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GetFriendlyName(this Type type)
        {
            var prefix = "";
            if (type.IsNested && !type.IsGenericParameter && type.DeclaringType != null)
                prefix = $"{type.DeclaringType.GetFriendlyName()}.";
            if (type == typeof(int))
                return $"{prefix}int";
            else if (type == typeof(short))
                return $"{prefix}short";
            else if (type == typeof(byte))
                return $"{prefix}byte";
            else if (type == typeof(bool))
                return $"{prefix}bool";
            else if (type == typeof(long))
                return $"{prefix}long";
            else if (type == typeof(float))
                return $"{prefix}float";
            else if (type == typeof(double))
                return $"{prefix}double";
            else if (type == typeof(decimal))
                return $"{prefix}decimal";
            else if (type == typeof(string))
                return $"{prefix}string";
            else if (type.GetTypeInfo().IsGenericType)
                return prefix + type.Name.Split('`')[0] + "<" +
                       String.Join(", ", type.GetGenericArguments().Select(GetFriendlyName).ToArray()) + ">";
            else
                return prefix + type.Name;
        }
    }
}
