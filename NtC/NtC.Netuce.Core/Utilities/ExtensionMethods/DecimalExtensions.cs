﻿using System;
using NtC.Netuce.Core.Properties;

namespace NtC.Netuce.Core.Utilities.ExtensionMethods
{
    public static partial class Extensions
    {
        public static string MakeMoneyString(this decimal input, bool switchCommas, bool tl)
        {

            if (input == decimal.MinValue) input = 0;

            decimal value = decimal.Round(input, 2);

            string str = value.ToString("###,###,###,###.##");

            if (value < 1 && value >= 0) str = "0" + str;
            if (switchCommas) str = str.SwitchCommas();

            if (str.IndexOf(",", System.StringComparison.Ordinal) > -1
                    && str != "0"
                    && str.Substring(str.IndexOf(",", System.StringComparison.Ordinal)).Length == 2) str += "0";

            if (tl)
                return str + " TL";

            return str + " USD";
        }
        public static string MakeMoneyString(this decimal input, bool switchCommas, bool TL, int decimals)
        {

            if (input == decimal.MinValue) input = 0;

            var value = decimal.Round(input, decimals);
            var format = "###,###,###,###.";
            for (var i = 0; i < decimals; i++)
            {
                format += "#";
            }
            var str = value.ToString(format);


            if (value < 1 && value >= 0) str = "0" + str;
            if (switchCommas) str = str.SwitchCommas();

            if (str.IndexOf(",", StringComparison.Ordinal) > -1
                    && str != "0"
                    && str.Substring(str.IndexOf(",", StringComparison.Ordinal)).Length == 2) str += "0";

            if (TL)
                return str + " TL";

            return str + " USD";
        }



        public static string MakeString(this decimal input, bool switchCommas)
        {

            var value = decimal.Round(input, 2);

            var str = value.ToString("N");

            if (switchCommas)
                str = str.SwitchCommas();

            return str;

        }


        public static decimal SubtractKDV(this decimal input)
        {
            input = input / (1 + Settings.Default.KDV);

            return input;

        }

        public static decimal AddKDV(this decimal input)
        {

            input = input + (input * Settings.Default.KDV);
            return input;

        }





    }
}
