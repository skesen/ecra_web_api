﻿using System.Collections.Generic;
using System.Linq;

namespace NtC.Netuce.Core.Utilities.ExtensionMethods
{
    public static partial class Extensions
    {


        public static List<int> ToIntList(this List<string> input)
        {
            return input.Select(int.Parse).ToList();
        }
    }
}
