﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;

namespace NtC.Netuce.Core.Utilities.ExtensionMethods
{
    public static partial class Extensions
    {

        public static DateTime GetDateWithoutTime(this DateTime input)
        {
            return new DateTime(input.Year, input.Month, input.Day);

        }

        public static bool IsNotNullOrMinValue(this DateTime? input)
        {
            if (input == null) return false;
            return (bool)(input.Value > SqlDateTime.MinValue);
        }
        /// <summary>
        /// Return Value = dd/MM/YYYY
        /// </summary>
        /// <param name="input"></param>
        /// <returns>dd/MM/YYYY</returns>
        public static string MakeDateString(this DateTime input)
        {
            var s = new StringBuilder();
            if (input.Day < 10)
                s.Append("0");
            s.Append(input.Day);
            s.Append("/");
            if (input.Month < 10)
                s.Append("0");
            s.Append(input.Month);
            s.Append("/");
            s.Append(input.Year);
            return s.ToString();

        }

        public static bool IsMax(this DateTime input)
        {
            return input.Year >= 9999;
        }

        public static DateTime CheckSqlDateMinValue(this DateTime input)
        {
            return input == DateTime.MinValue ? SqlDateTime.MinValue.Value : input;

        }


    
   
        public class UxrDateModel
        {
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
            public string Date { get; set; }
        }

        public static UxrDateModel GetUxrDate(DateTime start, DateTime end)
        {
            UxrDateModel model = new UxrDateModel();
            model.StartDate = start;
            model.EndDate = end;
            model.Date = start.ToString("yyyy-MM-dd") + " to " + end.ToString("yyyy-MM-dd");
            return model;
        }

        public static UxrDateModel GetUxrDate(string date)
        {
            UxrDateModel model = new UxrDateModel();
            model.StartDate = Convert.ToDateTime(date.Split(' ')[0]);
            model.EndDate = Convert.ToDateTime(date.Split(' ')[2]);
            model.Date = date;
            return model;
        }

     
        /// <summary>
        /// Belirli iki tarih arasındaki istenen günleri döner. Mesela 1 Ocak 2016 ile 1 Mart 2016 tarihleri arasındaki Pazartesi,Salı ve Çarşamba günlerinin tarihlerini liste halinde döner.
        /// </summary>
        /// <param name="startDate">Başlangıç tarihi</param>
        /// <param name="endDate">Bitiş tarihi</param>
        /// <param name="days">Tarihlerini elde etmek istediğimiz günler.</param>
        /// <returns></returns>
        public static List<DateTime> GetDateListBetweenTwoDates(DateTime startDate, DateTime endDate, List<DayOfWeek> days)
        {
            if (days == null || days.Count() == 0)
                return new List<DateTime>();

            List<DateTime> dates = new List<DateTime>();

            for (DateTime date = startDate; date.Date <= endDate; date = date.AddDays(1))
            {
                if (days.Contains(date.DayOfWeek))
                    dates.Add(date);
            }

            return dates.OrderBy(s => s.Date).ToList();
        }

        public static bool HasValue(this DateTime dateTime)
        {
            return (dateTime != default(DateTime));
        }

        public static DateTime FromComparableDate(this string dateTime)
        {
            return DateTime.ParseExact(dateTime, "yyyyMMdd", null);
        }
        public static string ToComparableDate(this DateTime dateTime)
        {
            return dateTime.ToString("yyyyMMdd");
        }
        public static string ToComparableDate(this DateTime? dateTime)
        {
            return dateTime.HasValue ? dateTime.Value.ToComparableDate() : "";
        }
    }
}
