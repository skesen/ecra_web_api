﻿using System;
using System.Web;

namespace NtC.Netuce.Core.Utilities.ExtensionMethods
{
    [Serializable]
    public class SessionContext : SessionBase
    {
        public static SessionContext CurrentSession
        {
            get
            {
                var session = (SessionContext)HttpContext.Current.Session["SessionId"];
                if (session == null)
                {
                    session = new SessionContext();
                    HttpContext.Current.Session["SessionId"] = session;
                }
                return session;
            }
        }

    }
    public class SessionBase
    {


    }
}
