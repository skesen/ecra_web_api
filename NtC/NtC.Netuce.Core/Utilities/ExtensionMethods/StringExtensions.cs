﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace NtC.Netuce.Core.Utilities.ExtensionMethods
{
    public static partial class Extensions
    {
        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHiJKLMNOPQRSTUVWXYZ";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        /// <summary>
        /// Tablo için guid id oluşturmaktadır.
        /// </summary>
        /// <returns>id</returns>
        public static string Createkey()
        {
            return Extensions.RandomString(1) + Extensions.RandomInt(2) + Extensions.RandomString(1) + Extensions.RandomInt(2) + Extensions.RandomString(1) + Extensions.RandomInt(2) + Extensions.RandomString(1);
        }
        public static string CleanDATA(this string uglyJson)
        {
            return uglyJson.Replace("\r\n ", "").Replace("\r\n", "").Replace("\n", "").Replace("\r", "").Replace("\t", "").Replace(Environment.NewLine, "").Replace(@"\s+", string.Empty);
        }

        /// <summary>
        /// Bu işlem sort için gelen parametreleri düzenlemek için geliştirilmiştir. /&sort={"name":"asc","datecreated":"date=>desc"}
        /// </summary>
        /// <param name="value">split edilmiş datanın value kısmı</param>
        /// <param name="seperator">ne ile ayrıldığı</param>
        /// <param name="key">split edilmiş datanın key kısmı</param>
        /// <returns></returns>
        public static string ParseColumnNameForSort(this string value, string seperator, string key,string tablename)
        {
            var result = "";
            var t = Regex.Split(value, ",");
            foreach (var s in t)
            {
                if (s.Contains(seperator))
                {
                    var q = Regex.Split(s, seperator);
                    //if (q[0] == "date" || q[0] == "num")                    
                    if (q[0] == "xxx")
                    {
                        result = tablename + "." + key + " " + q[1];
                    }
                }
                else
                    result += "(CASE WHEN " +tablename+"."+ key + " IS NULL THEN 1 ELSE 0 END), " + tablename + "." + key + " REGEXP '^[^A-Za-z0-9ğüşöçİĞÜŞÖÇ]+$' " + s + ", " + tablename + "." + key + " " + s;

            }

            return result;
        }
        /// <summary>
        /// Bu işlem view oluştururken field tablolarında as deyimini sql sorgusu için düzenleme için geliştirilmiştir.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="seperator"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string ParseColumnNameForView(this string value, string seperator, string key)
        {
            var result = "";
            var a = "";
            var k = value.Split(',');
            foreach (var s in k)
            {
                if (s.Contains("="))
                    a += ";" + s;
                else
                    a += "," + s;
            }
            a = a.Remove(0, 1);
            var t = Regex.Split(a, ",");
            foreach (var s in t)
            {
                if (s.Contains(seperator))
                {
                    var q = Regex.Split(s, seperator);
                    result += key + "." + q[0] + " as " + q[1] + ", ";
                }
                else
                    result += key + "." + s + ", ";

            }

            return result;
        }

        public static string ObjectClearText(this string str)
        {
            if (string.IsNullOrEmpty(str)) return str;
            str = str.Replace("{", "");
            str = str.Replace("}", "");
            str = str.Replace(":", "=");
            str = str.Replace(@"""", "");
            //dışarı taşınabilir.
            str = str.Replace("%7E=", " like");
            str = str.Replace("=='null'", " is null ");
            str = str.Replace("\"||\"", " or ").Replace("~=", " like ").Replace("\"&&\"", " && ").Replace("==", "=");
            return str;
        }

        public static string ObjectClearTextForJoinQuery(this string str, string tablename)
        {
            if (string.IsNullOrEmpty(str)) return str;
            str = str.Replace("{", "");
            str = str.Replace("}", "");
            str = str.Replace(":", "=");
            str = str.Replace(@"""", "");
            //dışarı taşınabilir.
            str = str.Replace("%7E=", " like");
            str = str.Replace("=='null'", " is null ");
            str = str.Replace("\"||\"", " or ").Replace("~=", " like ").Replace("\"&&\"", " && ").Replace("==", "=");
            return tablename + "." + str;
        }

        public static string ObjectClearTextForSpace(this string str)
        {
            if (string.IsNullOrEmpty(str)) return str;
            str = str.Replace(@"""", "");
            str = str.Replace(":", " ");
            str = str.Replace("}", "");
            str = str.Replace("{", "");
            return str;
        }

        public static Dictionary<string, string> StringClearText(this string str)
        {
            var result = "";
            var a = "";
            var k = str.Split(',');
            foreach (var s in k)
            {
                if (s.Contains("="))
                {
                    a += ";" + s;
                }
                else
                {
                    a += "," + s;
                }
            }
            a = a.Remove(0, 1);
            var retval = new Dictionary<string, string>();
            foreach (var s in a.Split(';'))
                retval.Add(s.Split('=')[0], Uri.UnescapeDataString(s.Split('=')[1]));
            return retval;
        }
        public static string StringClearTextForJoin(this string str, string joinname)
        {
            var result = "";
            var a = "";
            var k = str.Split(',');
            foreach (var s in k)
            {
                if (s.Contains("="))
                {
                    a += ";" + s;
                }
                else
                {
                    a += "," + s;
                }
            }
            a = a.Remove(0, 1);
            var jointable = "";
            int idx = 0;
            var key0 = "";
            var value0 = "";
            foreach (var s in a.Split(';'))
            {

                var key = a.Split(';')[idx].Split('=')[0];
                var value = Uri.UnescapeDataString(a.Split(';')[idx].Split('=')[1]);
                if (idx != 0)
                {
                    key0 = a.Split(';')[idx == 1 ? 0 : idx - 1].Split('=')[0];
                    value0 = Uri.UnescapeDataString(a.Split(';')[idx == 1 ? 0 : idx - 1].Split('=')[1]);
                }
                if (string.IsNullOrEmpty(jointable))
                {
                    jointable = key;
                }
                else if (idx % 2 != 0)
                {
                    jointable += joinname + " JOIN " + key + " ON " + key + "." + value + "=" + key0 + "." + value0;
                }
                idx++;

            }
            return jointable;
        }
        public static string RandomInt(int length)
        {
            const string chars = "0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string EncodeForSqlString(string value)
        {
            return value.Replace(@"""", "").Replace(@"\", @"\\")/*.Replace("'", @"\'")*/;
        }
        public static bool IsNumber(this string str)
        {
            double junk = 0;
            return double.TryParse(str, out junk);
        }
        /// <summary>
        /// Gelen texti istenilen değer ile karşılaştırma(küçülterek) 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool ContainsText(this string text, string str)
        {
            return text.ToLower().Contains(str);
        }

        public static bool EqualText(this string text, string str)
        {
            return text.ToLower().Equals(str.ToLower());
        }
        public static Dictionary<string, string> StringToDictionary(this string text)
        {
            return text.Split(',').ToDictionary(c => c.Split('=')[0], c => Uri.UnescapeDataString(c.Split('=')[1]));
        }
        public static byte[] ToByteArray(this string str)
        {
            var encoding = new UTF8Encoding();
            return encoding.GetBytes(str);
        }

        public static string RemoveInvalidCreativeDescriptionChars(this string input)
        {

            var chars = new List<string> { "^", "~", "<", ">", "/", ";", "|" };

            return chars.Aggregate(input, (current, chr) => current.Replace(chr, string.Empty));

        }

        public static string ProperCase(this string input)
        {
            input = input.ToLower();
            var sProper = "";

            var separators = new[] { ' ' };
            foreach (var chr in input.Split(separators))
            {
                sProper += char.ToUpper(chr[0]);
                sProper +=
                (chr.Substring(1, chr.Length - 1) + ' ');
            }
            return sProper;
        }

        public static string StripIntMarkers(this string input)
        {
            if (Equals(Thread.CurrentThread.CurrentCulture, CultureInfo.GetCultureInfo("en-US")))
                return input.Replace(",", "");
            return input.Replace(".", "");

        }

        public static string CleanURL(this string input)
        {
            if (input.StartsWith("http://http://") || input.StartsWith("https://http://") || input.StartsWith("https://https://"))
            {
                if (input.StartsWith("http://http://"))
                    input = input.Replace("http://http://", "http://");
                else if (input.StartsWith("https://http://"))
                    input = input.Replace("https://http://", "http://");
                else if (input.StartsWith("https://https://"))
                    input = input.Replace("https://https://", "https://");
            }

            if (input.StartsWith("http://") || input.StartsWith("https://"))
            {
                return input;
            }
            return "http://" + input;
        }
        public static String cast(Object o)
        {
            return (String)o;
        }



        public static string Truncate(this string input, int amount)
        {

            if (input == null) return string.Empty;

            string result;

            if (input.Length > amount && amount > 0)
            {
                result = input.Substring(0, amount);
            }
            else
            {
                result = input;
            }
            return result;
        }
        public static string Truncate(this string input, int amount, bool checkHtmlCode)
        {
            if (!checkHtmlCode)
                return input.Truncate(amount);
            if (input.Contains("&#"))
            {
                if (input.IndexOf("&#", amount) < amount + 10 && input.IndexOf("&#", amount) != -1)
                    return input.Substring(0, input.IndexOf("&#", amount));
                if (input.IndexOf("&#") > amount - 5 && input.IndexOf("&#") < amount + 5)
                    return input.Substring(0, input.IndexOf("&#"));
                return input.Truncate(amount);
            }
            return input.Truncate(amount);
        }

        public static string TrimFromRight(this string input, int amount)
        {
            string result;

            result = input.Length > amount ? input.Substring(0, input.Length - amount) : input;
            return result;
        }

        public static string GrabFromRight(this string input, int amount)
        {

            return input.Length > amount ? input.Substring(input.Length - amount) : input;

        }


        public static string ReplaceTurkishChars(this string input)
        {

            if (!string.IsNullOrEmpty(input))
            {
                input = input.Replace("ç", "c");
                input = input.Replace("Ç", "C");
                input = input.Replace("ş", "s");
                input = input.Replace("Ş", "S");
                input = input.Replace("İ", "I");
                input = input.Replace("ı", "i");
                input = input.Replace("ğ", "g");
                input = input.Replace("Ğ", "G");
                input = input.Replace("Ö", "O");
                input = input.Replace("ö", "o");
                input = input.Replace("Ü", "U");
                input = input.Replace("ü", "u");
            }
            return input;
        }

        public static string SwitchCommas(this string input)
        {
            var str = input;
            str = str.Replace(",", "**COMMA**");
            str = str.Replace(".", "**PERIOD**");
            str = str.Replace("**PERIOD**", ",");
            str = str.Replace("**COMMA**", ".");
            return str;
        }




    }
}
