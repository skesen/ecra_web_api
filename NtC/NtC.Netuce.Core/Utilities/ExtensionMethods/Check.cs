﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtC.Netuce.Core.Utilities.ExtensionMethods
{
    public static class Check
    {
        /// <summary>
        /// Determines whether the specified value is null.
        /// </summary>
        /// <typeparam name="T">Type of object to check</typeparam>
        /// <param name="value">The value.</param>
        /// <returns>
        /// 	<c>true</c> if the specified value is null; otherwise, <c>false</c>.
        /// </returns>
        static public bool IsNull<T>(T value)
        {
            return !typeof(T).IsValueType && value == null;
        }

        /// <summary>
        /// Determines whether [is null or empty] [the specified value].
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        /// 	<c>true</c> if [is null or empty] [the specified value]; otherwise, <c>false</c>.
        /// </returns>
        static public bool IsNullOrEmpty(string value)
        {
            return string.IsNullOrEmpty(value);
        }

        /// <summary>
        /// Determines whether [is null or empty] [the specified list].
        /// </summary>
        /// <typeparam name="T">Type of object to check</typeparam>
        /// <param name="list">The list.</param>
        /// <returns>
        /// 	<c>true</c> if [is null or empty] [the specified list]; otherwise, <c>false</c>.
        /// </returns>
        static public bool IsNullOrEmpty<T>(ICollection<T> list)
        {
            return list == null || list.Count == 0;
        }

        /// <summary>
        /// Determines whether [is not equal] [the specified value].
        /// </summary>
        /// <typeparam name="T">Type of object to check</typeparam>
        /// <param name="value">The value.</param>
        /// <param name="compareTo">The compare to.</param>
        /// <returns>
        /// 	<c>true</c> if [is not equal] [the specified value]; otherwise, <c>false</c>.
        /// </returns>
        static public bool IsNotEqual<T>(T value, T compareTo) where T : IComparable<T>
        {
            if (IsNull(value))
                return true;

            if (IsNull(compareTo))
                return true;

            return value.CompareTo(compareTo) != 0;
        }

        /// <summary>
        /// Determines whether [is less than] [the specified value].
        /// </summary>
        /// <typeparam name="T">Type of object to check</typeparam>
        /// <param name="value">The value.</param>
        /// <param name="max">The max.</param>
        /// <returns>
        /// 	<c>true</c> if [is less than] [the specified value]; otherwise, <c>false</c>.
        /// </returns>
        static public bool IsLessThan<T>(T value, T max) where T : IComparable<T>
        {
            if (IsNull(value))
                return false;

            if (IsNull(max))
                return false;

            return value.CompareTo(max) < 0;
        }

        /// <summary>
        /// Determines whether [is less than or equal] [the specified value].
        /// </summary>
        /// <typeparam name="T">Type of object to check</typeparam>
        /// <param name="value">The value.</param>
        /// <param name="max">The max.</param>
        /// <returns>
        /// 	<c>true</c> if [is less than or equal] [the specified value]; otherwise, <c>false</c>.
        /// </returns>
        static public bool IsLessThanOrEqual<T>(T value, T max) where T : IComparable<T>
        {
            if (IsNull(value))
                return false;

            if (IsNull(max))
                return false;

            return value.CompareTo(max) <= 0;
        }

        /// <summary>
        /// Determines whether [is greater than] [the specified value].
        /// </summary>
        /// <typeparam name="T">Type of object to check</typeparam>
        /// <param name="value">The value.</param>
        /// <param name="min">The min.</param>
        /// <returns>
        /// 	<c>true</c> if [is greater than] [the specified value]; otherwise, <c>false</c>.
        /// </returns>
        static public bool IsGreaterThan<T>(T value, T min) where T : IComparable<T>
        {
            if (IsNull(value))
                return false;

            if (IsNull(min))
                return false;

            return value.CompareTo(min) > 0;
        }

        /// <summary>
        /// Determines whether [is greater than or equal] [the specified value].
        /// </summary>
        /// <typeparam name="T">Type of object to check</typeparam>
        /// <param name="value">The value.</param>
        /// <param name="min">The min.</param>
        /// <returns>
        /// 	<c>true</c> if [is greater than or equal] [the specified value]; otherwise, <c>false</c>.
        /// </returns>
        static public bool IsGreaterThanOrEqual<T>(T value, T min) where T : IComparable<T>
        {
            if (IsNull(value))
                return false;

            if (IsNull(min))
                return false;

            return value.CompareTo(min) >= 0;
        }

        /// <summary>
        /// Determines whether [is out of range inclusive] [the specified value].
        /// </summary>
        /// <typeparam name="T">Type of object to check</typeparam>
        /// <param name="value">The value.</param>
        /// <param name="min">The min.</param>
        /// <param name="max">The max.</param>
        /// <returns>
        /// 	<c>true</c> if [is out of range inclusive] [the specified value]; otherwise, <c>false</c>.
        /// </returns>
        static public bool IsOutOfRangeInclusive<T>(T value, T min, T max) where T : IComparable<T>
        {
            if (IsNull(value))
                return false;

            if (IsNull(min))
                return false;

            if (IsNull(max))
                return false;

            return value.CompareTo(min) < 0 || value.CompareTo(max) > 0;
        }

        /// <summary>
        /// Determines whether [is out of range exclusive] [the specified value].
        /// </summary>
        /// <typeparam name="T">Type of object to check</typeparam>
        /// <param name="value">The value.</param>
        /// <param name="min">The min.</param>
        /// <param name="max">The max.</param>
        /// <returns>
        /// 	<c>true</c> if [is out of range exclusive] [the specified value]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsOutOfRangeExclusive<T>(T value, T min, T max) where T : IComparable<T>
        {
            if (IsNull(value))
                return false;

            if (IsNull(min))
                return false;

            return !IsNull(max) && (value.CompareTo(min) <= 0 || value.CompareTo(max) >= 0);
        }

        /// <summary>
        /// Determines whether [is file valid] [the specified file].
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns>
        /// 	<c>true</c> if [is file valid] [the specified file]; otherwise, <c>false</c>.
        /// </returns>

    }
}
