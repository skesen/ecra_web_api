﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtC.Netuce.Core.Utilities.ExtensionMethods
{
    public static partial class Extensions
    {

        public static string GetSiteStatus(this short input)
        {
            switch (input)
            {
                case -1:
                    return "Rejected";
                case 0:
                    return "Unapproved";
                case 1:
                    return "Approved";
                default:
                    return string.Empty;
            }
        }

    }
}
