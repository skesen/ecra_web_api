﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtC.Netuce.Core.Utilities.ExtensionMethods
{
    public static partial class Extensions
    {

        public static bool IsNullOrLessThanOne(this int? input)
        {
            return (input == null || input < 1);
        }


        public static string MakeString(this int input)
        {

            string str = input.ToString("N");

            return str.Substring(0, str.Length - 3);

        }

        public static string AddLeadingZeros(this int input, int numberOfZeroes)
        {
            var sb = new StringBuilder();
            for (var i = 0; i < numberOfZeroes; i++)
            {
                sb.Append("0");
            }

            sb.Append(input.ToString(CultureInfo.InvariantCulture));
            return sb.ToString();
        }

        public static string AddLeadingZerosByStringLength(this int input, int stringLength)
        {
            var number = input.ToString(CultureInfo.InvariantCulture);
            return input.AddLeadingZeros(stringLength - number.Length);
        }



    }
}
