﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace NtC.Netuce.Core.Utilities.ExtensionMethods
{
    public class FtpService
    {
        private const string FTP_SCHEME = "ftp://";

        public bool CreateDirectory(string ftpAddress, NetworkCredential credentials)
        {
            return TryCreateDirectory(ftpAddress, credentials, false);
        }
        /// <summary>
        /// must going to override function
        /// </summary>
        /// <param name="ftpAddress"></param>
        /// <param name="credentails"></param>
        /// <returns></returns>
        public bool DirectoryExists(string ftpAddress, NetworkCredential credentails)
        {
            Uri uri = new Uri(ftpAddress.StartsWith(FTP_SCHEME) ? ftpAddress : FTP_SCHEME + ftpAddress);
            if (uri.Scheme != Uri.UriSchemeFtp)
                return false;

            //FTPConnection ftp = null;
            //try
            //{
            //    ftp = new FTPConnection { ServerAddress = uri.Host, UserName = credentails.UserName, Password = credentails.Password };
            //    if (!ftp.IsConnected)
            //        ftp.Connect();
            //    return ftp.DirectoryExists(uri.AbsolutePath);
            //}
            //catch (Exception)
            //{
            //    return false;
            //}
            //finally
            //{
            //    if (ftp != null && ftp.IsConnected)
            //        ftp.Close();
            //}
            return true;
        }
        /// <summary>
        ///  must going to override function
        /// </summary>
        /// <param name="ftpAddress"></param>
        /// <param name="credentials"></param>
        /// <param name="recursive"></param>
        /// <returns></returns>
        public bool TryCreateDirectory(string ftpAddress, NetworkCredential credentials, bool recursive)
        {
            Uri uri = new Uri(ftpAddress.StartsWith(FTP_SCHEME) ? ftpAddress : string.Format("{0}{1}", FTP_SCHEME, ftpAddress));
            if (uri.Scheme != Uri.UriSchemeFtp)
                return false;

            if (DirectoryExists(ftpAddress, credentials))
                return true;

            //if (!recursive)
            //{
            //    FTPConnection ftp = null;
            //    try
            //    {
            //        ftp = new FTPConnection
            //        {
            //            ServerAddress = uri.Host,
            //            UserName = credentials.UserName,
            //            Password = credentials.Password
            //        };
            //        if (!ftp.IsConnected)
            //            ftp.Connect();
            //        ftp.CreateDirectory(uri.AbsolutePath);
            //    }
            //    catch
            //    {
            //        return false;
            //    }
            //    finally
            //    {
            //        if (ftp != null && ftp.IsConnected)
            //            ftp.Close();
            //    }
            //    return true;
            //}

            StringBuilder url = new StringBuilder(uri.GetLeftPart(UriPartial.Authority));
            string[] folderNames = uri.AbsolutePath.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            if (Check.IsNullOrEmpty(folderNames)) return false;
            //foreach (string folderName in folderNames)
            //{
            //    Uri folderUri = new Uri(url.Append("/").Append(folderName).ToString());
            //    if (DirectoryExists(folderUri.ToString(), credentials))
            //        continue;

            //    FTPConnection ftp = null;
            //    try
            //    {
            //        ftp = new FTPConnection { ServerAddress = folderUri.Host, UserName = credentials.UserName, Password = credentials.Password };
            //        if (!ftp.IsConnected)
            //            ftp.Connect();
            //        ftp.CreateDirectory(folderUri.AbsolutePath);
            //    }
            //    catch (Exception)
            //    {
            //        return false;
            //    }
            //    finally
            //    {
            //        if (ftp != null && ftp.IsConnected)
            //            ftp.Close();
            //    }
            //}
            return true;
        }

        public bool Upload(string ftpAddress, NetworkCredential credentials, byte[] fileContents)
        {
            Uri uri = new Uri(ftpAddress.StartsWith(FTP_SCHEME) ? ftpAddress : FTP_SCHEME + ftpAddress);
            if (uri.Scheme != Uri.UriSchemeFtp)
                return false;

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(uri);
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = credentials;
            request.ContentLength = fileContents.Length;
            request.Timeout = 120000;

            Stream requestStream = request.GetRequestStream();
            requestStream.Write(fileContents, 0, fileContents.Length);
            requestStream.Close();

            return ProcessFtpRequest(request);
        }

        public byte[] GetDownloadedFileBytes(string fileHttpFullPath)
        {
            try
            {
                using (WebClient wc = new WebClient())
                {
                    var byteArr = wc.DownloadData(fileHttpFullPath);
                    return byteArr;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public MemoryStream DownloadFile(string fileFullPath, string fileName, NetworkCredential credentials, System.Web.HttpResponseBase responseBase)
        //{
        //    Uri uri = new Uri(fileFullPath.StartsWith(FTP_SCHEME) ? fileFullPath : FTP_SCHEME + fileFullPath);
        //    if (uri.Scheme != Uri.UriSchemeFtp)
        //        return null;
        //    FtpWebRequest request = null;
        //    try
        //    {
        //        request = (FtpWebRequest)WebRequest.Create(uri);
        //        request.Method = WebRequestMethods.Ftp.DownloadFile;
        //        request.Credentials = credentials;
        //        request.Timeout = 120000;

        //        using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())//get file from ftp
        //        {
        //            MemoryStream memStream;
        //            using (Stream responseStream = response.GetResponseStream())
        //            {
        //                memStream = new MemoryStream();
        //                byte[] buffer = new byte[2048];
        //                int byteCount;
        //                do
        //                {
        //                    byteCount = responseStream.Read(buffer, 0, buffer.Length);
        //                    memStream.Write(buffer, 0, byteCount);
        //                } while (byteCount > 0);

        //                return memStream;


        //                //using (StreamReader reader = new StreamReader(responseStream))
        //                //{
        //                //    string file = reader.ReadToEnd();

        //                //    byte[] buff = new byte[2048];
        //                //    //int contentLen = responseStream.Read(buff, 0, buff.Length);
        //                //    //while (contentLen != 0)
        //                //    //{
        //                //    //    fs.Write(buff, 0, buff.Length);
        //                //    //    contentLen = ftpStream.Read(buff, 0, buff.Length);
        //                //    //}

        //                //    //responseBase.Buffer = true;
        //                //    //responseBase.Expires = 0;
        //                //    //responseBase.ContentType = MimeMapping.GetMimeMapping(fileName);
        //                //    //responseBase.AddHeader("Content-Type", MimeMapping.GetMimeMapping(fileName));
        //                //    //responseBase.AddHeader("Content-Disposition", "attachment;filename=" + fileName);
        //                //    //responseBase.Cache.SetCacheability(HttpCacheability.NoCache);
        //                //    //responseBase(fileBytes);
        //                //    //responseBase.End();
        //                //}
        //                //using (FileStream fs = File.Open(fileFullPath, FileMode.Open))
        //                //{
        //                //    byte[] fileBytes = new byte[fs.Length];
        //                //    fs.Read(fileBytes, 0, Convert.ToInt32(fs.Length));
        //                //    fs.Close();

        //                //    responseBase.Buffer = true;
        //                //    responseBase.Expires = 0;
        //                //    responseBase.ContentType = MimeMapping.GetMimeMapping(fileName);
        //                //    responseBase.AddHeader("Content-Type", MimeMapping.GetMimeMapping(fileName));
        //                //    responseBase.AddHeader("Content-Disposition", "attachment;filename=" + fileName);
        //                //    responseBase.Cache.SetCacheability(HttpCacheability.NoCache);
        //                //    responseBase.BinaryWrite(fileBytes);
        //                //    responseBase.End();

        //                //}



        //                ///------------------------------------////
        //                ////save file in buffer
        //                //string strDestopPath = System.Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);

        //                //int contentLen;
        //                //using (FileStream fs = new FileStream("C:/Downloads/" + fileName, FileMode.OpenOrCreate))
        //                //{
        //                //    byte[] buff = new byte[2048];
        //                //    contentLen = ftpStream.Read(buff, 0, buff.Length);
        //                //    while (contentLen != 0)
        //                //    {
        //                //        fs.Write(buff, 0, buff.Length);
        //                //        contentLen = ftpStream.Read(buff, 0, buff.Length);
        //                //    }
        //                //    ftpRequest = null;
        //                //}
        //                ///------------------------------------////
        //            }
        //        }
        //        return null;
        //    }
        //    catch (WebException ex)
        //    {
        //        string status = ((FtpWebResponse)ex.Response).StatusDescription;
        //        if (request != null) request.Abort();
        //        throw new WebException(status, ex);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (request != null) request.Abort();
        //        throw ex;
        //    }
        //}

        public bool DeleteFile(string ftpAddress, NetworkCredential credentials)
        {
            Uri uri = new Uri(ftpAddress.StartsWith(FTP_SCHEME) ? ftpAddress : FTP_SCHEME + ftpAddress);
            if (uri.Scheme != Uri.UriSchemeFtp)
                return false;

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(uri);
            request.Method = WebRequestMethods.Ftp.DeleteFile;
            request.Credentials = credentials;
            request.Timeout = 120000;

            return ProcessFtpRequest(request);
        }

        private static bool ProcessFtpRequest(object data)
        {
            FtpWebRequest request = data as FtpWebRequest;
            if (request == null)
                return false;

            try
            {
                using (var response = request.GetResponse() as FtpWebResponse)
                {
                    if (response != null && response.StatusCode != FtpStatusCode.ClosingData)
                        return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}
