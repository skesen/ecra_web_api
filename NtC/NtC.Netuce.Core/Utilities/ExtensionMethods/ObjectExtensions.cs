﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Script.Serialization;
using System.Xml;
using NtC.Netuce.Core.CrossCuttingConcern.ExceptionHandling.Exceptions;
using System.Collections.ObjectModel;
using System.Data;

namespace NtC.Netuce.Core.Utilities.ExtensionMethods
{

    public class ExpandoJSONConverter : JavaScriptConverter
    {
        public override object Deserialize(IDictionary<string, object> dictionary, Type type, JavaScriptSerializer serializer)
        {
            throw new NotImplementedException();
        }
        public override IDictionary<string, object> Serialize(object obj, JavaScriptSerializer serializer)
        {
            var result = new Dictionary<string, object>();
            var dictionary = obj as IDictionary<string, object>;
            foreach (var item in dictionary)
                result.Add(item.Key, item.Value);
            return result;
        }
        public override IEnumerable<Type> SupportedTypes
        {
            get
            {
                return new ReadOnlyCollection<Type>(new Type[] { typeof(System.Dynamic.ExpandoObject) });
            }
        }
    }

    public static partial class Extensions
    {
        public static DataTable ToDataTable(this IEnumerable<dynamic> items)
        {
            var data = items.ToArray();
            if (!data.Any()) return null;

            var dt = new DataTable();
            foreach (var key in ((IDictionary<string, object>)data[0]).Keys)
            {
                dt.Columns.Add(key);
            }
            foreach (var d in data)
            {
                dt.Rows.Add(((IDictionary<string, object>)d).Values.ToArray());
            }
            return dt;
        }

        public static Dictionary<string, string> ObjectToDictionary(this object value)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            if (value != null)
            {
                foreach (
                    PropertyDescriptor descriptor in
                        TypeDescriptor.GetProperties(value))
                {
                    if (descriptor != null && descriptor.Name != null)
                    {
                        object propValue = descriptor.GetValue(value);
                        if (propValue != null)
                            dictionary.Add(descriptor.Name, String.Format("{0}", propValue));
                    }
                }
                return dictionary;
            }
            return null;
        }

        public static string ConvertExpandotoJson(object obj)
        {
            var serializer = new JavaScriptSerializer();
            serializer.RegisterConverters(new JavaScriptConverter[] { new ExpandoJSONConverter() });
            var json = serializer.Serialize(obj);
            return json;
        }
        public static dynamic GetDataRow(DbDataReader dataReader)
        {
            var dataRow = new ExpandoObject() as IDictionary<string, object>;
            for (var fieldCount = 0; fieldCount < dataReader.FieldCount; fieldCount++)
            {
                dataRow.Add(dataReader.GetName(fieldCount), dataReader[fieldCount]);

            }
            return dataRow;
        }
      
        public static IEnumerable Append(this IEnumerable first, params object[] second)
        {
            return first.OfType<object>().Concat(second);
        }
        public static IEnumerable<T> Append<T>(this IEnumerable<T> first, params T[] second)
        {
            return first.Concat(second);
        }
        public static IEnumerable Prepend(this IEnumerable first, params object[] second)
        {
            return second.Concat(first.OfType<object>());
        }
        public static IEnumerable<T> Prepend<T>(this IEnumerable<T> first, params T[] second)
        {
            return second.Concat(first);
        }
        public static bool PlusOrMinus(this object input, object compare, decimal plusOrMinus)
        {

            float inputFloat;
            float compareFloat;

            if (!float.TryParse(input.ToString(), out inputFloat))
                throw new InvalidArgumentException("Object must be convertable to type float");
            if (!float.TryParse(compare.ToString(), out compareFloat))
                throw new InvalidArgumentException("Object must be convertable to type float");


            if (Math.Abs(inputFloat - compareFloat) < .001F) return true;

            var baseNumber = inputFloat - compareFloat;

            return (baseNumber >= (0 - (float)plusOrMinus) && baseNumber <= (float)(plusOrMinus));

        }

        public static int ToInt(this decimal input)
        {
            return decimal.ToInt32(input);
        }

        public static int ToInt(this object input)
        {
            var output = 0;
            if (input == null) return output;
            var number = input.ToString().StripIntMarkers();


            int.TryParse(number, out output);

            return output;

        }

        public static short ToShort(this object input)
        {
            short output = 0;
            if (input == null) return output;
            var number = input.ToString().StripIntMarkers();

            short.TryParse(number, out output);

            return output;

        }

        public static byte ToByte(this object input)
        {
            byte output = 0;
            if (input == null) return output;
            var number = input.ToString().StripIntMarkers();


            byte.TryParse(number, out output);

            return output;

        }

        public static string MakeMoneyString(this object input, bool switchCommas, bool TL)
        {
            if (input == null) return (0).MakeMoneyString(false, TL);

            var value = input.ToDecimal(switchCommas);

            return value == decimal.MinValue ? (0).MakeMoneyString(false, TL) : value.MakeMoneyString(false, TL);
        }

        public static long ToLong(this object input)
        {
            if (input == null) return 0;
            long result;
            return long.TryParse(input.ToString(), out result) ? result : long.MinValue;

        }

        public static decimal ToDecimal(this object input)
        {
            return ToDecimal(input, false);

        }

        public static float ToFloat(this object input)
        {
            return ToFloat(input, false);

        }
        public static bool IsDefault<T>(this T value)
        {
            return (Equals(value, default(T)));
        }

        public static float ToFloat(this object input, bool switchCommas)
        {

            if (switchCommas)
            {
                input = input.ToString().Replace(",", "**COMMA**");
                input = input.ToString().Replace(".", "**PERIOD**");
                input = input.ToString().Replace("**PERIOD**", ",");
                input = input.ToString().Replace("**COMMA**", ".");

            }

            float result;
            return float.TryParse(input.ToString(), out result) ? result : 0F;

        }

        public static bool ToBool(this object input)
        {
            if (input == null) return false;
            bool output;
            bool.TryParse(input.ToString(), out output);
            return output;

        }

        public static DateTime ToDateTime(this object input)
        {
            DateTime result;
            return DateTime.TryParse(input.ToString(), out result) ? result : DateTime.MinValue;

        }

        public static DateTime ToDateTime(this object input, bool USStyle)
        {
            IFormatProvider format = USStyle ? new CultureInfo("en-US") : new CultureInfo("tr-TR");
            DateTime result;

            return DateTime.TryParse(input.ToString(), format, DateTimeStyles.None, out result) == true
                       ? result
                       : DateTime.MinValue;

        }

        public static decimal ToDecimal(this object input, bool switchCommas)
        {

            if (switchCommas)
            {
                input = input.ToString().Replace(",", "**COMMA**");
                input = input.ToString().Replace(".", "**PERIOD**");
                input = input.ToString().Replace("**PERIOD**", ",");
                input = input.ToString().Replace("**COMMA**", ".");

            }

            decimal result;
            return decimal.TryParse(input.ToString(), out result) ? result : decimal.Zero;

        }

        public static bool ImplementsInterface(this object input, string interfaceName)
        {
            if (input == null) return false;
            return input.GetType().GetInterface(interfaceName) != null;
        }

        public static T To<T>(this object convertableSource, T defaultValue = default(T), bool returnDefaultValue = false, IFormatProvider provider = null)
        {
            try
            {
                if (convertableSource == null)
                {
                    return returnDefaultValue ? default(T) : defaultValue;
                }
                if (typeof(T).IsGenericType)
                {
                    if (typeof(T).GetGenericArguments()[0].ToString() == "System.Guid")
                    {
                        return (T)((object)new Guid(convertableSource.ToString()));
                    }

                    if (convertableSource == DBNull.Value)
                    {
                        throw new ArgumentNullException();
                    }

                    return GetConvertedCultureValue<T>(convertableSource, provider);
                }

                if ((typeof(T).ToString() == "System.Guid"))
                {
                    return (T)((object)new Guid(convertableSource.ToString()));
                }

                if (typeof(T).BaseType == typeof(Enum))
                {
                    return (T)Enum.Parse(typeof(T), convertableSource.ToString());
                }

                if (typeof(T) != typeof(bool)) return GetConvertedCultureValue<T>(convertableSource, provider);
                var convertableSourceVal = convertableSource;

                if (convertableSource.ToString() == "0")
                {
                    convertableSourceVal = "false";
                }

                if (convertableSource.ToString() == "1")
                {
                    convertableSourceVal = "true";
                }

                return (T)Convert.ChangeType(convertableSourceVal, typeof(T));
            }
            catch (Exception)
            {
                if (returnDefaultValue)
                {
                    return default(T);
                }

                if (!Equals(defaultValue, default(T)))
                {
                    return defaultValue;
                }

                throw new ArgumentNullException();
            }
        }

        private static T GetConvertedCultureValue<T>(object value, IFormatProvider cultureInfo)
        {
            var tempValue = value;

            var destinationType = typeof(T);
            if (destinationType.IsGenericType && destinationType.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                if (tempValue == null)
                    return default(T);

                var nullableConverter = new NullableConverter(destinationType);
                destinationType = nullableConverter.UnderlyingType;
            }
            if (!(tempValue is IConvertible)) return (T)tempValue;
            var culture = cultureInfo;
            tempValue = (T)Convert.ChangeType(tempValue, destinationType, culture);
            return (T)tempValue;
        }

        private static string RenderToStringInternal(this object obj, ref HashSet<object> serialized, string prefix = "")
        {
            if (obj == null)
                return "";

            var type = obj.GetType();
            var typeInfo = type.GetTypeInfo();
            var underlyingType = Nullable.GetUnderlyingType(type);
            var underlyingTypeInfo = underlyingType?.GetTypeInfo();

            if (serialized.Contains(obj))
                return "<ref>";

            //referans obje ise başta serialized içine yerleştiriyoruz ki circular referans olmasın.
            if (!(obj is string) &&
                !typeInfo.IsPrimitive && !typeInfo.IsValueType && !typeInfo.IsEnum && (type != typeof(Type)) &&
                (underlyingType == null || (!underlyingTypeInfo.IsPrimitive && !underlyingTypeInfo.IsEnum && !underlyingTypeInfo.IsValueType)))
                serialized.Add(obj);

            try
            {
                if (obj is string || typeInfo.IsPrimitive || typeInfo.IsValueType || typeInfo.IsEnum ||
                    (underlyingType != null && underlyingTypeInfo.IsPrimitive))
                    return obj.ToString();

                if (type == typeof(Type))
                    return ((Type)obj).GetFriendlyName();

                var xmlDoc = obj as XmlDocument;
                if (xmlDoc != null)
                    return prefix + xmlDoc.OuterXml;

                var xmlNode = obj as XmlNode;
                if (xmlNode != null)
                    return prefix + xmlNode.OuterXml;

                var dictionary = obj as IDictionary;
                if (dictionary != null)
                {
                    if (dictionary.Count == 0)
                        return "";
                    var result = new StringBuilder();
                    result.AppendFormat("\r\n{0}{{\r\n", prefix);
                    foreach (DictionaryEntry kv in dictionary)
                    {
                        var rendered = kv.Value.RenderToStringInternal(ref serialized, prefix + "  ");

                        if (string.IsNullOrEmpty(rendered.Trim()))
                            continue;

                        result.Append(prefix);
                        result.AppendFormat("  {0} : {1}\r\n", kv.Key, rendered);
                    }
                    result.AppendFormat("{0}}}", prefix);
                    return result.ToString();
                }

                var enumerable = obj as IEnumerable;
                if (enumerable != null)
                {
                    var result = new StringBuilder();
                    result.AppendFormat("\r\n{0}[\r\n", prefix);
                    foreach (var item in enumerable.Cast<object>())
                    {
                        var rendered = item.RenderToStringInternal(ref serialized, prefix + "  ");
                        if (string.IsNullOrEmpty(rendered.Trim()))
                            continue;

                        result.Append(prefix);
                        result.AppendFormat("  {0}\r\n", rendered);
                    }
                    result.AppendFormat("{0}]\r\n", prefix);
                    return result.ToString();
                }
            }
            catch
            {
                return prefix + "";
            }

            //sadece IEnumerable ve IDictionary tipinde olan gizli tipler render edilir. haricindekilere gerek yok.
            if (!typeInfo.IsVisible)
                return obj.ToString();

            //complex obje ise 
            var properties = new Dictionary<string, object>();
            foreach (var propertyInfo in typeInfo.GetProperties(BindingFlags.Instance | BindingFlags.Public))
            {
                try
                {
                    properties[propertyInfo.Name] = propertyInfo.GetValue(obj);
                }
                catch
                {
                    properties[propertyInfo.Name] = null;
                }
            }
            return properties.RenderToStringInternal(ref serialized, prefix + "\t");
        }
        /// <summary>
        /// Objeyi yalın haliyle okunabilir string'e çevirir. Json veya Xml olarak serilize yerine hata fırlatmadan string'e çevirmeye çalışır.
        /// <remarks>Örnek olarak hatalardaki Exception'lara bakabilirsiniz.</remarks>
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public static string RenderToString(this object obj, string prefix = "")
        {
            var serialized = new HashSet<object>();
            return obj.RenderToStringInternal(ref serialized, prefix);
        }

        public static List<ExpandoObject> ToDynamicList(dynamic source)
        {
            return (List<ExpandoObject>)source;
        }
        public static void Swap<T>(ref T lhs, ref T rhs)
        {
            T temp = lhs;
            lhs = rhs;
            rhs = temp;
        }
    }
}
