﻿using System.Collections.Generic;
using System.Net;

namespace NtC.Netuce.Core.Utilities.ExtensionMethods
{
    public static partial class Extensions
    {

        private static List<HttpStatusCode> GetBadURLWebExceptionStatuses()
        {
            var returnValue = new List<HttpStatusCode>
                                  {
                                      HttpStatusCode.BadRequest,
                                      HttpStatusCode.ExpectationFailed,
                                      HttpStatusCode.Forbidden,
                                      HttpStatusCode.Gone,
                                      HttpStatusCode.MethodNotAllowed,
                                      HttpStatusCode.NoContent,
                                      HttpStatusCode.NotFound,
                                      HttpStatusCode.RequestUriTooLong,
                                      HttpStatusCode.Unused
                                  };
            return returnValue;

        }

        public static bool IsCausedByBadInvalidUrl(this WebException input)
        {
            if (input.Response == null) return false;
            var code = ((HttpWebResponse)input.Response).StatusCode;
            return (GetBadURLWebExceptionStatuses().Contains(code));
        }

    }
}
