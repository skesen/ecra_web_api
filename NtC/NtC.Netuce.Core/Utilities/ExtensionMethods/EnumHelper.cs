﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace NtC.Netuce.Core.Utilities.ExtensionMethods
{
    public static class EnumHelper
    {
        public static Dictionary<int, string> ConvertToDictionary(this Type enumType)
        {
            if (!enumType.IsEnum)
                throw new ApplicationException("GetListItems does not support non-enum types");

            var list = new Dictionary<int, string>();

            foreach (var field in enumType.GetFields(BindingFlags.Static | BindingFlags.GetField | BindingFlags.Public))
            {
                var key = (int)field.GetValue(null);
                var value = Enum.GetName(enumType, key);

                list.Add(key, value);
            }
            return list;
        }

        public static int ToInt(this Enum enumType)
        {
            return Convert.ToInt32(enumType);
        }

        public static List<T> EnumToList<T>()
        {
            Type enumType = typeof(T);

            if (enumType.BaseType != typeof(Enum))
                throw new ArgumentException("T must be of type System.Enum");

            Array enumValArray = Enum.GetValues(enumType);

            List<T> enumValList = new List<T>(enumValArray.Length);

            foreach (int val in enumValArray)
            {
                enumValList.Add((T)Enum.Parse(enumType, val.ToString().Replace("_", " ")));
            }

            return enumValList;
        }
    }
}
