﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Configuration;
using System.Web.Script.Serialization;

namespace NtC.Netuce.Core.Utilities.ExtensionMethods
{
    public class Utf8StringWriter : StringWriter
    {
        public override Encoding Encoding => Encoding.UTF8;
    }

    public static class Serialization
    {
        private static readonly Hashtable SerializerCache = new Hashtable();

        public static string XmlSerialize<T>(T item)
        {
            var serializer = new XmlSerializer(typeof(T));

            using (var stringWriter = new Utf8StringWriter())
            {
                serializer.Serialize(stringWriter, item);

                return stringWriter.ToString();
            }
        }

        public static string JsonSerialize<T>(T item, int? maxDepth = null)
        {
            if (maxDepth.HasValue)
            {
                using (var strWriter = new StringWriter())
                {
                    using (var jsonWriter = new CustomJsonTextWriter(strWriter))
                    {
                        Func<bool> include = () => jsonWriter.CurrentDepth <= maxDepth;
                        var resolver = new CustomContractResolver(include);
                        var serializer = new JsonSerializer { ContractResolver = resolver, ReferenceLoopHandling = ReferenceLoopHandling.Ignore };
                        serializer.Serialize(jsonWriter, item);
                    }
                    return strWriter.ToString();
                }
            }
            else
            {
                var serializer = new JsonSerializer() { MaxDepth = 3, ReferenceLoopHandling = ReferenceLoopHandling.Ignore };
                using (var stringWriter = new StringWriter())
                {
                    try
                    {
                        serializer.Serialize(stringWriter, item);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }

                    return stringWriter.ToString();
                }
            }
        }

        public static string SerializeToString<T>(T obj)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, obj);
                return writer.ToString();
            }
        }

        public static string Serialize<T>(T value)
        {
            if (value == null) return string.Empty;

            var xmlSerializer = new XmlSerializer(typeof(T));
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");

            using (var stringWriter = new StringWriter())
            {
                using (var xmlWriter = XmlWriter.Create(stringWriter, new XmlWriterSettings { NamespaceHandling = NamespaceHandling.OmitDuplicates, OmitXmlDeclaration = true, Indent = true }))
                {
                    xmlSerializer.Serialize(xmlWriter, value);
                    return stringWriter.ToString();
                }
            }
        }

        public static T XmlDeserialize<T>(string xmlString)
        {
            using (var stringReader = new StringReader(s: xmlString))
            {
                var cachekey = new { Type = typeof(T) };
                XmlSerializer serializer = (XmlSerializer)SerializerCache[cachekey];
                if (serializer == null)
                {
                    lock (SerializerCache)
                    {
                        serializer = (XmlSerializer)SerializerCache[cachekey];
                        if (serializer == null)
                        {
                            serializer = new XmlSerializer(type: typeof(T));
                            SerializerCache.Add(cachekey, serializer);
                        }
                    }
                }
                return (T)serializer.Deserialize(stringReader);
            }
        }

        public static T DownloadAndXmlDeserialize<T>(string url, out bool isError, out string xmlResponse)
        {
            try
            {
                xmlResponse = null;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    var userName = ConfigurationManager.AppSettings["userName"];
                    var password = ConfigurationManager.AppSettings["password"];
                    string _auth = string.Format("{0}:{1}", userName, password);
                    string _enc = Convert.ToBase64String(Encoding.ASCII.GetBytes(s: _auth));
                    string _cred = string.Format("{0} {1}", "Basic", _enc);
                    client.DefaultRequestHeaders.Add("Authorization", _cred);
                    HttpResponseMessage response = client.GetAsync(url).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        isError = false;
                    }
                    else
                    {
                        isError = true;
                        xmlResponse = response.Content.ReadAsStringAsync().Result;
                        return default(T);
                    }

                    using (Stream xmlStream = response.Content.ReadAsStreamAsync().Result)
                    {
                        var cachekey = new { Type = typeof(T) };
                        lock (SerializerCache)
                        {
                            XmlSerializer serializer = (XmlSerializer)SerializerCache[cachekey];
                            if (serializer == null)
                            {
                                lock (SerializerCache)
                                {
                                    serializer = (XmlSerializer)SerializerCache[cachekey];
                                    if (serializer == null)
                                    {
                                        serializer = new XmlSerializer(type: typeof(T));
                                        SerializerCache.Add(cachekey, serializer);
                                    }
                                }
                            }
                            return (T)serializer.Deserialize(xmlStream);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                isError = true;
                throw ex;
            }
        }

        public static object Deserialize(string xml, Type type)
        {
            XmlSerializer xs = new XmlSerializer(type);
            StringReader sr = new StringReader(xml);
            XmlTextReader xr = new XmlTextReader(sr);

            return xs.Deserialize(xr);
        }

        public interface ISerializerAdapter
        {
            string Serialize(object obj);

            T Deserialize<T>(string input);
        }

        public class JavaScriptSerializerAdapter : ISerializerAdapter
        {
            private JavaScriptSerializer serializer;

            public JavaScriptSerializerAdapter()
            {
                serializer = new JavaScriptSerializer();
            }

            public string Serialize(object obj)
            {
                return serializer.Serialize(obj);
            }

            public T Deserialize<T>(string input)
            {
                try
                {
                    var deserializeObject = JsonConvert.DeserializeObject<T>(input);
                    //var deserializeObject = Jil.JSON.Deserialize<T>(input);
                    return deserializeObject;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public class CustomJsonTextWriter : JsonTextWriter
        {
            public CustomJsonTextWriter(TextWriter textWriter) : base(textWriter)
            {
            }

            public int CurrentDepth { get; private set; }

            public override void WriteStartObject()
            {
                CurrentDepth++;
                base.WriteStartObject();
            }

            public override void WriteEndObject()
            {
                CurrentDepth--;
                base.WriteEndObject();
            }
        }

        public class CustomContractResolver : DefaultContractResolver
        {
            private readonly Func<bool> _includeProperty;

            public CustomContractResolver(Func<bool> includeProperty)
            {
                _includeProperty = includeProperty;
            }

            protected override JsonProperty CreateProperty(
                MemberInfo member, MemberSerialization memberSerialization)
            {
                var property = base.CreateProperty(member, memberSerialization);
                var shouldSerialize = property.ShouldSerialize;
                property.ShouldSerialize = obj => _includeProperty() &&
                                                  (shouldSerialize == null ||
                                                   shouldSerialize(obj));
                return property;
            }
        }

        public sealed class DynamicJsonConverter : JavaScriptConverter
        {
            public override object Deserialize(IDictionary<string, object> dictionary, Type type, JavaScriptSerializer serializer)
            {
                if (dictionary == null)
                    throw new ArgumentNullException("dictionary");

                return type == typeof(object) ? new DynamicJsonObject(dictionary) : null;
            }

            public override IDictionary<string, object> Serialize(object obj, JavaScriptSerializer serializer)
            {
                throw new NotImplementedException();
            }

            public override IEnumerable<Type> SupportedTypes
            {
                get { return new ReadOnlyCollection<Type>(new List<Type>(new[] { typeof(object) })); }
            }

            #region Nested type: DynamicJsonObject

            private sealed class DynamicJsonObject : DynamicObject
            {
                private readonly IDictionary<string, object> _dictionary;

                public DynamicJsonObject(IDictionary<string, object> dictionary)
                {
                    if (dictionary == null)
                        throw new ArgumentNullException("dictionary");
                    _dictionary = dictionary;
                }

                public override string ToString()
                {
                    var sb = new StringBuilder("{");
                    ToString(sb);
                    return sb.ToString();
                }

                private void ToString(StringBuilder sb)
                {
                    var firstInDictionary = true;
                    foreach (var pair in _dictionary)
                    {
                        if (!firstInDictionary)
                            sb.Append(",");
                        firstInDictionary = false;
                        var value = pair.Value;
                        var name = pair.Key;
                        if (value is string)
                        {
                            sb.AppendFormat("{0}:\"{1}\"", name, value);
                        }
                        else if (value is IDictionary<string, object>)
                        {
                            new DynamicJsonObject((IDictionary<string, object>)value).ToString(sb);
                        }
                        else if (value is ArrayList)
                        {
                            sb.Append(name + ":[");
                            var firstInArray = true;
                            foreach (var arrayValue in (ArrayList)value)
                            {
                                if (!firstInArray)
                                    sb.Append(",");
                                firstInArray = false;
                                if (arrayValue is IDictionary<string, object>)
                                    new DynamicJsonObject((IDictionary<string, object>)arrayValue).ToString(sb);
                                else if (arrayValue is string)
                                    sb.AppendFormat("\"{0}\"", arrayValue);
                                else
                                    sb.AppendFormat("{0}", arrayValue);
                            }
                            sb.Append("]");
                        }
                        else
                        {
                            sb.AppendFormat("{0}:{1}", name, value);
                        }
                    }
                    sb.Append("}");
                }

                public override bool TryGetMember(GetMemberBinder binder, out object result)
                {
                    if (!_dictionary.TryGetValue(binder.Name, out result))
                    {
                        // return null to avoid exception.  caller can check for null this way...
                        result = null;
                        return true;
                    }

                    result = WrapResultObject(result);
                    return true;
                }

                public override bool TryGetIndex(GetIndexBinder binder, object[] indexes, out object result)
                {
                    if (indexes.Length == 1 && indexes[0] != null)
                    {
                        if (!_dictionary.TryGetValue(indexes[0].ToString(), out result))
                        {
                            // return null to avoid exception.  caller can check for null this way...
                            result = null;
                            return true;
                        }

                        result = WrapResultObject(result);
                        return true;
                    }

                    return base.TryGetIndex(binder, indexes, out result);
                }

                private static object WrapResultObject(object result)
                {
                    var dictionary = result as IDictionary<string, object>;
                    if (dictionary != null)
                        return new DynamicJsonObject(dictionary);

                    var arrayList = result as ArrayList;
                    if (arrayList != null && arrayList.Count > 0)
                    {
                        return arrayList[0] is IDictionary<string, object>
                            ? new List<object>(arrayList.Cast<IDictionary<string, object>>().Select(x => new DynamicJsonObject(x)))
                            : new List<object>(arrayList.Cast<object>());
                    }

                    return result;
                }
            }

            #endregion Nested type: DynamicJsonObject
        }
    }
}
