﻿namespace NtC.Netuce.Core.Utilities.EnumUtilities
{
    public enum NotifyType
    {
        Error,
        Warning,
        Info,
        Success
    }
}
