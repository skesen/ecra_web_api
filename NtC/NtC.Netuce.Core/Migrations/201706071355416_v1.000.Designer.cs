// <auto-generated />
namespace NtC.Netuce.Core.Concrete.EntityFramework
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class v1000 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(v1000));
        
        string IMigrationMetadata.Id
        {
            get { return "201706071355416_v1.000"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
