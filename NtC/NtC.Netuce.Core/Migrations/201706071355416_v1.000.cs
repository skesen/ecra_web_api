namespace NtC.Netuce.Core.Concrete.EntityFramework
{
    using System;
    using System.Data.Entity.Migrations;
    
    //public partial class v1000 : DbMigration
    //{
    //    public override void Up()
    //    {
    //        CreateTable(
    //            "dbo.Address",
    //            c => new
    //                {
    //                    addressid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    provinceid = c.String(unicode: false),
    //                    districtid = c.String(unicode: false),
    //                    countryid = c.String(unicode: false),
    //                    openaddress = c.String(unicode: false),
    //                    description = c.String(unicode: false),
    //                    tablename = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.addressid);
            
    //        CreateTable(
    //            "dbo.Client",
    //            c => new
    //                {
    //                    clientid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    secret = c.String(nullable: false, unicode: false),
    //                    name = c.String(nullable: false, maxLength: 100, storeType: "nvarchar"),
    //                    applicationtype = c.String(unicode: false),
    //                    active = c.Boolean(nullable: false),
    //                    refreshtokenlifetime = c.Int(nullable: false),
    //                    allowedorigin = c.String(maxLength: 100, storeType: "nvarchar"),
    //                })
    //            .PrimaryKey(t => t.clientid);
            
    //        CreateTable(
    //            "dbo.Comment",
    //            c => new
    //                {
    //                    commentid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    content = c.String(unicode: false),
    //                    date = c.String(unicode: false),
    //                    userid = c.String(unicode: false),
    //                    commentedid = c.String(unicode: false),
    //                    commentedtable = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.commentid);
            
    //        CreateTable(
    //            "dbo.Company",
    //            c => new
    //                {
    //                    companyid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    description = c.String(unicode: false),
    //                    logo = c.String(unicode: false),
    //                    contactpersonid = c.String(unicode: false),
    //                    addressid = c.String(unicode: false),
    //                    legaladdress = c.String(unicode: false),
    //                    note = c.String(unicode: false),
    //                    annualincome = c.String(unicode: false),
    //                    employees = c.String(unicode: false),
    //                    companytype = c.String(unicode: false),
    //                    phone = c.String(unicode: false),
    //                    mail = c.String(unicode: false),
    //                    provinceid = c.String(unicode: false),
    //                    districtid = c.String(unicode: false),
    //                    countryid = c.String(unicode: false),
    //                    state = c.String(unicode: false),
    //                    zip = c.String(unicode: false),
    //                    taxno = c.String(unicode: false),
    //                    taxoffice = c.String(unicode: false),
    //                    sectorid = c.String(unicode: false),
    //                    point = c.String(unicode: false),
    //                    address = c.String(unicode: false),
    //                    source = c.String(unicode: false),
    //                    fax = c.String(unicode: false),
    //                    legalname = c.String(unicode: false),
    //                    contactsourceid = c.String(unicode: false),
    //                    contactstatusid = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.companyid);
            
    //        CreateTable(
    //            "dbo.CompanyTag",
    //            c => new
    //                {
    //                    companytagid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    companyid = c.String(unicode: false),
    //                    tagid = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.companytagid);
            
    //        CreateTable(
    //            "dbo.ContactSource",
    //            c => new
    //                {
    //                    contactsourceid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    category = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.contactsourceid);
            
    //        CreateTable(
    //            "dbo.ContactStatus",
    //            c => new
    //                {
    //                    contactstatusid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    category = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.contactstatusid);
            
    //        CreateTable(
    //            "dbo.Country",
    //            c => new
    //                {
    //                    countryid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.countryid);
            
    //        CreateTable(
    //            "dbo.Department",
    //            c => new
    //                {
    //                    departmentid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    category = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.departmentid);
            
    //        CreateTable(
    //            "dbo.District",
    //            c => new
    //                {
    //                    districtid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    provinceid = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.districtid);
            
    //        CreateTable(
    //            "dbo.Individual",
    //            c => new
    //                {
    //                    individualid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    companyid = c.String(unicode: false),
    //                    name = c.String(unicode: false),
    //                    surname = c.String(unicode: false),
    //                    photo = c.String(unicode: false),
    //                    note = c.String(unicode: false),
    //                    title = c.String(unicode: false),
    //                    gender = c.String(unicode: false),
    //                    source = c.String(unicode: false),
    //                    headed = c.String(unicode: false),
    //                    phone = c.String(unicode: false),
    //                    mobilephone = c.String(unicode: false),
    //                    mail = c.String(unicode: false),
    //                    provinceid = c.String(unicode: false),
    //                    districtid = c.String(unicode: false),
    //                    countryid = c.String(unicode: false),
    //                    state = c.String(unicode: false),
    //                    worktitleid = c.String(unicode: false),
    //                    address = c.String(unicode: false),
    //                    contactsourceid = c.String(unicode: false),
    //                    contactstatusid = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.individualid);
            
    //        CreateTable(
    //            "dbo.IndividualAccess",
    //            c => new
    //                {
    //                    individualaccessid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    individualid = c.String(unicode: false),
    //                    accessid = c.String(unicode: false),
    //                    accessidtype = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.individualaccessid);
            
    //        CreateTable(
    //            "dbo.IndividualTag",
    //            c => new
    //                {
    //                    individualtagid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    individualid = c.String(unicode: false),
    //                    tagid = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.individualtagid);
            
    //        CreateTable(
    //            "dbo.Library",
    //            c => new
    //                {
    //                    libraryid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    path = c.String(unicode: false),
    //                    identity = c.String(unicode: false),
    //                    size = c.String(unicode: false),
    //                    type = c.String(unicode: false),
    //                    extension = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.libraryid);
            
    //        CreateTable(
    //            "dbo.Link",
    //            c => new
    //                {
    //                    linkid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    alinkid = c.String(unicode: false),
    //                    blinkid = c.String(unicode: false),
    //                    linktypeid = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.linkid);
            
    //        CreateTable(
    //            "dbo.LinkCategory",
    //            c => new
    //                {
    //                    linkcategoryid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    alinktable = c.String(unicode: false),
    //                    blinktable = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.linkcategoryid);
            
    //        CreateTable(
    //            "dbo.LinkType",
    //            c => new
    //                {
    //                    linktypeid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    siblingid = c.String(unicode: false),
    //                    linkcategoryid = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.linktypeid);
            
    //        CreateTable(
    //            "dbo.Phone",
    //            c => new
    //                {
    //                    phoneid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    phonenumber = c.String(unicode: false),
    //                    tablename = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.phoneid);
            
    //        CreateTable(
    //            "dbo.Potential",
    //            c => new
    //                {
    //                    potentialid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    companyname = c.String(unicode: false),
    //                    individualname = c.String(unicode: false),
    //                    individualsurname = c.String(unicode: false),
    //                    description = c.String(unicode: false),
    //                    provinceid = c.String(unicode: false),
    //                    districtid = c.String(unicode: false),
    //                    countryid = c.String(unicode: false),
    //                    mail = c.String(unicode: false),
    //                    phone = c.String(unicode: false),
    //                    fax = c.String(unicode: false),
    //                    mobilephone = c.String(unicode: false),
    //                    facebookurl = c.String(unicode: false),
    //                    instagramurl = c.String(unicode: false),
    //                    address = c.String(unicode: false),
    //                    twitterurl = c.String(unicode: false),
    //                    linkedinurl = c.String(unicode: false),
    //                    url = c.String(unicode: false),
    //                    annualincome = c.String(unicode: false),
    //                    employees = c.String(unicode: false),
    //                    referencephone = c.String(unicode: false),
    //                    worktitleid = c.String(unicode: false),
    //                    referencemail = c.String(unicode: false),
    //                    reference = c.String(unicode: false),
    //                    departmentid = c.String(unicode: false),
    //                    potentialsourceid = c.String(unicode: false),
    //                    potentialdescription = c.String(unicode: false),
    //                    potentialstateid = c.String(unicode: false),
    //                    potentialstatedescription = c.String(unicode: false),
    //                    libraryid = c.String(unicode: false),
    //                    visibilityid = c.String(unicode: false),
    //                    headed = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.potentialid);
            
    //        CreateTable(
    //            "dbo.PotentialSource",
    //            c => new
    //                {
    //                    potentialsourceid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    category = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.potentialsourceid);
            
    //        CreateTable(
    //            "dbo.PotentialState",
    //            c => new
    //                {
    //                    potentialstateid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    category = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.potentialstateid);
            
    //        CreateTable(
    //            "dbo.PotentialTag",
    //            c => new
    //                {
    //                    potentialtagid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    potentialid = c.String(unicode: false),
    //                    tagid = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.potentialtagid);
            
    //        CreateTable(
    //            "dbo.Product",
    //            c => new
    //                {
    //                    productid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    code = c.String(unicode: false),
    //                    name = c.String(unicode: false),
    //                    description = c.String(unicode: false),
    //                    count = c.Int(),
    //                    currency = c.String(unicode: false),
    //                    unit = c.String(unicode: false),
    //                    price = c.Decimal(nullable: false, precision: 18, scale: 2),
    //                    productcategoryid = c.String(unicode: false),
    //                    source = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.productid);
            
    //        CreateTable(
    //            "dbo.ProductCategory",
    //            c => new
    //                {
    //                    productcategoryid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    description = c.String(unicode: false),
    //                    parentid = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.productcategoryid);
            
    //        CreateTable(
    //            "dbo.ProductTag",
    //            c => new
    //                {
    //                    producttagid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    productid = c.String(unicode: false),
    //                    tagid = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.producttagid);
            
    //        CreateTable(
    //            "dbo.Project",
    //            c => new
    //                {
    //                    projectid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    reporter = c.Int(nullable: false),
    //                    shortname = c.String(unicode: false),
    //                    label = c.String(unicode: false),
    //                    projectcategoryid = c.String(unicode: false),
    //                    projectmanagerid = c.String(unicode: false),
    //                    startdate = c.DateTime(precision: 0),
    //                    enddate = c.DateTime(precision: 0),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                    Tag_tagid = c.String(maxLength: 128, storeType: "nvarchar"),
    //                })
    //            .PrimaryKey(t => t.projectid)
    //            .ForeignKey("dbo.Tag", t => t.Tag_tagid)
    //            .Index(t => t.Tag_tagid);
            
    //        CreateTable(
    //            "dbo.ProjectCategory",
    //            c => new
    //                {
    //                    projectcategoryid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.projectcategoryid);
            
    //        CreateTable(
    //            "dbo.ProjectTag",
    //            c => new
    //                {
    //                    projecttagid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    projectid = c.String(unicode: false),
    //                    tagid = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.projecttagid);
            
    //        CreateTable(
    //            "dbo.Province",
    //            c => new
    //                {
    //                    provinceid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    countryid = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.provinceid);
            
    //        CreateTable(
    //            "dbo.RefreshToken",
    //            c => new
    //                {
    //                    refreshtokenid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    subject = c.String(nullable: false, maxLength: 50, storeType: "nvarchar"),
    //                    clientid = c.String(nullable: false, maxLength: 50, storeType: "nvarchar"),
    //                    issuedutc = c.DateTime(nullable: false, precision: 0),
    //                    expiresutc = c.DateTime(nullable: false, precision: 0),
    //                    protectedticket = c.String(nullable: false, unicode: false),
    //                })
    //            .PrimaryKey(t => t.refreshtokenid);
            
    //        CreateTable(
    //            "dbo.Reminder",
    //            c => new
    //                {
    //                    reminderid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    note = c.String(unicode: false),
    //                    reminddate = c.String(unicode: false),
    //                    referencedate = c.DateTime(nullable: false, precision: 0),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.reminderid);
            
    //        CreateTable(
    //            "dbo.RoleAction",
    //            c => new
    //                {
    //                    roleactionid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    actiongroupid = c.String(unicode: false),
    //                    description = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.roleactionid);
            
    //        CreateTable(
    //            "dbo.RolePermission",
    //            c => new
    //                {
    //                    rolepermissionid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    value = c.Boolean(nullable: false),
    //                    roleactionid = c.String(unicode: false),
    //                    userroleid = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.rolepermissionid);
            
    //        CreateTable(
    //            "dbo.AspNetRoles",
    //            c => new
    //                {
    //                    Id = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    Name = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                })
    //            .PrimaryKey(t => t.Id)
    //            .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
    //        CreateTable(
    //            "dbo.AspNetUserRoles",
    //            c => new
    //                {
    //                    UserId = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    RoleId = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                })
    //            .PrimaryKey(t => new { t.UserId, t.RoleId })
    //            .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
    //            .ForeignKey("dbo.AspNetUser", t => t.UserId, cascadeDelete: true)
    //            .Index(t => t.UserId)
    //            .Index(t => t.RoleId);
            
    //        CreateTable(
    //            "dbo.Sector",
    //            c => new
    //                {
    //                    sectorid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.sectorid);
            
    //        CreateTable(
    //            "dbo.SocialMedia",
    //            c => new
    //                {
    //                    socialmediaid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    url = c.String(unicode: false),
    //                    tablename = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.socialmediaid);
            
    //        CreateTable(
    //            "dbo.TableReplace",
    //            c => new
    //                {
    //                    tablereplaceid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    tableid = c.String(unicode: false),
    //                    trash_tableid = c.String(unicode: false),
    //                    tablename = c.String(unicode: false),
    //                    trash_tablename = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.tablereplaceid);
            
    //        CreateTable(
    //            "dbo.Tag",
    //            c => new
    //                {
    //                    tagid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    category = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.tagid);
            
    //        CreateTable(
    //            "dbo.Task",
    //            c => new
    //                {
    //                    taskid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    description = c.String(unicode: false),
    //                    projectid = c.String(unicode: false),
    //                    parenttaskid = c.String(unicode: false),
    //                    startdate = c.DateTime(precision: 0),
    //                    enddate = c.DateTime(precision: 0),
    //                    reporterid = c.String(unicode: false),
    //                    assigneeid = c.String(unicode: false),
    //                    taskcategoryid = c.String(unicode: false),
    //                    tasktypeid = c.String(unicode: false),
    //                    shortname = c.String(unicode: false),
    //                    priority = c.String(unicode: false),
    //                    repeattypeid = c.String(unicode: false),
    //                    taskstatusid = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.taskid);
            
    //        CreateTable(
    //            "dbo.TaskAttachment",
    //            c => new
    //                {
    //                    taskattachmentid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    taskid = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    folderid = c.String(unicode: false),
    //                    url = c.String(unicode: false),
    //                    type = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.taskattachmentid)
    //            .ForeignKey("dbo.Task", t => t.taskid)
    //            .Index(t => t.taskid);
            
    //        CreateTable(
    //            "dbo.TaskCategory",
    //            c => new
    //                {
    //                    taskcategoryid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    description = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.taskcategoryid);
            
    //        CreateTable(
    //            "dbo.TaskComment",
    //            c => new
    //                {
    //                    taskcommentid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    taskid = c.String(unicode: false),
    //                    note = c.String(unicode: false),
    //                    userid = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.taskcommentid);
            
    //        CreateTable(
    //            "dbo.TaskLink",
    //            c => new
    //                {
    //                    tasklinkid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    taskid = c.String(unicode: false),
    //                    linktype = c.String(unicode: false),
    //                    linkid = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.tasklinkid);
            
    //        CreateTable(
    //            "dbo.TaskStatus",
    //            c => new
    //                {
    //                    taskstatusid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.taskstatusid);
            
    //        CreateTable(
    //            "dbo.TaskType",
    //            c => new
    //                {
    //                    tasktypeid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.tasktypeid);
            
    //        CreateTable(
    //            "dbo.Team",
    //            c => new
    //                {
    //                    teamid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.teamid);
            
    //        CreateTable(
    //            "dbo.TeamMember",
    //            c => new
    //                {
    //                    teammemberid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    teamid = c.String(unicode: false),
    //                    userid = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.teammemberid);
            
    //        CreateTable(
    //            "dbo.Trash_Address",
    //            c => new
    //                {
    //                    addressid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    provinceid = c.String(unicode: false),
    //                    districtid = c.String(unicode: false),
    //                    countryid = c.String(unicode: false),
    //                    openaddress = c.String(unicode: false),
    //                    description = c.String(unicode: false),
    //                    tablename = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.addressid);
            
    //        CreateTable(
    //            "dbo.Trash_Comment",
    //            c => new
    //                {
    //                    commentid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    content = c.String(unicode: false),
    //                    date = c.String(unicode: false),
    //                    userid = c.String(unicode: false),
    //                    commentedid = c.String(unicode: false),
    //                    commentedtable = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.commentid);
            
    //        CreateTable(
    //            "dbo.Trash_Company",
    //            c => new
    //                {
    //                    companyid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    description = c.String(unicode: false),
    //                    logo = c.String(unicode: false),
    //                    contactpersonid = c.String(unicode: false),
    //                    addressid = c.String(unicode: false),
    //                    legaladdress = c.String(unicode: false),
    //                    note = c.String(unicode: false),
    //                    annualincome = c.String(unicode: false),
    //                    employees = c.String(unicode: false),
    //                    companytype = c.String(unicode: false),
    //                    phone = c.String(unicode: false),
    //                    mail = c.String(unicode: false),
    //                    provinceid = c.String(unicode: false),
    //                    districtid = c.String(unicode: false),
    //                    countryid = c.String(unicode: false),
    //                    state = c.String(unicode: false),
    //                    zip = c.String(unicode: false),
    //                    taxno = c.String(unicode: false),
    //                    taxoffice = c.String(unicode: false),
    //                    sectorid = c.String(unicode: false),
    //                    point = c.String(unicode: false),
    //                    address = c.String(unicode: false),
    //                    source = c.String(unicode: false),
    //                    fax = c.String(unicode: false),
    //                    legalname = c.String(unicode: false),
    //                    contactsourceid = c.String(unicode: false),
    //                    contactstatusid = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.companyid);
            
    //        CreateTable(
    //            "dbo.Trash_CompanyTag",
    //            c => new
    //                {
    //                    companytagid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    companyid = c.String(unicode: false),
    //                    tagid = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.companytagid);
            
    //        CreateTable(
    //            "dbo.Trash_ContactSource",
    //            c => new
    //                {
    //                    contactsourceid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    category = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.contactsourceid);
            
    //        CreateTable(
    //            "dbo.Trash_ContactStatus",
    //            c => new
    //                {
    //                    contactstatusid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    category = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.contactstatusid);
            
    //        CreateTable(
    //            "dbo.Trash_Country",
    //            c => new
    //                {
    //                    countryid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.countryid);
            
    //        CreateTable(
    //            "dbo.Trash_Department",
    //            c => new
    //                {
    //                    departmentid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    category = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.departmentid);
            
    //        CreateTable(
    //            "dbo.Trash_District",
    //            c => new
    //                {
    //                    districtid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    provinceid = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.districtid);
            
    //        CreateTable(
    //            "dbo.Trash_Individual",
    //            c => new
    //                {
    //                    individualid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    companyid = c.String(unicode: false),
    //                    name = c.String(unicode: false),
    //                    surname = c.String(unicode: false),
    //                    photo = c.String(unicode: false),
    //                    note = c.String(unicode: false),
    //                    title = c.String(unicode: false),
    //                    gender = c.String(unicode: false),
    //                    source = c.String(unicode: false),
    //                    headed = c.String(unicode: false),
    //                    phone = c.String(unicode: false),
    //                    mobilephone = c.String(unicode: false),
    //                    mail = c.String(unicode: false),
    //                    provinceid = c.String(unicode: false),
    //                    districtid = c.String(unicode: false),
    //                    countryid = c.String(unicode: false),
    //                    state = c.String(unicode: false),
    //                    worktitleid = c.String(unicode: false),
    //                    address = c.String(unicode: false),
    //                    contactsourceid = c.String(unicode: false),
    //                    contactstatusid = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.individualid);
            
    //        CreateTable(
    //            "dbo.Trash_IndividualAccess",
    //            c => new
    //                {
    //                    individualaccessid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    individualid = c.String(unicode: false),
    //                    accessid = c.String(unicode: false),
    //                    accessidtype = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.individualaccessid);
            
    //        CreateTable(
    //            "dbo.Trash_IndividualTag",
    //            c => new
    //                {
    //                    individualtagid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    individualid = c.String(unicode: false),
    //                    tagid = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.individualtagid);
            
    //        CreateTable(
    //            "dbo.Trash_Library",
    //            c => new
    //                {
    //                    libraryid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    path = c.String(unicode: false),
    //                    identity = c.String(unicode: false),
    //                    size = c.String(unicode: false),
    //                    type = c.String(unicode: false),
    //                    extension = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.libraryid);
            
    //        CreateTable(
    //            "dbo.Trash_Link",
    //            c => new
    //                {
    //                    linkid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    alinkid = c.String(unicode: false),
    //                    blinkid = c.String(unicode: false),
    //                    linktypeid = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.linkid);
            
    //        CreateTable(
    //            "dbo.Trash_LinkCategory",
    //            c => new
    //                {
    //                    linkcategoryid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    alinktable = c.String(unicode: false),
    //                    blinktable = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.linkcategoryid);
            
    //        CreateTable(
    //            "dbo.Trash_LinkType",
    //            c => new
    //                {
    //                    linktypeid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    siblingid = c.String(unicode: false),
    //                    linkcategoryid = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.linktypeid);
            
    //        CreateTable(
    //            "dbo.Trash_Phone",
    //            c => new
    //                {
    //                    phoneid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    phonenumber = c.String(unicode: false),
    //                    tablename = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.phoneid);
            
    //        CreateTable(
    //            "dbo.Trash_Potential",
    //            c => new
    //                {
    //                    potentialid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    companyname = c.String(unicode: false),
    //                    individualname = c.String(unicode: false),
    //                    individualsurname = c.String(unicode: false),
    //                    description = c.String(unicode: false),
    //                    provinceid = c.String(unicode: false),
    //                    districtid = c.String(unicode: false),
    //                    countryid = c.String(unicode: false),
    //                    mail = c.String(unicode: false),
    //                    phone = c.String(unicode: false),
    //                    fax = c.String(unicode: false),
    //                    mobilephone = c.String(unicode: false),
    //                    facebookurl = c.String(unicode: false),
    //                    instagramurl = c.String(unicode: false),
    //                    address = c.String(unicode: false),
    //                    twitterurl = c.String(unicode: false),
    //                    linkedinurl = c.String(unicode: false),
    //                    url = c.String(unicode: false),
    //                    annualincome = c.String(unicode: false),
    //                    employees = c.String(unicode: false),
    //                    referencephone = c.String(unicode: false),
    //                    worktitleid = c.String(unicode: false),
    //                    referencemail = c.String(unicode: false),
    //                    reference = c.String(unicode: false),
    //                    departmentid = c.String(unicode: false),
    //                    potentialsourceid = c.String(unicode: false),
    //                    potentialdescription = c.String(unicode: false),
    //                    potentialstateid = c.String(unicode: false),
    //                    potentialstatedescription = c.String(unicode: false),
    //                    libraryid = c.String(unicode: false),
    //                    visibilityid = c.String(unicode: false),
    //                    headed = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.potentialid);
            
    //        CreateTable(
    //            "dbo.Trash_PotentialSource",
    //            c => new
    //                {
    //                    potentialsourceid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    category = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.potentialsourceid);
            
    //        CreateTable(
    //            "dbo.Trash_PotentialState",
    //            c => new
    //                {
    //                    potentialstateid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    category = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.potentialstateid);
            
    //        CreateTable(
    //            "dbo.Trash_PotentialTag",
    //            c => new
    //                {
    //                    potentialtagid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    potentialid = c.String(unicode: false),
    //                    tagid = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.potentialtagid);
            
    //        CreateTable(
    //            "dbo.Trash_Product",
    //            c => new
    //                {
    //                    productid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    code = c.String(unicode: false),
    //                    name = c.String(unicode: false),
    //                    description = c.String(unicode: false),
    //                    count = c.Int(),
    //                    currency = c.String(unicode: false),
    //                    unit = c.String(unicode: false),
    //                    price = c.Decimal(nullable: false, precision: 18, scale: 2),
    //                    productcategoryid = c.String(unicode: false),
    //                    source = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.productid);
            
    //        CreateTable(
    //            "dbo.Trash_ProductCategory",
    //            c => new
    //                {
    //                    productcategoryid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    description = c.String(unicode: false),
    //                    parentid = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.productcategoryid);
            
    //        CreateTable(
    //            "dbo.Trash_ProductTag",
    //            c => new
    //                {
    //                    producttagid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    productid = c.String(unicode: false),
    //                    tagid = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.producttagid);
            
    //        CreateTable(
    //            "dbo.Trash_Project",
    //            c => new
    //                {
    //                    projectid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    reporter = c.Int(nullable: false),
    //                    shortname = c.String(unicode: false),
    //                    label = c.String(unicode: false),
    //                    projectcategoryid = c.String(unicode: false),
    //                    projectmanagerid = c.String(unicode: false),
    //                    startdate = c.DateTime(precision: 0),
    //                    enddate = c.DateTime(precision: 0),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.projectid);
            
    //        CreateTable(
    //            "dbo.Trash_ProjectCategory",
    //            c => new
    //                {
    //                    projectcategoryid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.projectcategoryid);
            
    //        CreateTable(
    //            "dbo.Trash_ProjectTag",
    //            c => new
    //                {
    //                    projecttagid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    projectid = c.String(unicode: false),
    //                    tagid = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.projecttagid);
            
    //        CreateTable(
    //            "dbo.Trash_Province",
    //            c => new
    //                {
    //                    provinceid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    countryid = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.provinceid);
            
    //        CreateTable(
    //            "dbo.Trash_Reminder",
    //            c => new
    //                {
    //                    reminderid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    note = c.String(unicode: false),
    //                    reminddate = c.String(unicode: false),
    //                    referencedate = c.DateTime(nullable: false, precision: 0),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.reminderid);
            
    //        CreateTable(
    //            "dbo.Trash_RoleAction",
    //            c => new
    //                {
    //                    roleactionid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    actiongroupid = c.String(unicode: false),
    //                    description = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.roleactionid);
            
    //        CreateTable(
    //            "dbo.Trash_RolePermission",
    //            c => new
    //                {
    //                    rolepermissionid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    value = c.Boolean(nullable: false),
    //                    roleactionid = c.String(unicode: false),
    //                    userroleid = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.rolepermissionid);
            
    //        CreateTable(
    //            "dbo.Trash_Sector",
    //            c => new
    //                {
    //                    sectorid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.sectorid);
            
    //        CreateTable(
    //            "dbo.Trash_SocialMedia",
    //            c => new
    //                {
    //                    socialmediaid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    url = c.String(unicode: false),
    //                    tablename = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.socialmediaid);
            
    //        CreateTable(
    //            "dbo.Trash_Task",
    //            c => new
    //                {
    //                    taskid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    description = c.String(unicode: false),
    //                    projectid = c.String(unicode: false),
    //                    shortname = c.String(unicode: false),
    //                    parenttaskid = c.String(unicode: false),
    //                    startdate = c.DateTime(precision: 0),
    //                    enddate = c.DateTime(precision: 0),
    //                    reporterid = c.String(unicode: false),
    //                    assigneeid = c.String(unicode: false),
    //                    taskcategoryid = c.String(unicode: false),
    //                    tasktypeid = c.String(unicode: false),
    //                    priority = c.String(unicode: false),
    //                    repeattypeid = c.String(unicode: false),
    //                    taskstatusid = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.taskid);
            
    //        CreateTable(
    //            "dbo.Trash_TaskAttachment",
    //            c => new
    //                {
    //                    taskattachmentid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    taskid = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    folderid = c.String(unicode: false),
    //                    url = c.String(unicode: false),
    //                    type = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.taskattachmentid)
    //            .ForeignKey("dbo.Trash_Task", t => t.taskid)
    //            .Index(t => t.taskid);
            
    //        CreateTable(
    //            "dbo.Trash_TaskCategory",
    //            c => new
    //                {
    //                    taskcategoryid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    description = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.taskcategoryid);
            
    //        CreateTable(
    //            "dbo.Trash_TaskComment",
    //            c => new
    //                {
    //                    taskcommentid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    taskid = c.String(unicode: false),
    //                    note = c.String(unicode: false),
    //                    userid = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.taskcommentid);
            
    //        CreateTable(
    //            "dbo.Trash_TaskLink",
    //            c => new
    //                {
    //                    tasklinkid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    taskid = c.String(unicode: false),
    //                    linktype = c.String(unicode: false),
    //                    linkid = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.tasklinkid);
            
    //        CreateTable(
    //            "dbo.Trash_TaskStatus",
    //            c => new
    //                {
    //                    taskstatusid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.taskstatusid);
            
    //        CreateTable(
    //            "dbo.Trash_TaskType",
    //            c => new
    //                {
    //                    tasktypeid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.tasktypeid);
            
    //        CreateTable(
    //            "dbo.Trash_Team",
    //            c => new
    //                {
    //                    teamid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.teamid);
            
    //        CreateTable(
    //            "dbo.Trash_TeamMember",
    //            c => new
    //                {
    //                    teammemberid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    teamid = c.String(unicode: false),
    //                    userid = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.teammemberid);
            
    //        CreateTable(
    //            "dbo.Trash_User",
    //            c => new
    //                {
    //                    userid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    surname = c.String(unicode: false),
    //                    email = c.String(unicode: false),
    //                    phone = c.String(unicode: false),
    //                    userroleid = c.String(unicode: false),
    //                    status = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.userid);
            
    //        CreateTable(
    //            "dbo.Trash_UserRole",
    //            c => new
    //                {
    //                    userroleid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.userroleid);
            
    //        CreateTable(
    //            "dbo.Trash_UserRolePermissionGroup",
    //            c => new
    //                {
    //                    userpermissiongroupid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    description = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.userpermissiongroupid);
            
    //        CreateTable(
    //            "dbo.Trash_Visibility",
    //            c => new
    //                {
    //                    visibilityid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    category = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.visibilityid);
            
    //        CreateTable(
    //            "dbo.Trash_WorkFlow",
    //            c => new
    //                {
    //                    workflowid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    workflowtriggerid = c.String(unicode: false),
    //                    workflowactionid = c.String(unicode: false),
    //                    workflowdata = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.workflowid);
            
    //        CreateTable(
    //            "dbo.Trash_WorkFlowAction",
    //            c => new
    //                {
    //                    workflowactionid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.workflowactionid);
            
    //        CreateTable(
    //            "dbo.Trash_WorkFlowData",
    //            c => new
    //                {
    //                    workflowdataid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    workflowid = c.String(unicode: false),
    //                    key = c.String(unicode: false),
    //                    value = c.String(unicode: false),
    //                    parentid = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.workflowdataid);
            
    //        CreateTable(
    //            "dbo.Trash_WorkFlowTrigger",
    //            c => new
    //                {
    //                    workflowtriggerid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.workflowtriggerid);
            
    //        CreateTable(
    //            "dbo.Trash_WorkTitle",
    //            c => new
    //                {
    //                    worktitleid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.worktitleid);
            
    //        CreateTable(
    //            "dbo.User",
    //            c => new
    //                {
    //                    userid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    surname = c.String(unicode: false),
    //                    email = c.String(unicode: false),
    //                    phone = c.String(unicode: false),
    //                    userroleid = c.String(unicode: false),
    //                    status = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.userid);
            
    //        CreateTable(
    //            "dbo.UserRole",
    //            c => new
    //                {
    //                    userroleid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.userroleid);
            
    //        CreateTable(
    //            "dbo.UserRolePermissionGroup",
    //            c => new
    //                {
    //                    userpermissiongroupid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    description = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.userpermissiongroupid);
            
    //        CreateTable(
    //            "dbo.AspNetUser",
    //            c => new
    //                {
    //                    Id = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    UserId = c.String(unicode: false),
    //                    Email = c.String(maxLength: 256, storeType: "nvarchar"),
    //                    EmailConfirmed = c.Boolean(nullable: false),
    //                    PasswordHash = c.String(unicode: false),
    //                    SecurityStamp = c.String(unicode: false),
    //                    PhoneNumber = c.String(unicode: false),
    //                    PhoneNumberConfirmed = c.Boolean(nullable: false),
    //                    TwoFactorEnabled = c.Boolean(nullable: false),
    //                    LockoutEndDateUtc = c.DateTime(precision: 0),
    //                    LockoutEnabled = c.Boolean(nullable: false),
    //                    AccessFailedCount = c.Int(nullable: false),
    //                    UserName = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                })
    //            .PrimaryKey(t => t.Id)
    //            .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
    //        CreateTable(
    //            "dbo.AspNetUserClaims",
    //            c => new
    //                {
    //                    Id = c.Int(nullable: false, identity: true),
    //                    UserId = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    ClaimType = c.String(unicode: false),
    //                    ClaimValue = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.Id)
    //            .ForeignKey("dbo.AspNetUser", t => t.UserId, cascadeDelete: true)
    //            .Index(t => t.UserId);
            
    //        CreateTable(
    //            "dbo.AspNetUserLogins",
    //            c => new
    //                {
    //                    LoginProvider = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    ProviderKey = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    UserId = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                })
    //            .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
    //            .ForeignKey("dbo.AspNetUser", t => t.UserId, cascadeDelete: true)
    //            .Index(t => t.UserId);
            
    //        CreateTable(
    //            "dbo.Visibility",
    //            c => new
    //                {
    //                    visibilityid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    category = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.visibilityid);
            
    //        CreateTable(
    //            "dbo.WorkFlow",
    //            c => new
    //                {
    //                    workflowid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    workflowtriggerid = c.String(unicode: false),
    //                    workflowactionid = c.String(unicode: false),
    //                    workflowdata = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.workflowid);
            
    //        CreateTable(
    //            "dbo.WorkFlowAction",
    //            c => new
    //                {
    //                    workflowactionid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.workflowactionid);
            
    //        CreateTable(
    //            "dbo.WorkFlowData",
    //            c => new
    //                {
    //                    workflowdataid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    workflowid = c.String(unicode: false),
    //                    key = c.String(unicode: false),
    //                    value = c.String(unicode: false),
    //                    parentid = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.workflowdataid);
            
    //        CreateTable(
    //            "dbo.WorkFlowTrigger",
    //            c => new
    //                {
    //                    workflowtriggerid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                    editor = c.String(maxLength: 128, storeType: "nvarchar"),
    //                    creator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    datecreated = c.DateTime(precision: 0),
    //                    datemodified = c.DateTime(precision: 0),
    //                    rowversion = c.Int(nullable: false),
    //                })
    //            .PrimaryKey(t => t.workflowtriggerid);
            
    //        CreateTable(
    //            "dbo.WorkTitle",
    //            c => new
    //                {
    //                    worktitleid = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
    //                    name = c.String(unicode: false),
    //                })
    //            .PrimaryKey(t => t.worktitleid);
            
    //    }
        
    //    public override void Down()
    //    {
    //        DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUser");
    //        DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUser");
    //        DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUser");
    //        DropForeignKey("dbo.Trash_TaskAttachment", "taskid", "dbo.Trash_Task");
    //        DropForeignKey("dbo.TaskAttachment", "taskid", "dbo.Task");
    //        DropForeignKey("dbo.Project", "Tag_tagid", "dbo.Tag");
    //        DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
    //        DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
    //        DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
    //        DropIndex("dbo.AspNetUser", "UserNameIndex");
    //        DropIndex("dbo.Trash_TaskAttachment", new[] { "taskid" });
    //        DropIndex("dbo.TaskAttachment", new[] { "taskid" });
    //        DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
    //        DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
    //        DropIndex("dbo.AspNetRoles", "RoleNameIndex");
    //        DropIndex("dbo.Project", new[] { "Tag_tagid" });
    //        DropTable("dbo.WorkTitle");
    //        DropTable("dbo.WorkFlowTrigger");
    //        DropTable("dbo.WorkFlowData");
    //        DropTable("dbo.WorkFlowAction");
    //        DropTable("dbo.WorkFlow");
    //        DropTable("dbo.Visibility");
    //        DropTable("dbo.AspNetUserLogins");
    //        DropTable("dbo.AspNetUserClaims");
    //        DropTable("dbo.AspNetUser");
    //        DropTable("dbo.UserRolePermissionGroup");
    //        DropTable("dbo.UserRole");
    //        DropTable("dbo.User");
    //        DropTable("dbo.Trash_WorkTitle");
    //        DropTable("dbo.Trash_WorkFlowTrigger");
    //        DropTable("dbo.Trash_WorkFlowData");
    //        DropTable("dbo.Trash_WorkFlowAction");
    //        DropTable("dbo.Trash_WorkFlow");
    //        DropTable("dbo.Trash_Visibility");
    //        DropTable("dbo.Trash_UserRolePermissionGroup");
    //        DropTable("dbo.Trash_UserRole");
    //        DropTable("dbo.Trash_User");
    //        DropTable("dbo.Trash_TeamMember");
    //        DropTable("dbo.Trash_Team");
    //        DropTable("dbo.Trash_TaskType");
    //        DropTable("dbo.Trash_TaskStatus");
    //        DropTable("dbo.Trash_TaskLink");
    //        DropTable("dbo.Trash_TaskComment");
    //        DropTable("dbo.Trash_TaskCategory");
    //        DropTable("dbo.Trash_TaskAttachment");
    //        DropTable("dbo.Trash_Task");
    //        DropTable("dbo.Trash_SocialMedia");
    //        DropTable("dbo.Trash_Sector");
    //        DropTable("dbo.Trash_RolePermission");
    //        DropTable("dbo.Trash_RoleAction");
    //        DropTable("dbo.Trash_Reminder");
    //        DropTable("dbo.Trash_Province");
    //        DropTable("dbo.Trash_ProjectTag");
    //        DropTable("dbo.Trash_ProjectCategory");
    //        DropTable("dbo.Trash_Project");
    //        DropTable("dbo.Trash_ProductTag");
    //        DropTable("dbo.Trash_ProductCategory");
    //        DropTable("dbo.Trash_Product");
    //        DropTable("dbo.Trash_PotentialTag");
    //        DropTable("dbo.Trash_PotentialState");
    //        DropTable("dbo.Trash_PotentialSource");
    //        DropTable("dbo.Trash_Potential");
    //        DropTable("dbo.Trash_Phone");
    //        DropTable("dbo.Trash_LinkType");
    //        DropTable("dbo.Trash_LinkCategory");
    //        DropTable("dbo.Trash_Link");
    //        DropTable("dbo.Trash_Library");
    //        DropTable("dbo.Trash_IndividualTag");
    //        DropTable("dbo.Trash_IndividualAccess");
    //        DropTable("dbo.Trash_Individual");
    //        DropTable("dbo.Trash_District");
    //        DropTable("dbo.Trash_Department");
    //        DropTable("dbo.Trash_Country");
    //        DropTable("dbo.Trash_ContactStatus");
    //        DropTable("dbo.Trash_ContactSource");
    //        DropTable("dbo.Trash_CompanyTag");
    //        DropTable("dbo.Trash_Company");
    //        DropTable("dbo.Trash_Comment");
    //        DropTable("dbo.Trash_Address");
    //        DropTable("dbo.TeamMember");
    //        DropTable("dbo.Team");
    //        DropTable("dbo.TaskType");
    //        DropTable("dbo.TaskStatus");
    //        DropTable("dbo.TaskLink");
    //        DropTable("dbo.TaskComment");
    //        DropTable("dbo.TaskCategory");
    //        DropTable("dbo.TaskAttachment");
    //        DropTable("dbo.Task");
    //        DropTable("dbo.Tag");
    //        DropTable("dbo.TableReplace");
    //        DropTable("dbo.SocialMedia");
    //        DropTable("dbo.Sector");
    //        DropTable("dbo.AspNetUserRoles");
    //        DropTable("dbo.AspNetRoles");
    //        DropTable("dbo.RolePermission");
    //        DropTable("dbo.RoleAction");
    //        DropTable("dbo.Reminder");
    //        DropTable("dbo.RefreshToken");
    //        DropTable("dbo.Province");
    //        DropTable("dbo.ProjectTag");
    //        DropTable("dbo.ProjectCategory");
    //        DropTable("dbo.Project");
    //        DropTable("dbo.ProductTag");
    //        DropTable("dbo.ProductCategory");
    //        DropTable("dbo.Product");
    //        DropTable("dbo.PotentialTag");
    //        DropTable("dbo.PotentialState");
    //        DropTable("dbo.PotentialSource");
    //        DropTable("dbo.Potential");
    //        DropTable("dbo.Phone");
    //        DropTable("dbo.LinkType");
    //        DropTable("dbo.LinkCategory");
    //        DropTable("dbo.Link");
    //        DropTable("dbo.Library");
    //        DropTable("dbo.IndividualTag");
    //        DropTable("dbo.IndividualAccess");
    //        DropTable("dbo.Individual");
    //        DropTable("dbo.District");
    //        DropTable("dbo.Department");
    //        DropTable("dbo.Country");
    //        DropTable("dbo.ContactStatus");
    //        DropTable("dbo.ContactSource");
    //        DropTable("dbo.CompanyTag");
    //        DropTable("dbo.Company");
    //        DropTable("dbo.Comment");
    //        DropTable("dbo.Client");
    //        DropTable("dbo.Address");
    //    }
    //}
}
