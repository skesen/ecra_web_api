﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using MongoDB.Driver;
using NtC.Netuce.Core.Concrete.EntityFramework;
using NtC.Netuce.Core.Entities;

namespace NtC.Netuce.Core.DataAccess.EntityFramework
{
    public abstract class EfEntityRepositoryBase : IEntityRepository
    {
        private bool disposed = false;
        //private NtCContext db;
        private NtCContext _context = new NtCContext();
        //private MongoClient _mcontext= new MongoClient();
        //private void CreateContext(string subName)
        //{
        //    db = new NtCContext(subName + "_db");
        //}
        //protected virtual void SendClientSub(string subName)
        //{
        //    CreateContext(subName);
        //}
        protected NtCContext db
        {
            get { return _context; }
            set { _context = value; }
        }
        //protected MongoClient mdb
        //{
        //    get { return _mcontext; }
        //    set { _mcontext = value; }
        //}
        public T Get<T>(Expression<Func<T, bool>> filter = null) where T : class
        {
            return db.Set<T>().SingleOrDefault(filter);
        }

        public List<IEnumerable<T>> GetPaging<T>(IList<T> source, int currentPage, int pageSize) where T : class
        {
           return (List<IEnumerable<T>>) source.Skip(currentPage * pageSize).Take(pageSize);
        }

        public IEnumerable<T> GetList<T>(Expression<Func<T, bool>> filter = null) where T : class
        {
            return filter == null
                ? db.Set<T>()
                : db.Set<T>().Where(filter);

        }
        
        public int RowCount(string tableName, string filter)
        {
            if (tableName == null)
            {
                throw new ArgumentNullException("tableName", "Hata Kodu:R101");
            }
            return db.USP_GetRowCount(tableName, filter);
        }
        public virtual T FindById<T>(long id) where T : class
        {
            T model = db.Set<T>().Find(id);
            if (model == null)
            {
                throw new ArgumentNullException("model", "Hata Kodu:F100");
            }
            return model;
        }
        //Hata Kodu:C101
        public virtual void Create<T>(T model) where T : class
        {
            if (model == null)
            {
                throw new ArgumentNullException("model", "Hata Kodu:C101");
            }

            db.Set<T>().Add(model);
        }
        //Hata Kodu:C102
        public virtual void Update<T>(T model) where T : class
        {
            if (model == null)
            {
                throw new ArgumentNullException("model", "Hata Kodu:C102");
            }

            var checker1 = db.Entry(model).GetDatabaseValues().GetValue<int>("RowVersion");
            var checker2 = ((IRowVersion)model).rowversion;
            if (checker1 != checker2)
            {
                throw new DbUpdateConcurrencyException("Başka bir kullanıcı güncelleme yapmakta,lütfen daha sonra tekrar deneyin");
            }
            db.Entry(model).State = EntityState.Modified;
            ((IBaseEntity)model).datemodified = DateTime.Now;
            ((IBaseEntity)model).rowversion += 1;
        }
        //Hata Kodu:C103
        public virtual void Delete<T>(int id) where T : class
        {
            try
            {
                var temp = db.Set<T>().Find(id);
                int checker1 = db.Entry(temp).GetDatabaseValues().GetValue<int>("RowVersion");
                int checker2 = (temp as IRowVersion).rowversion;
                if (db.Entry(temp).GetDatabaseValues().GetValue<int>("RowVersion") != (temp as IRowVersion).rowversion)
                {
                    throw new DbUpdateConcurrencyException("Başka bir kullanıcı güncelleme yapmakta,lütfen daha sonra tekrar deneyin");
                }
                if (temp == null) throw new ArgumentNullException("model", "InnerExceptionCode:C103");
                db.Set<T>().Remove(temp);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual void Save()
        {
            try
            {
                int foo = db.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                var msg = string.Empty;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        msg += Environment.NewLine + string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                //throw new Exception(msg, dbEx);
                throw new Exception("Üzgünüz...Bir hata oluştu,lütfen bilgilerinizi tekrar kontrol edin.", dbEx);
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new DbUpdateConcurrencyException("Başka bir kullanıcı güncelleme yapmakta,lütfen daha sonra tekrar deneyin", ex.InnerException);
            }
            catch (ObjectDisposedException ex)
            {
                throw new ObjectDisposedException(ex.Message, ex.InnerException);
            }
            catch (NotSupportedException ex)
            {
                throw new NotSupportedException(ex.Message, ex.InnerException);
            }
            catch (InvalidOperationException ex)
            {
                throw new InvalidOperationException(ex.Message, ex.InnerException);
            }
            catch (DbUpdateException ex)
            {
                throw new DbUpdateException(ex.Message, ex.InnerException);
            }
        }

        

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
                if (disposing)
                    db.Dispose();
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}
