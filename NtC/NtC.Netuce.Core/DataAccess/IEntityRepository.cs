﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace NtC.Netuce.Core.DataAccess
{
    public interface IEntityRepository : IDisposable
    {
        int RowCount(string tableName, string filter = null);

        T FindById<T>(long id) where T : class;

        T Get<T>(Expression<Func<T, bool>> filter = null) where T : class;

        IEnumerable<T> GetList<T>(Expression<Func<T, bool>> filter = null) where T : class;

        List<IEnumerable<T>> GetPaging<T>(IList<T> source, int startIndex, int length) where T : class;

        void Create<T>(T model) where T : class;

        void Update<T>(T model) where T : class;

        void Delete<T>(int id) where T : class;

        void Save();

    }
}
