﻿using System;
using System.Collections.Generic;
using System.IO;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using NtC.Netuce.Core.CrossCuttingConcern.EmailSender;

namespace NtC.Netuce.WebApi.Extensions
{
    public class EmailExtensions
    {
        #region Message Instances
        /// <summary>
        /// Retrieve a Message by ID.(message id'ye göre maili çekiyor)
        /// </summary>
        /// <param name="service">Gmail API service instance.</param>
        /// <param name="userId">User's email address. The special value "me"
        /// can be used to indicate the authenticated user.</param>
        /// <param name="messageId">ID of Message to retrieve.</param>
        public static Message GetMessage(GmailService service, string userId, string messageId)
        {
            try
            {
                var query = service.Users.Messages.Get(userId, messageId).Execute();
                return query;
            }
            catch (Exception e)
            {
                var email = new ErrorEmail("Gsuite", "GetMessage : " + e.Message);
                email.Send();
            }

            return null;
        }


        /// <summary>
        /// Insert an email Message into the user's mailbox.
        /// </summary>
        /// <param name="service">Gmail API service instance.</param>
        /// <param name="userId">User's email address. The special value "me"
        /// can be used to indicate the authenticated user.</param>
        /// <param name="email">Email to be inserted.</param>
        public static Message InsertMessage(GmailService service, string userId, Message email)
        {
            try
            {
                return service.Users.Messages.Insert(email, userId).Execute();
            }
            catch (Exception e)
            {
                var errormail = new ErrorEmail("Gsuite", "InsertMessage : " + e.Message);
                errormail.Send();
            }

            return null;
        }

        public static List<Message> ListMessages(GmailService service, string userId, string query)
        {
            List<Message> result = new List<Message>();
            UsersResource.MessagesResource.ListRequest request = service.Users.Messages.List(userId);
            request.Q = query;

            do
            {
                try
                {
                    ListMessagesResponse response = request.Execute();
                    result.AddRange(response.Messages);
                    request.PageToken = response.NextPageToken;
                }
                catch (Exception e)
                {
                    var errormail = new ErrorEmail("Gsuite", "ListMessages : " + e.Message);
                    errormail.Send();
                }
            } while (!String.IsNullOrEmpty(request.PageToken));

            return result;
        }
        #endregion

        #region Message.Attachments Instances
        /// <summary>
        /// Get and store attachment from Message with given ID.
        /// </summary>
        /// <param name="service">Gmail API service instance.</param>
        /// <param name="userId">User's email address. The special value "me"
        /// can be used to indicate the authenticated user.</param>
        /// <param name="messageId">ID of Message containing attachment.</param>
        /// <param name="outputDir">Directory used to store attachments.</param>
        public static void GetAttachments(GmailService service, String userId, String messageId, String outputDir)
        {
            try
            {
                Message message = service.Users.Messages.Get(userId, messageId).Execute();
                IList<MessagePart> parts = message.Payload.Parts;
                foreach (MessagePart part in parts)
                {
                    if (!String.IsNullOrEmpty(part.Filename))
                    {
                        String attId = part.Body.AttachmentId;
                        MessagePartBody attachPart = service.Users.Messages.Attachments.Get(userId, messageId, attId).Execute();

                        // Converting from RFC 4648 base64 to base64url encoding
                        // see http://en.wikipedia.org/wiki/Base64#Implementations_and_history
                        String attachData = attachPart.Data.Replace('-', '+');
                        attachData = attachData.Replace('_', '/');

                        byte[] data = Convert.FromBase64String(attachData);
                        File.WriteAllBytes(Path.Combine(outputDir, part.Filename), data);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
            }
        }
        #endregion

        #region Label Instances
        /// <summary>
        /// List the labels in the user's mailbox.(kişinin mailini çekiyor.)
        /// </summary>
        /// <param name="service">Gmail API service instance.</param>
        /// <param name="userId">User's email address. The special value "me"
        /// can be used to indicate the authenticated user.</param>
        public static void ListLabels(GmailService service, string userId)
        {
            try
            {
                ListLabelsResponse response = service.Users.Labels.List(userId).Execute();
                foreach (Label label in response.Labels)
                {
                    Console.WriteLine(label.Id + " - " + label.Name);
                }
            }
            catch (Exception e)
            {
                var errormail = new ErrorEmail("Gsuite", "ListLabels : " + e.Message);
                errormail.Send();
            }
        }

        /// <summary>
        /// Update an existing Label.
        /// </summary>
        /// <param name="service">Gmail API service instance.</param>
        /// <param name="userId">User's email address. The special value "me"
        /// can be used to indicate the authenticated user.</param>
        /// <param name="labelId">Id of the label to be updated.</param>
        /// <param name="newLabelName">Name of the new Label.</param>
        /// <param name="showInMessageList">Show or hide label in message.</param>
        /// <param name="showInLabelList">Show or hide label in label list.</param>
        public static Label UpdateLabel(GmailService service, string userId, string labelId,
            string newLabelName, bool showInMessageList, bool showInLabelList)
        {
            string messageListVisibility = showInMessageList ? "show" : "hide";
            string labelListVisibility = showInLabelList ? "labelShow" : "labelHide";

            Label label = new Label
            {
                Name = newLabelName,
                MessageListVisibility = messageListVisibility,
                LabelListVisibility = labelListVisibility
            };

            try
            {
                return service.Users.Labels.Update(label, userId, labelId).Execute();
            }
            catch (Exception e)
            {
                var errormail = new ErrorEmail("Gsuite", "UpdateLabel : " + e.Message);
                errormail.Send();
            }
            return null;
        }

        public static Label GetLabel(GmailService service, string userId, string labelId)
        {
            try
            {
                Label label = service.Users.Labels.Get(userId, labelId).Execute();
                return label;
            }
            catch (Exception e)
            {
                var email = new ErrorEmail("Gsuite", "GetLabel : " + e.Message);
                email.Send();
            }
            return null;
        }
        #endregion

        #region Thread Instance
        /// <summary>
        /// Get a Thread with given ID.
        /// </summary>
        /// <param name="service">Gmail API service instance.</param>
        /// <param name="userId">User's email address. The special value "me"
        /// can be used to indicate the authenticated user.</param>
        /// <param name="threadId">ID of Thread to retrieve.</param>
        public static Thread GetThread(GmailService service, string userId, string threadId)
        {
            try
            {
                return service.Users.Threads.Get(userId, threadId).Execute();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
            }

            return null;
        }

        /// <summary>
        /// List all message threads of the user's mailbox.
        /// </summary>
        /// <param name="service">Gmail API service instance.</param>
        /// <param name="userId">User's email address. The special value "me"
        /// can be used to indicate the authenticated user.</param>
        public static Tuple<List<Thread>,string> ListThreads(GmailService service, string userId,int maxresult)
        {
            var result = new List<Thread>();
            var request = service.Users.Threads.List(userId);
            
            do
            {
                try
                {
                    ListThreadsResponse response = request.Execute();
                    result.AddRange(response.Threads);
                    
                    request.PageToken = response.NextPageToken;
                    request.MaxResults = request.MaxResults != null ? request.MaxResults + 1 : 1;
                }
                catch (Exception e)
                {
                    Console.WriteLine("An error occurred: " + e.Message);
                }
            } while (!string.IsNullOrEmpty(request.PageToken) && request.MaxResults != maxresult);

            return new Tuple<List<Thread>, string>(result,request.PageToken);
        }

        /// <summary>
        /// Modify the Labels applied to a Thread.
        /// </summary>
        /// <param name="service">Gmail API service instance.</param>
        /// <param name="userId">User's email address. The special value "me"
        /// can be used to indicate the authenticated user.</param>
        /// <param name="threadId">ID of the Thread to modify.</param>
        /// <param name="labelsToAdd">List of label ids to add.</param>
        /// <param name="labelsToRemove">List of label ids to remove.</param>
        public static Thread ModifyThread(GmailService service, string userId, string threadId, List<string> labelsToAdd, List<string> labelsToRemove)
        {
            var mods = new ModifyThreadRequest
            {
                AddLabelIds = labelsToAdd,
                RemoveLabelIds = labelsToRemove
            };

            try
            {
                return service.Users.Threads.Modify(mods, userId, threadId).Execute();
            }
            catch (Exception e)
            {
                var errormail = new ErrorEmail("Gsuite", "ModifyThread : " + e.Message);
                errormail.Send();
            }

            return null;
        }

        #endregion

        #region History Instances
        /// <summary>
        /// List History of all changes to the user's mailbox.
        /// </summary>
        /// <param name="service">Gmail API service instance.</param>
        /// <param name="userId">User's email address. The special value "me"
        /// can be used to indicate the authenticated user.</param>
        /// <param name="startHistoryId">Only return changes at or after startHistoryId.</param>
        public static List<History> ListHistory(GmailService service, string userId, ulong startHistoryId)
        {
            List<History> result = new List<History>();
            UsersResource.HistoryResource.ListRequest request = service.Users.History.List(userId);
            request.StartHistoryId = startHistoryId;

            do
            {
                try
                {
                    ListHistoryResponse response = request.Execute();
                    if (response.History != null)
                    {
                        result.AddRange(response.History);
                    }
                    request.PageToken = response.NextPageToken;
                }
                catch (Exception e)
                {
                    Console.WriteLine("An error occurred: " + e.Message);
                }
            } while (!String.IsNullOrEmpty(request.PageToken));

            return result;
        }
        #endregion
    }
}