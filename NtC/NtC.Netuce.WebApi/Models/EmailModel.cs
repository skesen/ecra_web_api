﻿namespace NtC.Netuce.WebApi.Models
{
    public class EmailModel
    {
        public string filter { get; set; }
        public string where { get; set; }
        public string field { get; set; }
        public string method { get; set; }
        public string emailtype { get; set; }
        public string sort { get; set; }
    }

    public class ResponseEmailModel
    {
        public string subject { get; set; }
        public string cc { get; set; }
        public string from { get; set; }
        public string to { get; set; }
        public string xgoogleoriginaldate { get; set; }
        public string snippet { get; set; }
        public string threadid { get; set; }
        public string etag { get; set; }
        public string attachementid { get; set; }
        public string messageid { get; set; }
        public string internaldate { get; set; }
        public string historyid { get; set; }
        public string[] labelids { get; set; }

    }
}