﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Handlers;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using NtC.Netuce.Business.Abstract;
using NtC.Netuce.Core.CrossCuttingConcern.EmailSender;
using NtC.Netuce.WebApi.Filter;
using NtC.Netuce.WebApi.MessageHandlers;

namespace NtC.Netuce.WebApi.Controllers
{
    [EnableCors("*", "*", "*")]
    [InvalidModelStateFilter]
    //[Authorize]
    public class NtCController : ApiController
    {
        private INtCService _repo;

        public NtCController(INtCService _repo)
        {
            this._repo = _repo;
        }


        [Authorize]
        [Route("api")]
        [NoPageMessageHandler]
        public HttpResponseMessage Api()
        {
            return Request.CreateResponse(HttpStatusCode.Forbidden, "-----");
        }

        [HttpPost]
        [Route("api/addfile")]
        public HttpResponseMessage AddFile()
        {
            var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);

            try
            {
                ProgressMessageHandler progress = new ProgressMessageHandler();
                progress.HttpSendProgress += HttpSendProgress;
                var httpRequest = HttpContext.Current.Request;
                if (httpRequest.Files.Count < 1)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }

                foreach (string file in httpRequest.Files)
                {

                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null)
                    {
                        var extension = Path.GetExtension(postedFile.FileName);
                        var filename = Regex.Split(postedFile.FileName, @"\.")[0];
                        var mappath = "~/Drive/uploads/";
                        var directory = new DirectoryInfo(HttpContext.Current.Server.MapPath(mappath));
                        var files = directory.GetFiles().ToList();
                        foreach (var fileInfo in files)
                        {
                            if (fileInfo != null && fileInfo.Name == postedFile.FileName)
                            {
                                filename = filename + "-" + DateTime.Now.Day + DateTime.Now.Millisecond;
                                break;
                            }
                        }


                        var filePath = HttpContext.Current.Server.MapPath(mappath + filename + extension);
                        postedFile.SaveAs(filePath);

                        var model = "{name=" + filename + extension + ",path=" + mappath + filename + extension + ",size=" + postedFile.ContentLength + ",extension=" + extension + "}";
                        var keyval = new[]
                        {
                            new KeyValuePair<string, string>("method","create")
                        };

                        var query = _repo.Post(baseUrl,"library", keyval, JsonConvert.SerializeObject(model));
                        progress.HttpSendProgress -= HttpSendProgress;
                        return Request.CreateResponse(HttpStatusCode.OK, query);

                    }
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }


            }
            catch (Exception ex)
            {
                var email = new ErrorEmail(baseUrl,"Kayit olusturulurken problem olustu. " + ex);
                email.Send();
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { isValid = false, message = "Beklenmedik bir hata oluştu." });
            }
            return null;
        }

        private static void HttpSendProgress(object sender, HttpProgressEventArgs e)
        {
            HttpRequestMessage request = sender as HttpRequestMessage;
            var id = request.RequestUri.Query[0];
            var perc = e.ProgressPercentage;
            var b = e.TotalBytes;
            var bt = e.BytesTransferred;
        }
        
        [HttpPost]
        [Route("api/deleteimage")]
        public async Task<object> Delete(string fileName)
        {
            try
            {
                var mappath = "~/Drive/uploads/";
                var filePath = Directory.GetFiles(mappath, fileName)
                                .FirstOrDefault();

                await Task.Factory.StartNew(() =>
                {
                    File.Delete(filePath);
                });

                return new { Successful = true, Message = fileName + "deleted successfully" };
            }
            catch (Exception ex)
            {
                return new { Successful = false, Message = "error deleting fileName " + ex.GetBaseException().Message };
            }
        }
        
        //[Authorize]
        [Route("api/count/{table}")]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Count(string table)
        {
            if (string.IsNullOrEmpty(table))
            {
                return Request.CreateResponse<int>(HttpStatusCode.BadRequest, 0);
            }
            var allUrlKeyValues = ControllerContext.Request.GetQueryNameValuePairs();

            var keyValuePairs = allUrlKeyValues as KeyValuePair<string, string>[] ?? allUrlKeyValues.ToArray();

            return Request.CreateResponse<int>(HttpStatusCode.OK, _repo.Count(table, keyValuePairs)); 
        }

        [Authorize]
        [Route("api/{table}")]
        public async Task<IEnumerable<object>> Get(string table)
        {
            var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);
            if (string.IsNullOrEmpty(table))
            {
                return new List<object> { true };
            }
            var allUrlKeyValues = ControllerContext.Request.GetQueryNameValuePairs();

            var keyValuePairs = allUrlKeyValues as KeyValuePair<string, string>[] ?? allUrlKeyValues.ToArray();
            
            return await Task.Run(() => _repo.Get(baseUrl, table, keyValuePairs));
        }
        
        //[Authorize]
        [Route("api/{table}")]
        public HttpResponseMessage Post([FromBody] object model)
        {
            var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);
            //if (model == null) return Request.CreateResponse(HttpStatusCode.BadRequest);
            var allUrlKeyValues = ControllerContext.Request.GetQueryNameValuePairs();
            var keyValuePairs = allUrlKeyValues as KeyValuePair<string, string>[] ?? allUrlKeyValues.ToArray();

            var query = _repo.Post(baseUrl,ControllerContext.Request.RequestUri.Segments[2], keyValuePairs, JsonConvert.SerializeObject(model));

            var isfield = query.GetType().GetProperty("answer");
            if (isfield != null && (string)isfield.GetValue(query, null) == "0")
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, query);
            }
            if (isfield != null && (string)isfield.GetValue(query, null) == "1")
            {
            return Request.CreateResponse<object>(HttpStatusCode.OK, null);

            }
            return Request.CreateResponse<object>(HttpStatusCode.OK, query);
        }

        
        //ALTER TABLE ntc_ecra.tasks CONVERT TO CHARACTER SET utf8
    }
}
