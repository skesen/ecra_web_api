﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using NtC.Netuce.Business.Abstract;
using NtC.Netuce.Core.CrossCuttingConcern.EmailSender;
using NtC.Netuce.Core.Entities;
using NtC.Netuce.Core.Utilities.EnumUtilities;
using NtC.Netuce.WebApi.Filter;
using NtC.Netuce.WebApi.OAuth;

namespace NtC.Netuce.WebApi.Controllers
{
    [EnableCors("*", "*", "*")]
    [InvalidModelStateFilter]
    public class UserController : ApiController
    {
        private AuthRepository _repo = null;

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IUserService _repouser;
        public UserController(IUserService _repouser)
        {
            this._repouser = _repouser;
            _repo = new AuthRepository();
        }

        [Route("user/CheckUserEmail")]
        public HttpResponseMessage CheckUserEmail([FromBody] object model)
        {
            var data = JsonConvert.SerializeObject(model);
            var obje = data.Replace("{", "").Replace("}", "").Replace(":", "=");
            var gs = obje.Split(',').ToDictionary(c => c.Split('=')[0], c => Uri.UnescapeDataString(c.Split('=')[1]));
            var useremail = "";
            foreach (var k in gs)
            {
                if (k.Key.ToLower().Contains("e"))
                {
                    useremail = k.Value.Replace("\\", "").Replace("\"", "");
                }
            }
            if (!string.IsNullOrEmpty(useremail))
            {
                var user = _repo.FindByEmail(useremail);
                if (user != null && user.Email==null)
                {
                    return Request.CreateResponse<object>(HttpStatusCode.OK, new { status = true });
                }
                return Request.CreateResponse<object>(HttpStatusCode.OK, new { status = false });
            }
            return Request.CreateResponse<object>(HttpStatusCode.OK, new { status = false });
        }

        //[Authorize]
        [Route("user/PasswordResetRequest")]
        public HttpResponseMessage PasswordResetRequest([FromBody] object model)
        {
            var data = JsonConvert.SerializeObject(model);
            var obje = data.Replace("{", "").Replace("}", "").Replace(":", "=");
            var gs = obje.Split(',').ToDictionary(c => c.Split('=')[0], c => Uri.UnescapeDataString(c.Split('=')[1]));
            var email = "";
            var userid = "";

            foreach (var k in gs)
            {
                if (k.Key.ToLower().Contains("userid"))
                {
                    userid = k.Value.Replace("\\", "").Replace("\"", "");
                }
                if (k.Key.ToLower().Contains("email"))
                {
                    email = k.Value.Replace("\\", "").Replace("\"", "");
                }
            }
            var newuser = _repouser.GetUserByEmail('"' + email + '"');
            if (newuser != null && !string.IsNullOrEmpty(newuser.email))
            {
                var currentUser = _repo.FindByEmail(newuser.email);

                if (currentUser != null)
                {
                    string code = _repo.GeneratePasswordResetToken(currentUser.Id);
                    code = HttpUtility.UrlEncode(code);
                    var mail = new InvitationEmail(userid: currentUser.Id, username: email, email: email,
                        token: code);
                    mail.Send(Enums.EmailType.invitation);
                    return Request.CreateResponse<object>(HttpStatusCode.OK,
                        new { userid = currentUser.Id, code = code });
                }
                else
                {
                    var user = new ApplicationUser
                    {
                        UserName = email,
                        AccessFailedCount = 0,
                        UserId = newuser.userid,
                        LockoutEndDateUtc = DateTime.Now,
                        Email = email,
                        EmailConfirmed = false
                    };
                    var result = _repo.RegisterUser(user,
                        "NtC_" + Guid.NewGuid().ToString().Replace("-", "").Substring(7));
                    if (result.Succeeded)
                    {

                        user.Id = _repo.FindByEmail(email).Id;
                        string code = _repo.GeneratePasswordResetToken(user.Id);
                        var mail = new InvitationEmail(userid: user.Id, username: email, email: email, token: code);
                        code = HttpUtility.UrlEncode(code);
                        mail.Send();
                        return Request.CreateResponse<object>(HttpStatusCode.OK, new { userid = user.Id, code = code });
                    }
                    return Request.CreateResponse<object>(HttpStatusCode.InternalServerError, result.Errors);
                }
            }
            return Request.CreateResponse<object>(HttpStatusCode.InternalServerError, false);
        }




        [Route("user/PasswordResetTokenRequest")]
        public HttpResponseMessage PasswordResetTokenRequest([FromBody] object model)
        {
            var data = JsonConvert.SerializeObject(model);
            var obje = data.Replace("{", "").Replace("}", "").Replace(":", "=");
            var gs = obje.Split(',').ToDictionary(c => c.Split('=')[0], c => Uri.UnescapeDataString(c.Split('=')[1]));
            var email = "";
            var userid = "";

            foreach (var k in gs)
            {
                if (k.Key.ToLower().Contains("userid"))
                {
                    userid = k.Value.Replace("\\", "").Replace("\"", "");
                }
                if (k.Key.ToLower().Contains("email"))
                {
                    email = k.Value.Replace("\\", "").Replace("\"", "");
                }
            }


            var currentUser = _repo.FindByEmail(email);
            if (currentUser == null)
            {
                var user = new ApplicationUser
                {
                    UserName = email,
                    AccessFailedCount = 0,
                    UserId = userid,
                    LockoutEndDateUtc = DateTime.Now,
                    Email = email,
                    EmailConfirmed = false
                };
                var result = _repo.RegisterUser(user, "NtC_" + Guid.NewGuid().ToString().Replace("-", "").Substring(7));
                if (result.Succeeded)
                {

                    user.Id = _repo.FindByEmail(email).Id;
                    string code = _repo.GeneratePasswordResetToken(user.Id);
                    //var mail = new InvitationEmail(userid: user.Id, username: email, email: email, token: code);
                    //code = HttpUtility.UrlEncode(code);
                    //mail.Send();
                    return Request.CreateResponse<object>(HttpStatusCode.OK, new { userid = user.Id, code = code, username = email, email = email });
                }
                return Request.CreateResponse<object>(HttpStatusCode.InternalServerError, result.Errors);
            }
            else
            {
                string code = _repo.GeneratePasswordResetToken(currentUser.Id);
                code = HttpUtility.UrlEncode(code);
                //var mail = new InvitationEmail(userid: currentUser.Result.Id, username: email, email: email, token: code);
                //mail.Send(Enums.EmailType.invitation);
                return Request.CreateResponse<object>(HttpStatusCode.OK, new { userid = currentUser.Id, token = code, username = email, email = email });
            }
        }



        [Route("user/PasswordReset")]
        public HttpResponseMessage PasswordReset([FromBody] object model)
        {
            var data = JsonConvert.SerializeObject(model);
            var obje = data.Replace("{", "").Replace("}", "").Replace(":", "=");
            var gs = obje.Split(',').ToDictionary(c => c.Split('=')[0], c => Uri.UnescapeDataString(c.Split('=')[1]));
            var userid = "";
            var password = "";

            foreach (var k in gs)
            {
                if (k.Key.ToLower().Contains("u"))
                {
                    userid = k.Value.Replace("\\", "").Replace("\"", "");
                }
                if (k.Key.ToLower().Contains("p"))
                {
                    password = k.Value.Replace("\\", "").Replace("\"", "");
                }
            }


            var token = Request.Headers.Authorization.Parameter;

            token = HttpUtility.UrlDecode(token);

            var user = _repo.FindByIdAsync(userid);
            if (user != null)
            {
                var result = _repo.ResetPassword(userId: userid, token: token, newPassword: password);
                if (result)//mesajı ile birlikte dönecez.
                {
                    return Request.CreateResponse<object>(HttpStatusCode.OK, new { userid = userid, status = true });
                }
                else
                {
                    return Request.CreateResponse<object>(HttpStatusCode.NotFound, new { userid = userid, status = false });

                }
            }
            return Request.CreateResponse<object>(HttpStatusCode.Unauthorized, new { userid = userid, status = false });

        }





    }
}
