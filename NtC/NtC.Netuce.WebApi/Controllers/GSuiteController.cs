﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Gmail.v1;
using Google.Apis.Oauth2.v2;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using NtC.Netuce.Business.Abstract;
using NtC.Netuce.Core.CrossCuttingConcern.EmailSender;
using NtC.Netuce.Core.Utilities.ExtensionMethods;
using NtC.Netuce.WebApi.Extensions;
using NtC.Netuce.WebApi.Filter;
using NtC.Netuce.WebApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace NtC.Netuce.WebApi.Controllers
{
    [EnableCors("*", "*", "*")]
    [InvalidModelStateFilter]
    public class GSuiteController : ApiController
    {
        private INtCService _repo;

        public GSuiteController(INtCService _repo)
        {
            this._repo = _repo;
        }
        static string[] Scopes = { GmailService.Scope.MailGoogleCom };
        static string ApplicationName = "ecra_web_api";

        public UserCredential credential;
        [Route("AuthCallback")]
        [AcceptVerbs("GET", "POST")]

        public HttpResponseMessage AuthCallback()
        {
            return new HttpResponseMessage(HttpStatusCode.OK);
        }
        #region Mail Functions
        //[Authorize]
        [Route("mail/{type}")]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Mail(string type)
        {
            try
            {

                using (var stream =
               new FileStream(System.Web.Hosting.HostingEnvironment.MapPath("~/client_secret.json"), FileMode.Open, FileAccess.Read))
                {
                    string credPath = Environment.GetFolderPath(
                   Environment.SpecialFolder.CommonDocuments);
                    credPath = Path.Combine(credPath, ".credentials/gmail-dotnet-quickstart.json");

                    var secrets = new ClientSecrets
                    {
                        ClientId = "15059478814-jdscob56qllnh2kkjup28882kvd5ehso.apps.googleusercontent.com",
                        ClientSecret = "Spz6cUsUhnvOi5k_hyHq_ULL"
                    };
                    using (var cts = new CancellationTokenSource())
                    {
                        cts.CancelAfter(TimeSpan.FromMinutes(1));
                        try
                        {
                            credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                             secrets
                             //GoogleClientSecrets.Load(stream).Secrets
                            , Scopes, "user", CancellationToken.None,
                             new FileDataStore(credPath, true)
                            ).Result;
                        }
                        catch (Exception ex)
                        {
                            var email = new ErrorEmail("Email integration", ex.Message + "<br>" + ex.InnerException + "<br>" + new FileDataStore(credPath, true) + "<br>" + credPath);
                            email.Send();
                        }
                    }
                }

                // Create Gmail API service.
                var service = new GmailService(initializer: new BaseClientService.Initializer
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "ecra_web_api",

                });


                var allUrlKeyValues = ControllerContext.Request.GetQueryNameValuePairs();

                var keyValuePairs = allUrlKeyValues as KeyValuePair<string, string>[] ?? allUrlKeyValues.ToArray();

                var mailModel = SolveEmailValues(keyValuePairs);

                var wheretext = mailModel.where.ObjectClearText();

                var filtertext = mailModel.filter.ObjectClearText();

                if (wheretext == null) return new HttpResponseMessage(HttpStatusCode.BadRequest);


                var w = wheretext.Split(',').ToDictionary(c => c.Split('=')[0], c => Uri.UnescapeDataString(c.Split('=')[1]));

                var me = w.FirstOrDefault(x => x.Key == "userid").Value;

                switch (type)
                {
                    case "messages":
                        {
                            if (mailModel.@where != null && mailModel.@where.ContainsText("messageid"))
                            {
                                var query = EmailExtensions.GetMessage(service, me, w.FirstOrDefault(x => x.Key == "messageid").Value);

                                return Request.CreateResponse<object>(HttpStatusCode.OK, query);
                            }
                            else
                            {
                                var query = EmailExtensions.ListMessages(service, me, "");
                                return Request.CreateResponse<object>(HttpStatusCode.OK, query);

                            }

                        }
                    case "threads":
                        {
                            if (!string.IsNullOrEmpty(filtertext))
                            {
                                var groupfilter = GrouptoFilter(filtertext);

                                var query = EmailExtensions.ModifyThread(service, me,
                                    w.FirstOrDefault(x => x.Key == "threadid").Value, groupfilter.Item1, groupfilter.Item2);
                                return Request.CreateResponse<object>(HttpStatusCode.OK, query);
                            }
                            if (mailModel.@where != null && mailModel.@where.ContainsText("threadid"))
                            {
                                var query = EmailExtensions.GetThread(service, me, w.FirstOrDefault(x => x.Key == "threadid").Value);
                                return Request.CreateResponse<object>(HttpStatusCode.OK, query);
                            }
                            else
                            {
                                var query = EmailExtensions.ListThreads(service, me, 1);
                                return Request.CreateResponse<object>(HttpStatusCode.OK, query);

                            }

                        }

                    case "labels":
                        {
                            if (mailModel.@where != null && mailModel.@where.ContainsText("labelid"))
                            {
                                var query = EmailExtensions.GetLabel(service, me, w.FirstOrDefault(x => x.Key == "labelid").Value);
                                return Request.CreateResponse<object>(HttpStatusCode.OK, query);
                            }
                            else
                            {
                                //var query = EmailExtensions.ListLabels(service,me);
                            }
                            return null;

                        }
                    case "history":
                        {
                            var query = EmailExtensions.ListHistory(service, me, Convert.ToUInt64(w.FirstOrDefault(x => x.Key == "historyid").Value));
                            return Request.CreateResponse<object>(HttpStatusCode.OK, query);
                        }

                    case "attachments":
                        {
                            return null;

                        }

                }

                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {

                var email = new ErrorEmail("Email integration", ex.Message + "<br>" +
                    ex.InnerException + "<br>" +
                    ex.Data + "<br>" +
                    ex.HResult + "<br>" +
                    ex.Source + "<br>" + ex.StackTrace);
                //email.Send();
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        private Tuple<List<string>, List<string>> GrouptoFilter(string filtertext)
        {
            List<string> removeLabelIds = null;
            List<string> addLabelIds = null;
            if (filtertext == "UNREAD")
                removeLabelIds.Add("UNREAD");
            if (filtertext == "INBOX")
                removeLabelIds.Add("INBOX");
            if (filtertext == "TRASH")
                addLabelIds.Add("TRASH");
            if (filtertext == "STARRED")
                addLabelIds.Add("STARRED");

            return new Tuple<List<string>, List<string>>(addLabelIds, removeLabelIds);
        }

        #endregion


        #region SolveQueryString
        private EmailModel SolveEmailValues(KeyValuePair<string, string>[] keyValuePairs)
        {
            var retList = new EmailModel();
            if (keyValuePairs == null) return retList;
            foreach (var k in keyValuePairs)
            {
                if (k.Key.ContainsText("where"))
                    retList.where = k.Value;
                if (k.Key.ContainsText("filter"))
                    retList.filter = k.Value;
                if (k.Key.ContainsText("field"))
                    retList.field = k.Value;
                if (k.Key.ContainsText("sort"))
                    retList.sort = k.Value;
                if (k.Key.ContainsText("method"))
                    retList.method = k.Value;
            }
            return retList;
        }
        #endregion






    }


}
