﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using NtC.Netuce.Business.Abstract;
using NtC.Netuce.WebApi.Filter;

namespace NtC.Netuce.WebApi.Controllers
{
    [EnableCors("*", "*", "*")]
    [InvalidModelStateFilter]
    public class ViewController : ApiController
    {

        private INtCService _repo;

        public ViewController(INtCService _repo)
        {
            this._repo = _repo;
        }

        [Authorize]
        [Route("createview/{viewname}")]
        public object CreateView(string viewname)
        {
            var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);
            if (string.IsNullOrEmpty(viewname))
            {
                return true;
            }
            var allUrlKeyValues = ControllerContext.Request.GetQueryNameValuePairs();

            var keyValuePairs = allUrlKeyValues as KeyValuePair<string, string>[] ?? allUrlKeyValues.ToArray();

            return _repo.CreateView(baseUrl, viewname, keyValuePairs);
        }

    }
}
