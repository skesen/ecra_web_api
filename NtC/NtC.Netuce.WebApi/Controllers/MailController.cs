﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using NtC.Netuce.Business.Abstract;
using NtC.Netuce.WebApi.Filter;

namespace NtC.Netuce.WebApi.Controllers
{
    [EnableCors("*", "*", "*")]
    [InvalidModelStateFilter]
    //[Authorize]
    public class MailController : ApiController
    {
        private INtCNotification _repo;

        public MailController(INtCNotification _repo)
        {
            this._repo = _repo;
        }
        
        [Route("sendmail")]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage SendMail()
        {
            var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);
            var allUrlKeyValues = ControllerContext.Request.GetQueryNameValuePairs();
            var keyValuePairs = allUrlKeyValues as KeyValuePair<string, string>[] ?? allUrlKeyValues.ToArray();

            return Request.CreateResponse(_repo.SendEmail(baseUrl, keyValuePairs) ? HttpStatusCode.OK : HttpStatusCode.BadRequest);
        }

    }
}
