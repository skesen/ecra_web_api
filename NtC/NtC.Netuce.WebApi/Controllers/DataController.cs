﻿using System.Data;
using System.IO;
using System.Net;
using System.Web.Services;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using NtC.Netuce.Business.Abstract;
using NtC.Netuce.WebApi.Filter;

namespace NtC.Netuce.WebApi.Controllers
{
    [EnableCors("*", "*", "*")]
    [InvalidModelStateFilter]
    public class DataController : ApiController
    {
        private INtCService _repo;

        public DataController(INtCService _repo)
        {
            this._repo = _repo;
        }

        //[Authorize]
        [Route("getexcel")]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetExcel([FromBody] object model)
        {
            return ExcelExport<dynamic>(JsonConvert.SerializeObject(model));
        }

        [WebMethod]
        public HttpResponseMessage ExcelExport<T>(string data)
        {
            var listData = (DataTable)JsonConvert.DeserializeObject(data, (typeof(DataTable)));

            string attachment = "attachment; filename=Report.xlsx";
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            System.Web.HttpContext.Current.Response.ClearContent();
            System.Web.HttpContext.Current.Response.AddHeader("content-disposition", attachment);
            System.Web.HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            var str = new StringBuilder();
            str.Append("<table border=`" + "1px" + "`b>");
            str.Append("<tr>");
            foreach (DataColumn dc in listData.Columns)
            {
                str.Append("<td><b><font face=Arial Narrow size=3>" + dc.ColumnName + "</font></b></td>");
            }
            str.Append("<td border=`" + "0px" + "`><img src='http://ytthukukdegerkaybi.appspot.com/img/logo-powered-by-ecra-250.png'></td>");

            str.Append("</tr>");
            foreach (DataRow dr in listData.Rows)
            {
                str.Append("<tr>");

                for (var i = 0; i < listData.Columns.Count; i++)
                {
                    str.Append("<td><font face=Arial Narrow size=" + "14px" + ">" + dr[i] + "</font></td>");
                }

                str.Append("</tr>"); 
            }
            str.Append("</table>");

            byte[] temp = Encoding.UTF8.GetBytes(str.ToString());

            //To write the table to the File content.
            response.Content = new StringContent(temp.ToString());

            return response;
        }
    }
}




#region Legacy Web NtcBase
//ntcBase.excelExport=function(table, queryParameters)
//{

//    this.queryParameters = { };
//    if (queryParameters)
//    {
//        this.queryParameters = queryParameters;
//    }
//    else
//    {
//        this.queryParameters = null;
//    }
//    var a = document.createElement("a");
//    document.body.appendChild(a);
//    var self = this;
//    return $.ajax({
//        url: self.setUrl('/getexcel/', table),
//        method: "GET",
//        responseType: 'arraybuffer',
//            headers:
//        {
//            'Content-type': 'application/json'
//            }
//    }).success(function(data) {
//        var blob = new Blob([data], { type: "application/vnd.ms-excel" }),
//                url = URL.createObjectURL(blob);
//        a.href = url;
//        a.download = table + new Date().toDateString();
//        a.click();
//        window.URL.revokeObjectURL(url);
//    });
//}
#endregion


#region Legacy Client ExcelExport
//function NTCExcelExport(data)
//{
//    this.data;
//    this.exportdata;

//    this.data = typeof data != 'object' ? JSON.parse(data) : data;
//    this.exportdata = '';
//}

//NTCExcelExport.prototype.generateExcelListForCSV = function()
//{
//    var self = this;

//    for(var key in self.data)
//    {
//        var currentdata = self.data[key];
//        var line = '';
//        for (var index in currentdata)
//        {
//            line += currentdata[index] + ',';
//        }

//        line.slice(0, line.Length - 1);

//        this.exportdata += line + '\r\n';
//    }
//    window.open("data:application/vnd.ms-excel;charset=utf-8," + encodeURIComponent(self.exportdata))
//}

    ///////////////js 
//function taskDetailButtonCallback()
//{
//    var taskid = this.taskid;
//    var redirectUrl = window.location.origin + '/projeler-gorevler/gorevler/' + taskid;
//    window.location = redirectUrl;
//}
#endregion
