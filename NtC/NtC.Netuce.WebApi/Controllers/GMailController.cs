﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using System.Web.Mvc;
using Google.Apis.Auth.OAuth2.Mvc;
using Google.Apis.Gmail.v1;
using Google.Apis.Services;
using NtC.Netuce.Core.CrossCuttingConcern.EmailSender;
using NtC.Netuce.Core.Utilities.ExtensionMethods;
using NtC.Netuce.WebApi.Extensions;
using NtC.Netuce.WebApi.Filter;
using NtC.Netuce.WebApi.Models;

namespace NtC.Netuce.WebApi.Controllers
{
    [EnableCors("*", "*", "*")]
    [InvalidModelStateFilter]
    public class GMailController : Controller
    {
        //[System.Web.Http.Route("mail/{type}")]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        public async Task<GmailService> GetGmailService(string type, CancellationToken cancellationToken)
        {
            try
            {

           
            var result = await new AuthorizationCodeMvcApp(this, new AppFlowMetadata()).
                AuthorizeAsync(cancellationToken);

            if (result.Credential != null)
            {
                var service = new GmailService(initializer: new BaseClientService.Initializer
                {
                    HttpClientInitializer = result.Credential,
                    ApplicationName = "ecra_web_api",

                });
             
                return service;
            }
            else
            {
                return null;
            }
            }
            catch (Exception ex)
            {

                var email = new ErrorEmail("Email integration", ex.Message);
                email.Send();
                return null;
            }
        }
        
    }
}
