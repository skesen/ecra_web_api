﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using NtC.Netuce.Core.Entities;
using NtC.Netuce.Core.Concrete.EntityFramework;
using NtC.Netuce.Entities.Concrete;

namespace NtC.Netuce.WebApi.OAuth
{

    public class AuthRepository : IDisposable
    {
        private NtCContext _ctx;

        private UserManager<ApplicationUser> _userManager;

        public AuthRepository()
        {
            _ctx = new NtCContext();
            _userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(_ctx));
        }

        public void Dispose()
        {
            _ctx.Dispose();
            _userManager.Dispose();

        }

        #region ApplicationUser Transaction
        public IdentityResult RegisterUser(ApplicationUser userModel, string password)
        {
            ApplicationUser user = new ApplicationUser
            {
                UserName = userModel.UserName,
                Email = userModel.Email,
                UserId = userModel.UserId
            };

            var result =  _userManager.Create(user, password);

            return result;
        }
        public ApplicationUser FindUserAsync(string userName, string password)
        {
            var user = _userManager.Find(userName, password);

            return user;
        }
        public ApplicationUser FindByEmail(string email)
        {
            ApplicationUser user = _userManager.FindByEmail(email);

            return user;
        }
        public ApplicationUser FindAsync(UserLoginInfo loginInfo)
        {
            ApplicationUser user = _userManager.Find(loginInfo);

            return user;
        }
        public ApplicationUser FindByIdAsync(string userid)
        {
            ApplicationUser user = _userManager.FindById(userid);

            return user;
        }
        public IdentityResult CreateAsync(ApplicationUser user)
        {
            var result =  _userManager.Create(user);

            return result;
        }
        public IdentityResult AddLoginAsync(string userId, UserLoginInfo login)
        {
            var result =  _userManager.AddLogin(userId, login);

            return result;
        }
        public bool ResetPassword(string userId, string token, string newPassword)
        {
            var isuser=_userManager.RemovePasswordAsync(userId);
            if (!isuser.IsCompleted) return false;
            var user = _userManager.AddPasswordAsync(userId, newPassword);
            return user.Result.Succeeded;
        }

        #endregion

        #region RefreshToken Transaction
        public Client FindClient(string clientId)
        {
            var client = _ctx.Client.Find(clientId);

            return client;
        }
        public async Task<bool> AddRefreshToken(RefreshToken token)
        {

            var existingToken = _ctx.RefreshToken.Where(r => r.subject == token.subject && r.clientid == token.clientid).SingleOrDefault();

            if (existingToken != null)
            {
                var result = await RemoveRefreshToken(existingToken);
            }

            _ctx.RefreshToken.Add(token);

            return await _ctx.SaveChangesAsync() > 0;
        }
        public async Task<bool> RemoveRefreshToken(string refreshTokenId)
        {
            var refreshToken = await _ctx.RefreshToken.FindAsync(refreshTokenId);

            if (refreshToken != null)
            {
                _ctx.RefreshToken.Remove(refreshToken);
                return await _ctx.SaveChangesAsync() > 0;
            }

            return false;
        }
        public async Task<bool> RemoveRefreshToken(RefreshToken refreshToken)
        {
            _ctx.RefreshToken.Remove(refreshToken);
            return await _ctx.SaveChangesAsync() > 0;
        }
        public string GeneratePasswordResetToken(string id)
        {
            return Guid.NewGuid().ToString();
        }
        public async Task<RefreshToken> FindRefreshToken(string refreshTokenId)
        {
            var refreshToken = await _ctx.RefreshToken.FindAsync(refreshTokenId);

            return refreshToken;
        }
        public List<RefreshToken> GetAllRefreshTokens()
        {
            return _ctx.RefreshToken.ToList();
        }
        #endregion
        
    }
}
