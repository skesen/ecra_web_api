﻿using NtC.Netuce.WebApi.OAuth.Providers;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Facebook;
using NtC.Netuce.WebApi.OAuth;

[assembly: OwinStartup(typeof(Startup))]
namespace NtC.Netuce.WebApi.OAuth
{
    public class Startup
    {
        public static OAuthBearerAuthenticationOptions OAuthBearerOptions { get; private set; }
        //public static GoogleOAuth2AuthenticationOptions googleAuthOptions { get; private set; }
        //public static FacebookAuthenticationOptions facebookAuthOptions { get; private set; }
        public void Configuration(IAppBuilder app)
        {
            app.Use<InvalidAuthenticationMiddleware>();

            HttpConfiguration config = new HttpConfiguration();

            ConfigureOAuth(app);
            //app.UseCors(CorsOptions.AllowAll);
            //WebApiConfig.Register(config);
            app.UseWebApi(config);


        }
        public void ConfigureOAuth(IAppBuilder app)
        {
            //use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);
            OAuthBearerOptions = new OAuthBearerAuthenticationOptions();

            OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {

                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = new AuthorizationServerProvider(),
                //RefreshTokenProvider = new RefreshTokenProvider()
            };

            // Token Generation
            app.UseOAuthAuthorizationServer(OAuthServerOptions);
            app.UseOAuthBearerAuthentication(OAuthBearerOptions);
            //Configure Google External Login
            //googleAuthOptions = new GoogleOAuth2AuthenticationOptions()
            //{
            //    ClientId = "xxxxxx",
            //    ClientSecret = "xxxxxx",
            //    Provider = new GoogleAuthProvider()
            //};
            //app.UseGoogleAuthentication(googleAuthOptions);

            ////Configure Facebook External Login
            //            facebookAuthOptions = new FacebookAuthenticationOptions()
            //            {
            //                AppId = "xxxxxx",
            //                AppSecret = "xxxxxx",
            //                Provider = new FacebookAuthenticationProvider
            //                {
            //#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
            //                    OnAuthenticated = async context =>
            //                    {
            //                        context.Identity.AddClaim(new System.Security.Claims.Claim("FacebookAccessToken", context.AccessToken));
            //                        foreach (var claim in context.User)
            //                        {
            //                            var claimType = string.Format("urn:facebook:{0}", claim.Key);
            //                            string claimValue = claim.Value.ToString();
            //                            if (!context.Identity.HasClaim(claimType, claimValue))
            //                                context.Identity.AddClaim(new System.Security.Claims.Claim(claimType, claimValue, "XmlSchemaString", "Facebook"));

            //                        }

            //                    }
            //#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
            //                }
            //        };
            //            app.UseFacebookAuthentication(facebookAuthOptions);

        }
    }
}
