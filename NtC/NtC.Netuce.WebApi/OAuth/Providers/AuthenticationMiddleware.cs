﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Owin;

namespace NtC.Netuce.WebApi.OAuth.Providers
{
    public class InvalidAuthenticationMiddleware : OwinMiddleware
    {
        public InvalidAuthenticationMiddleware(OwinMiddleware next)
            : base(next)
        {
        }

        public override async Task Invoke(IOwinContext context)
        {
            await Next.Invoke(context);

            if (context.Response.StatusCode == 400
            && context.Response.Headers.ContainsKey(
                      AuthorizationServerProvider.ServerGlobalVariables.OwinChallengeFlag))
            {
                var headerValues = context.Response.Headers.GetValues
                      (AuthorizationServerProvider.ServerGlobalVariables.OwinChallengeFlag);

                context.Response.StatusCode =
                       Convert.ToInt16(headerValues.FirstOrDefault());

                context.Response.Headers.Remove(
                       AuthorizationServerProvider.ServerGlobalVariables.OwinChallengeFlag);
            }
        }
    }
}