﻿using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Routing;
using Elmah.Contrib.WebApi;
using Newtonsoft.Json.Serialization;
using NtC.Netuce.WebApi.Filter;


namespace NtC.Netuce.WebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // enable elmah
            config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.LocalOnly;
            config.Services.Add(typeof(IExceptionLogger), new ElmahExceptionLogger());
            config.Routes.MapHttpRoute("AXD", "{resource}.axd/{*pathInfo}", null, null, new StopRoutingHandler());
            // Web API configurations
            config.MapHttpAttributeRoutes();
            config.EnableCors();

            //Filters
            config.Filters.Add(new ExceptionFilter());

            //Message handlers
            //ProgressMessageHandler progress = new ProgressMessageHandler();
            //progress.HttpSendProgress += HttpSendProgress;//acçılacak file upload için kodu aşağıda
            //config.MessageHandlers.Add(progress);
            //config.MessageHandlers.Add(new RequireHttpsMessageHandler()); //SSL kullanımları için


            // Web API routes
            // config.Routes.MapHttpRoute(
            //     name: "DefaultApi",
            //     routeTemplate: "api/{controller}/{id}",
            //     defaults: new { id = RouteParameter.Optional },
            //     constraints: null,
            //     handler: new RandomRequestMessageHandler()
            // );
            // config.Routes.MapHttpRoute(
            //     name: "Count",
            //     routeTemplate: "api/count/{controller}/{id}",
            //     defaults: new { controller = "ntc", id = RouteParameter.Optional },
            //    constraints: null,
            //     handler: new RandomRequestMessageHandler()
            // );
            // config.Routes.MapHttpRoute(
            //     name: "UserApi",
            //     routeTemplate: "user/{controller}/{id}",
            //     defaults: new { id = RouteParameter.Optional },
            //     constraints: null,
            //     handler: new RandomRequestMessageHandler()
            // );
            // config.Routes.MapHttpRoute(
            //    name: "User",
            //    routeTemplate: "{controller}/{id}",
            //    defaults: new { controller = "user", id = RouteParameter.Optional },
            //    constraints: null,
            //     handler: new RandomRequestMessageHandler()
            //);

            // config.Routes.MapHttpRoute(
            //    name: "Api",
            //    routeTemplate: "{controller}/{id}",
            //    defaults: new { controller = "ntc", id = RouteParameter.Optional },
            //    constraints: null,
            //     handler: new RandomRequestMessageHandler()
            //);

            config.Routes.MapHttpRoute(
            name: "NotFound",
            routeTemplate: "{*path}",
            defaults: new { controller = "Error", action = "NotFound" }
            );
            //Bu Kısım Cache için örnektir.

            //var eTagHandler = new HttpCachingHandler("Accept", "Accept-Charset");
            //eTagHandler.CacheInvalidationStore.Add(requestUri => {
            //    if (requestUri.StartsWith("/api/projects/", StringComparison.InvariantCultureIgnoreCase)) { return new[] { "/api/projects" }; }
            //    return new string[0];
            //});
            //config.MessageHandlers.Insert(0, eTagHandler);

            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            //config.Services.Replace(typeof(IExceptionHandler), new GlobalHandler());
        }
        //private static void HttpSendProgress(object sender, HttpProgressEventArgs e)
        //{
        //    HttpRequestMessage request = sender as HttpRequestMessage;
        //    var id = request.RequestUri.Query[0];
        //    var perc = e.ProgressPercentage;
        //    var b = e.TotalBytes;
        //    var bt = e.BytesTransferred;
        //}

    }
}
