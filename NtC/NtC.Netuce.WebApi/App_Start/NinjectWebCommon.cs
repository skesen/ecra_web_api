using System.Web.Http;
using NtC.Netuce.Business.Abstract;
using NtC.Netuce.Business.Concrete.Managers;
using NtC.Netuce.Core.Abstract;
using NtC.Netuce.Core.Concrete.EntityFramework.Utilities;
using WebApiContrib.IoC.Ninject;
using System;
using System.Web;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Web.Common;



[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(NtC.Netuce.WebApi.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(NtC.Netuce.WebApi.App_Start.NinjectWebCommon), "Stop")]

namespace NtC.Netuce.WebApi.App_Start
{
    

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                GlobalConfiguration.Configuration.DependencyResolver = new NinjectResolver(kernel);
                
                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }


        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<INtCService>().To<NtCManager>();
            kernel.Bind<INtCDal>().To<EfNtCDal>();
            kernel.Bind<IUserDal>().To<EfUserDal>();
            kernel.Bind<IUserService>().To<UserManager>();
            kernel.Bind<INotificationDal>().To<EfNotificationDal>();
            kernel.Bind<INtCNotification>().To<NotificationManager>();
        }        
    }
}
