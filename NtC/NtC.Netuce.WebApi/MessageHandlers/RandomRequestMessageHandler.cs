﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace NtC.Netuce.WebApi.MessageHandlers
{
    public class RandomRequestMessageHandler : HttpMessageHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request,
            CancellationToken cancellationToken)
        {

            var response = request.CreateResponse(HttpStatusCode.Forbidden, "Api Çalışıyor");
            return Task.FromResult(response);
        }
    }
}
