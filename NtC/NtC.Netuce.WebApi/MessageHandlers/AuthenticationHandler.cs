﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace NtC.Netuce.WebApi.MessageHandlers
{
    public class AuthenticationHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            try
            {
                var tokens = request.Headers.GetValues("Authorization").FirstOrDefault();
                if (tokens != null)
                {
                    byte[] data = Convert.FromBase64String(tokens);
                    string decodedString = Encoding.UTF8.GetString(data);
                    string[] tokensValues = decodedString.Split(':');

                    //check from db
                    if (tokensValues[0] == "serkan" && tokensValues[1] == "12345")
                    {
                        IPrincipal principal = new GenericPrincipal(new GenericIdentity(tokensValues[0]), new[] { "Admin" });
                        Thread.CurrentPrincipal = principal;
                        HttpContext.Current.User = principal;
                    }
                }

            }
            catch
            {
            }
            return base.SendAsync(request, cancellationToken);
        }
    }
}