﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Results;

namespace NtC.Netuce.WebApi.MessageHandlers
{

    public class GlobalHandler : ExceptionHandler
    {
        public override void Handle(ExceptionHandlerContext context)
        {
            const string message = "Your input conflicted with the current state of the system.";
            if (context.Exception is DivideByZeroException)
            {
                context.Result = new ResponseMessageResult(context.Request.CreateErrorResponse(HttpStatusCode.Conflict, message));
            }
            else
            {
                var error = new HttpError("An interval server error occured.")
                    {
                        {"CorrelationId", context.Request.GetCorrelationId().ToString()}
                    };
                var response = context.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, error);
                context.Result = new ResponseMessageResult(response);
            }
        }

        public override bool ShouldHandle(ExceptionHandlerContext context)
        {
            return true;
        }
    }

}
