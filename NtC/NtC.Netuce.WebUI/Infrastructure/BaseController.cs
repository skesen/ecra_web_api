﻿using System;
using System.IO;
using System.Web.Mvc;
using NtC.Netuce.Core.CrossCuttingConcern.ExceptionHandling.Exceptions;
using NtC.Netuce.Core.CrossCuttingConcern.Logging.Log4Net.Loggers;
using NtC.Netuce.Core.Utilities.EnumUtilities;

namespace NtC.Netuce.WebUI.Infrastructure
{
    public class BaseController:Controller
    {
        protected override void OnException(ExceptionContext filterContext)
        {
            Exception e = filterContext.Exception;
            var logger = new EventLoggerService();



            filterContext.ExceptionHandled = true;

            if (e is NotificationException)
            {
                logger.Info(filterContext.Exception.Message);

                filterContext.Result = new ViewResult
                {
                    TempData = new TempDataDictionary
                    {
                        { string.Format("NtC.{0}", NotifyType.Error),e.Message }
                    },
                    ViewData = new ViewDataDictionary(filterContext.Result)
                };
            }
            else
            {
                logger.Error(filterContext.Exception.Message);

                filterContext.Result = new ViewResult()
                {
                    ViewName = "Error",
                    ViewData = new ViewDataDictionary(e)
                };
            }
        }

        public virtual string RenderPartialViewToString()
        {
            return RenderPartialViewToString(null, null);
        }

        public virtual string RenderPartialViewToString(string viewName)
        {
            return RenderPartialViewToString(viewName, null);
        }

        public virtual string RenderPartialViewToString(object model)
        {
            return RenderPartialViewToString(null, model);
        }

        public virtual string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = this.ControllerContext.RouteData.GetRequiredString("action");

            this.ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(
                    this.ControllerContext, viewName);
                var viewContext = new ViewContext(this.ControllerContext, viewResult.View, this.ViewData, this.TempData,
                    sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        protected virtual void SuccessNotification(string message, bool persistForTheNextRequest = true)
        {
            AddNotification(NotifyType.Success, message, persistForTheNextRequest);
        }

        protected virtual void ErrorNotification(string message, bool persistForTheNextRequest = true)
        {
            AddNotification(NotifyType.Error, message, persistForTheNextRequest);
        }

        protected virtual void AddNotification(NotifyType type, string message, bool persistForTheNextRequest)
        {
            string dataKey = string.Format("NtC.{0}", type);

            if (persistForTheNextRequest)
                TempData[dataKey] = message;
            else
                ViewData[dataKey] = message;
        }
    }
}